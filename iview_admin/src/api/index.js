import axios from 'axios';
// 定义接口请求方法
const postComplexRequest = (url, params) => {
    return axios({
        method: 'post',
        url: `${url}`,
        data: params,
        headers: {
            'Content-Type': 'application/json',
        }
    });
};
// 统一请求路径前缀
var baseLuggage = 'http://47.93.185.195:7009';
// 获取旅客列表
export const findPassengerList = (params) => {
    return postComplexRequest(`${baseLuggage}` + '/luggagetrack/findPassengerList', params);
};
// 获取旅客行程
export const findTripByOneID = (params) => {
    return postComplexRequest(`${baseLuggage}` + '/luggagetrack/findTripByOneID', params);
};
// 获取旅客行李
export const findLuggageByTripID = (params) => {
    return postComplexRequest(`${baseLuggage}` + '/luggagetrack/findLuggageByTripID', params);
};



