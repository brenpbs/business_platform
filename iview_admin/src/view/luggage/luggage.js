import {
    findPassengerList,
    findTripByOneID,
    findLuggageByTripID
} from "@/api/index";
import Util from '@/libs/util';
export default {
    data () {
        return {
            searchForm: {
                pageNum: 1,
                pageSize: 10,
                memberName:"",
                certificateNumber:'',
                memberPhone : '',
            },
            columns: [
                {
                type: "index",
                width: 60,
                align: "center",
                fixed: "left"
                },
                {
                title: "姓名",
                key: "memberName",
                },
                {
                title: "手机号",
                key: "memberPhone",
                },
                {
                title: "证件号",
                key: "certificateNumber",
                },
                {
                title: "会员等级",
                key: "memberLevel",
                align:'center',
                    render: (h,params)=>{
                    let obj='';
                    if(params.row.memberLevel == '1'){
                        obj= '普通会员'
                    }else if(params.row.memberLevel == '2'){
                        obj= '中级会员'
                    }else if(params.row.memberLevel == '3'){
                        obj= '高级会员'
                    }
                    return h('div',
                        obj
                    )
                    }
                },
            ],
            data: [],
            total:0,
            chooseItem:false,
            showLuggage:false,
            detailLoading:true,
            tripColumns: [
                {
                    type: "index",
                    width: 60,
                    align: "center",
                    fixed: "left"
                },
                {
                    title: "航班号",
                    key: "flightNumber",
                },
                {
                    title: "出发地",
                    key: "flightDeparture",
                },
                {
                    title: "目的地",
                    key: "flightDestination",
                },
                {
                    title: "登机口",
                    key: "boardGate",
                },
                
            ],
            tripData:[],
            luggageColumns:[
                {
                    title: "行李编号",
                    key: "luggage_number",
                },
                {
                    title: "行李重量（KG）",
                    key: "luggage_weight",
                }, 
            ],
            luggageData:[],
        }
    },
    mounted() {
        this.init();
    },
    methods: {
        init() {
            this.getPassengerList();
        },
        changePage(v) {
        this.searchForm.pageNum = v;
        this.getPassengerList();
        },
        changePageSize(v) {
        this.searchForm.pageSize = v;
        this.getPassengerList();
        },
        // 点击搜索按钮
        handleSubmit(){
            this.searchForm.pageNum = 1;
            this.searchForm.pageSize = 10;
            this.getPassengerList();
        },
        // 获取旅客列表
        getPassengerList(){
            findPassengerList(this.searchForm).then(res => {
                console.log(res)
                this.loading = false;
                if (res.data.success === true) {
                    this.data = res.data.result.list;
                    this.total = res.data.result.total;
                }
            });
        },
        // 选择查询时间
        selectDate(e) {
            this.searchForm.flight_date = e;
        },
        // 点击查看旅客行程
        getTrip(v) {
            this.chooseItem = true;
            this.passengerInfo = v;
            this.getTriplList();
            this.showLuggage = false;
            this.luggageData = [];
        },
        // 查询旅客行程
        getTriplList(){
            console.log(this.passengerInfo)
            findTripByOneID({oneId:this.passengerInfo.oneId}).then(res => {
                this.detailLoading = false;
                if (res.data.success === true) {
                    this.tripData = res.data.result.list;
                }
            });
        },
        // 点击行程查询行李
        getTripLuggage(v){
            this.showLuggage = true;
            findLuggageByTripID({one_id:this.passengerInfo.oneId,trip_number:v.tripNumber}).then(res => {
                if (res.data.success === true) {
                    
                    this.luggageData = res.data.result.list;
                }
            });
        },
    },
}