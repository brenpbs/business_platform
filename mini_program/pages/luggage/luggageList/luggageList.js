//luggageList.js
const app = getApp()
Page({
  data: {
    currentIndex: 0,
    userInfo: {},
    tripInfo: {},
    luggageList: []
  },
  onLoad(options) {
    var that = this;
    var y = new Date().getFullYear(),
      m = new Date().getMonth() + 1,
      d = new Date().getDate();
    var nowDate = y + '-' + m + '-' + d;
    wx.request({
      url: app.appConfig.config.findLuggageByCertificateNumber + '/' + options.idCardNo + '/' + nowDate,
      success: function (res) {
        if (res.data && res.data.result) {
          res.data.result.tripInfoResultDTO.boardTime = that.fermitTime(res.data.result.tripInfoResultDTO.boardTime).slice(5,16);
          that.setData({
            userInfo: res.data.result.passengerResultDTO,
            tripInfo: res.data.result.tripInfoResultDTO,
            luggageList: res.data.result.pageLuggageFindReslutDTO.list
          })
        }
      },
      fail: function () {
        app.openAlert();
      }
    })
  },
  changeBtn: function (ev) {
    this.setData({
      currentIndex: ev.target.dataset.index
    })
  },
  // 格林威治时间转换
  fermitTime: function (time) {
    let now = new Date(time),
      Y = now.getFullYear(),
      M = now.getMonth() + 1,
      D = now.getDate(),
      h = now.getHours(),
      m = now.getMinutes(),
      s = now.getSeconds();
    M = M < 10 ? '0' + M : M;
    D = D < 10 ? '0' + D : D;
    h = h < 10 ? '0' + h : h;
    m = m < 10 ? '0' + m : m;
    s = s < 10 ? '0' + s : s;
    return Y + '-' + M + '-' + D + ' ' + h + ':' + m + ':' + s;
  },
})