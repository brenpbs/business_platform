//luggage.js
const app = getApp()
Page({
  data: {
    showInput: false,
    idCardNo: '',
    errTip:''
  },
  onLoad() {},
  onShow(){
    this.setData({
      showInput: false,
      idCardNo: '',
      errTip:''
    })
  },
  scan(){
    wx.scanCode({
      success (res) {
        wx.navigateTo({
          url: './luggageList/luggageList?idCardNo='+res.result,
        })
      }
    })
  },
  inputIdCardNo(e) {
    this.setData({
      idCardNo: e.detail.value
    })
  },
  showInputChange() {
    this.setData({
      showInput:true
    })
  },
  next() {
    // 这里可以使用util.js的校验库规则进行校验，调用方式是app.util.commonCheck
    if(app.util.commonCheck.isNull(this.data.idCardNo)){
      this.setData({errTip:'请输入证件号'})
      return
    }
    wx.navigateTo({
      url: './luggageList/luggageList?idCardNo='+this.data.idCardNo,
    })
  },
})