var appConfig = require('./utils/appConfig.js');
var util = require('./utils/util.js');
App({
    // ======================== 全局数据对象（整个应用程序共享） ===================
    globalData: {},
    appConfig: appConfig,
    util: util,
    // ==================================== 生命周期方法 ====================
    //小程序初始化时触发
    onLaunch() {},
    // 当应用程序进入前台显示状态时触发
    onShow() {},
    // 当应用程序进入后台状态时触发
    onHide() {},
    // ===================================== 应用程序全局方法 ==========================
    // 全局弹窗，可在项目中使用app.openAlert进行调用
    openAlert: function(val) {
        var text = '网络无反应，请检查您的网络设置';
        if (val) {
            text = val;
        }
        wx.showModal({
            content: text,
            showCancel: false,
            success: function(res) {
                if (res.confirm) {
                    console.log('用户点击确定')
                }
            }
        });
    },
    // 全局消息提示窗，可在项目中使用app.openToast进行调用
    openToast: function(val) {
        wx.showToast({
            title: val,
            icon: 'success',
            duration: 1000
        });
    }

})