// 常见校验规则
var commonCheck = {
    // 空校验
    isNull: function(data) {
        if (!data || data.trim().length == 0) {
            return true;
        } else {
            return false;
        }
    },
    // 手机号校验
    isPhone: function(data) {
        if (/^[1][3,4,5,6,7,8,9][0-9]{9}$/.test(data)) {
            return true;
        } else {
            return false;
        }
    },
    //固定长度数字校验
    isNNum: function(data, len) {
        if (/^[0-9]*$/.test(data) && data.length == len) {

            return true;
        } else {
            return false;
        }
    },
    // 长度校验
    isLength: function(data, len) {
        if (data.length == len) {
            return true;
        } else {
            return false;
        }
    },
    // 非文字校验
    isNoLetter: function(data) {
        if (/^[0-9a-zA-Z]*$/g.test(data)) {
            return true;
        } else {
            return false;
        }
    },
    // 证件号校验
    isID: function(id) {
        if (/^(\d{15}$|^\d{18}$|^\d{17}(\d|X|x))$/.test(id)) {
            return true;
        } else {
            return false;
        }
    },
    // 中文校验
    isRealName: function(data) {
        if (/^[\u4e00-\u9fa5a-zA-Z]+$/.test(data)) {
            return true;
        } else {
            return false;
        }
    },
    // 邮件校验
    isEmail: function (data) {
      if (/^[A-Za-z\d]+([-_.][A-Za-z\d]+)*@([A-Za-z\d]+[-.])+[A-Za-z\d]{2,4}$/.test(data)) {
        return true;
      } else {
        return false;
      }
    }
}

module.exports = {
    commonCheck: commonCheck
}