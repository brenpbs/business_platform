
  // 测试环境
  var basePath =  "http://47.93.185.195:7009";
  // 生产环境
  // var basePath = "https://mp.cwagpss.com";

//============================接口列表============================
var config = {
  'findLuggageByCertificateNumber': `${basePath}/luggagetrack/findLuggageByCertificateNumber`,
}

module.exports = {
  config: config
}