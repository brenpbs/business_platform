# 业务中台

#### 介绍
使用阿里hsf分布式微服务框架搭建的业务中台，涉及三个业务中心、vue后台管理端、微信小程序端

#### 软件架构
软件架构说明


#### 安装教程

##### 1.  轻量级配置中心
###### 1.1 前提条件
    在使用轻量级配置及注册中心前，请完成以下工作：  
    下载 1.8 及以上版本的 JDK，并设置环境变量 JAVA_HOME。  
    确认 8080 、8848 和 9600 端口未被使用  
###### 1.2 下载轻量级配置中心
Windows：  
下载[轻量级配置及注册中心压缩包](https://edas.oss-cn-hangzhou.aliyuncs.com/edas-res/edas-lightweight-server-1.0.0.zip?spm=a2c4g.11186623.2.18.79941e47xZOhNi&file=edas-lightweight-server-1.0.0.zip)  
在本地解压压缩包。  
Unix：  
执行命令 wget http://edas.oss-cn-hangzhou.aliyuncs.com/edas-res/edas-lightweight-server-1.0.0.tar.gz 下载轻量级配置及注册中心压缩包。  
执行命令 tar -zvxf edas-lightweight-server-1.0.0.tar.gz 解压压缩包。  
###### 1.3 启动轻量级配置中心
Windows启动  
    进入目录 edas-lightweight\bin。  
    启动轻量级配置及注册中心，并查看启动结果  
![输入图片说明](https://images.gitee.com/uploads/images/2020/0415/171851_6ae7b23a_810509.png "轻量级配置中心")
Unix启动：  
    执行 sh startup.sh  
###### 1.4 在本地开发环境中配置 hosts
    打开 hosts 文件。  
    Windows 操作系统：C:\Windows\System32\drivers\etc\hosts  
    Unix 操作系统：/etc/hosts  
    添加轻量级配置及注册中心配置。  
    如果在 IP 为 192.168.1.100 的机器上启动了轻量级配置及注册中心，则需要在 hosts 文件里加入配置：192.168.1.100 jmenv.tbsite.net。  
    如果在本地启动轻量级配置及注册中心，则在 hosts 文件中配置将上面的 IP 改为 127.0.0.1 jmenv.tbsite.net。  
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
