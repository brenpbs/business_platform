package com.happy.ipss.dto;
import java.util.List;
/**
 * 统一返回分页对象
 **/
public class PageDTO<T>{
    public PageDTO(long total, long pages, List<T> list) {
        this.total = total;
        this.pages = pages;
        this.list = list;
    }
    /**
     * 总条数
     */
    private long total;
    /**
     * 总页数
     */
    private long pages;
    /**
     * 列表
     */
    private List<T> list;

    public long getTotal() {
        return total;
    }

    public long getPages() {
        return pages;
    }

    public List<T> getList() {
        return list;
    }






}
