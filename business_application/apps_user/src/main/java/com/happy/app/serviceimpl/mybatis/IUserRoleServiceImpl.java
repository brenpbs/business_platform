package com.happy.app.serviceimpl.mybatis;

import com.happy.app.dao.mapper.UserRoleMapper;
import com.happy.app.entity.Role;
import com.happy.app.entity.UserRole;
import com.happy.app.service.mybatis.IUserRoleService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 *
 */
@Service
public class IUserRoleServiceImpl extends ServiceImpl<UserRoleMapper, UserRole>
		implements IUserRoleService {

	@Autowired
	private UserRoleMapper userRoleMapper;


	@Override
	public List<Role> findByUserIdAndTenantId(String userId, String tenantId) {
		//
		return userRoleMapper.findByUserIdAndTenantId(userId, tenantId);
	}

}
