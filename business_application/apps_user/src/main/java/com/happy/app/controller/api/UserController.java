package com.happy.app.controller.api;

import com.alibaba.dubbo.rpc.RpcContext;
import com.happy.app.appbase.base.BaseController;
import com.happy.app.appbase.common.vo.PageVo;
import com.happy.app.appbase.common.vo.Result;
import com.happy.app.appbase.common.vo.SearchVo;
import com.happy.app.controller.api.service.CommonService;
import com.happy.app.entity.Department;
import com.happy.app.entity.Role;
import com.happy.app.entity.User;
import com.happy.app.entity.UserB;
import com.happy.app.entity.sub.SubEmployeePageResultDTO;
import com.happy.app.service.DepartmentService;
import com.happy.app.service.UserService;
import com.happy.app.service.mybatis.IUserRoleService;
import com.happy.app.appbase.common.utils.*;
import com.happy.ipss.dto.PageRspDTO;
import com.happy.ipss.exception.ParamException;
import com.happy.ipss.exception.HandlerException;
import com.happy.ipss.user.api.constant.AccountStateParamEnum;
import com.happy.ipss.user.api.dto.LoginByAccountDTO;
import com.happy.ipss.user.api.dto.employee.EmployeeInfoDTO;
import com.happy.ipss.user.api.dto.employee.EmployeePageResultDTO;
import com.happy.ipss.user.api.dto.employee.EmployeeParamDTO;
import com.happy.ipss.user.api.service.EmployeeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import net.sf.json.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 *
 */
@Slf4j
@CrossOrigin
@RestController
@Api(description = "用户接口")
@RequestMapping("/app/user")
@CacheConfig(cacheNames = "user")
@Transactional
public class UserController extends BaseController {
    @Value("${appcode.applicationCode}")
    private String applicationCode;

    @Autowired
    private UserService userService;
    @Autowired
    private DepartmentService departmentService;
    @Autowired
    private IUserRoleService iUserRoleService;
    @PersistenceContext
    private EntityManager entityManager;
    @Autowired
    private CommonService commonService;
    // ###################中台接口对象##################
    @Autowired
    private EmployeeService employeeService;

    // ###################中台接口对象 end##################

    /**
     * zzPreFlightHandler
     *
     * @returnuser/info
     */
    @CrossOrigin
    @RequestMapping(value = "/info", method = {RequestMethod.POST, RequestMethod.GET, RequestMethod.OPTIONS})
    @ApiOperation(value = "获取当前登录用户接口")
    public Result<UserB> getUserInfo(HttpServletRequest request) {
        PageData pd = new PageData();
        pd = this.getPageData();
        String runum = pd.getString("runum");
        runum = Tools.getRandomNumBaCheckNull(runum);
        RpcContext.getContext().setAttachment("busSeq", runum);
        RpcContext.getContext().setAttachment("reqDesc", "获取当前登录用户接口");
        log.info("流水号 : " + runum + "应用:请求: 获取当前登录用户接口" + " 数据:" + JSONObject.fromObject(pd));
        String accessType = request.getHeader("accessType");
        String accessToken = request.getHeader("accessToken");
        log.info("请求类型为:" + accessType);
        log.info("请求token为:" + accessToken);
        String accountId = "";
        try {
            if (CommonUtils.EMPLOYEE_TYPE_E.equals(accessType)) {
                accountId = employeeService.authToken(accessToken, CommonUtils.TOKEN_EXPIRY_60);
            } else {
                return new ResultUtil<UserB>().setErrorMsg(CommonUtils.PARAM_IS_ERROR_CN);
            }

        } catch (Exception e) {
            Result<Object> checkExceptionMessage = ExceptionUtils.checkExceptionMessage(e);
            return new ResultUtil<UserB>().setErrorMsg(checkExceptionMessage.getMessage());
        }
//		User uu = userService.findByUsernameAndAccountType(accessName, accessType);
        User uu = userService.findByAccountId(accountId);
        log.info("流水号 : " + runum + "业务处理: userService.findByAccountId" + " 数据:" + JSONObject.fromObject(uu));
        if (uu == null) {
            return new ResultUtil<UserB>().setErrorMsg("账号异常");
        }

        UserB userB = new UserB();
        userB.setTenantId(uu.getTenantId());
        userB.setTenantCode("11");
        userB.setTenantName("11");
        userB.setTenantContactName("11");
        userB.setTenantContactPhone("11");
        userB.setId(uu.getId());
        userB.setAccountId(uu.getAccountId());
        userB.setDepartmentId(uu.getDepartmentId());
        userB.setUsername(uu.getUsername());
        userB.setAccountType(uu.getAccountType());
        userB.setRoles(uu.getRoles());
        userB.setPermissions(uu.getPermissions());
        userB.setDefaultRole(uu.getDefaultRole());
        // api获取用户信息-处理性别问题
        if ((userB != null) && (!StringUtils.isEmpty(userB.getAccountId()))) {
            String gender = "";
            try {
                if (CommonUtils.EMPLOYEE_TYPE_E.equals(accessType)) {
                    EmployeeInfoDTO findInfo = employeeService.findInfo(userB.getAccountId());
                    gender = findInfo.getEmployeeDTO().getGender();
                    userB.setCreateTime(findInfo.getCreateTime());
                    userB.setUpdateTime(findInfo.getUpdateTime());
                    userB.setNickName(findInfo.getEmployeeDTO().getEmployeeName());
                    userB.setDescription(findInfo.getEmployeeDTO().getRemark());
                    userB.setCreateTime(findInfo.getEmployeeDTO().getCreateTime());
                } else {
                    return new ResultUtil<UserB>().setErrorMsg(CommonUtils.PARAM_IS_ERROR_CN);
                }

            } catch (Exception e) {
                Result<Object> checkExceptionMessage = ExceptionUtils.checkExceptionMessage(e);
                return new ResultUtil<UserB>().setErrorMsg(checkExceptionMessage.getMessage());
            }

            // value = "0女 1男 2保密"
            if ("M".equals(gender)) {
                userB.setSex(1);
            } else if ("F".equals(gender)) {
                userB.setSex(0);
            } else {
                userB.setSex(2);
            }
        }
        // 清除持久上下文环境 避免后面语句导致持久化
        entityManager.clear();
        userB.setPassword(null);
        log.info("流水号 : " + runum + "应用:响应:获取当前登录用户接口" + " 数据:" + userB.getUsername());
        return new ResultUtil<UserB>().setData(userB);
    }

    /**
     * 工作人员登录-账号密码方式
     *
     * @param loginDto
     * @param accountType
     * @return
     */
    @RequestMapping(value = "/admin/loginByAccount", method = RequestMethod.POST)
    @ApiOperation(value = "账号密码方式")
    public Result<Object> loginByAccount(LoginByAccountDTO loginDto, @RequestParam(required = false) String accountType,
//			@RequestParam(required = false) String applicationCode,
                                         @RequestParam(required = false) Boolean saveLogin) {
        PageData pd = new PageData();
        pd = this.getPageData();
        String runum = pd.getString("runum");
        runum = Tools.getRandomNumBaCheckNull(runum);
        RpcContext.getContext().setAttachment("busSeq", runum);
        RpcContext.getContext().setAttachment("reqDesc", "账号密码方式");
        log.info("流水号 : " + runum + "应用:请求: 账号密码方式" + " 数据:" + JSONObject.fromObject(pd));
        // 验证参数是否为空
        List<String> list = new ArrayList<String>();
        list.add(loginDto.getLoginName());
        list.add(loginDto.getPassword());
        loginDto.setMaintainer("cxlxsystem");
        list.add(accountType);
        Result<Object> listExistEmpty = EmptyUtils.isListExistEmpty(list);
        if (!CommonUtils.SUCCESS_CODE_INTEGER.equals(listExistEmpty.getCode())) {
            log.info(listExistEmpty.getMessage());
            return listExistEmpty;
        }
        // 解决为空报错问题
        if (saveLogin == null) {
            saveLogin = false;
        }
        loginDto.setExpiry(CommonUtils.TOKEN_EXPIRY_60);
        loginDto.setApplicationCode(applicationCode);

        log.info("流水号 : " + runum + "业务处理: 访问service层 begin" + " 数据:" + "");
        Result<Object> resObj = commonService.loginByAccount(loginDto, accountType, saveLogin, runum);
        log.info("流水号 : " + runum + "业务处理: 访问service层 end" + " 数据:" + JSONObject.fromObject(resObj));
        log.info("流水号 : " + runum + "应用:响应:账号密码方式" + " 数据:" + JSONObject.fromObject(resObj));
        return resObj;
    }

    /**
     * 多条件分页获取用户列表zz
     *
     * @param accountParamDTO
     * @param searchVo
     * @param pageVo
     * @return
     */
    @RequestMapping(value = "/getByCondition", method = RequestMethod.GET)
    @ApiOperation(value = "多条件分页获取列表")
    public Result<PageRspDTO<SubEmployeePageResultDTO>> getByCondition(@ModelAttribute EmployeeParamDTO accountParamDTO,
                                                                       @ModelAttribute SearchVo searchVo, @ModelAttribute PageVo pageVo, String state) {
        PageData pd = new PageData();
        pd = this.getPageData();
        String runum = pd.getString("runum");
        runum = Tools.getRandomNumBaCheckNull(runum);
        RpcContext.getContext().setAttachment("busSeq", runum);
        RpcContext.getContext().setAttachment("reqDesc", "多条件分页获取列表");
        log.info("流水号 : " + runum + "应用:请求: 多条件分页获取列表" + " 数据:" + JSONObject.fromObject(pd));
        if (accountParamDTO.getPageNum() < 1 || accountParamDTO.getPageSize() < 0) {
            return new ResultUtil<PageRspDTO<SubEmployeePageResultDTO>>().setErrorMsg("页码不能小于1");
        }
        if (StringUtils.isEmpty(accountParamDTO.getTenantId())) {
            return new ResultUtil<PageRspDTO<SubEmployeePageResultDTO>>().setErrorMsg(CommonUtils.TENANTID_IS_NULL_EN);
        }
        // 状态转换
        if (StringUtils.isEmpty(state)) {

        } else if (CommonUtils.ACCOUNT_DELETE_CODE_Y.equals(state)) {
            accountParamDTO.setStateEnum(AccountStateParamEnum.NORMAL);
        } else if (CommonUtils.ACCOUNT_DELETE_CODE_N.equals(state)) {
            accountParamDTO.setStateEnum(AccountStateParamEnum.LOCK);

        } else {
            return new ResultUtil<PageRspDTO<SubEmployeePageResultDTO>>().setErrorMsg(CommonUtils.PARAM_IS_ERROR_CN);
        }

        PageRspDTO<EmployeePageResultDTO> findInfoByParam = null;
        // 性别转换
        String gender = accountParamDTO.getGender();
        if (CommonUtils.GENDER_MALE.equals(gender)) {
            accountParamDTO.setGender(CommonUtils.GENDE_M);
        } else if (CommonUtils.GENDER_FEMALE.equals(gender)) {
            accountParamDTO.setGender(CommonUtils.GENDE_F);
        } else if (CommonUtils.GENDER_UNKNOW.equals(gender)) {
            accountParamDTO.setGender(CommonUtils.GENDE_U);
        } else {
            accountParamDTO.setGender("");
        }
        try {
            findInfoByParam = employeeService.findInfoByParam(accountParamDTO);
            log.info("findInfoByParam = " + findInfoByParam);
            log.info("findInfoByParam JSONObject = " + JSONObject.fromObject(findInfoByParam));
        } catch (ParamException e) {
            log.info("HSF-ParamException" + e.getMessage());
            return new ResultUtil<PageRspDTO<SubEmployeePageResultDTO>>().setErrorMsg(e.getMessage());
        } catch (HandlerException e) {
            log.info("HSF-HandlerException:" + e.getMessage());
            return new ResultUtil<PageRspDTO<SubEmployeePageResultDTO>>().setErrorMsg(e.getMessage());
        } catch (Exception e) {
            log.info("HSF-Exception:" + e.getMessage());
            if (e.getMessage() != null && e.getMessage().contains(CommonUtils.HSF_MESSAGE_ERROR_EN)) {
                return new ResultUtil<PageRspDTO<SubEmployeePageResultDTO>>()
                        .setErrorMsg(CommonUtils.HSF_MESSAGE_ERROR_CN);
            }
            return new ResultUtil<PageRspDTO<SubEmployeePageResultDTO>>().setErrorMsg(e.getMessage());
        }
        List<EmployeePageResultDTO> getList = findInfoByParam.getList();
        //
        List<SubEmployeePageResultDTO> resList = new ArrayList<SubEmployeePageResultDTO>();
        PageRspDTO<SubEmployeePageResultDTO> resPage = new PageRspDTO<>(findInfoByParam.getCode(),
                findInfoByParam.getMessage(), findInfoByParam.getTotal(), findInfoByParam.getPages(), resList);
        for (EmployeePageResultDTO obj : getList) {
            SubEmployeePageResultDTO temp = new SubEmployeePageResultDTO();
            // 对象转换
            TransformationParendToSubPageResult(obj, temp);
            resList.add(temp);
        }
        Result<PageRspDTO<SubEmployeePageResultDTO>> setData = new ResultUtil<PageRspDTO<SubEmployeePageResultDTO>>()
                .setData(resPage);
        log.info("流水号 : " + runum + "应用:响应:多条件分页获取列表" + " 数据:" + JSONObject.fromObject(setData));
        return setData;
    }

    /**
     * 对象转换 EmployeePageResultDTO to SubEmployeePageResultDTO
     *
     * @param obj
     * @param temp
     * @return
     */
    private SubEmployeePageResultDTO TransformationParendToSubPageResult(EmployeePageResultDTO obj,
                                                                         SubEmployeePageResultDTO temp) {
        // 对象属性拷贝
        BeanUtils.copyProperties(obj, temp);
        // 性别转换
        String gender = obj.getGender();
        if (CommonUtils.GENDE_M.equals(gender)) {
            temp.setGender(CommonUtils.GENDER_MALE);
        } else if (CommonUtils.GENDE_F.equals(gender)) {
            temp.setGender(CommonUtils.GENDER_FEMALE);
        } else {
            temp.setGender(CommonUtils.GENDER_UNKNOW);
        }
        // 日期传唤 yyyy-MM-dd HH:mm:ss
        temp.setLastLoginTimeStr(DateUtil.getStrFromDate(obj.getLastLoginTime()));
        temp.setUpdateTimeStr(DateUtil.getStrFromDate(obj.getUpdateTime()));
        temp.setCreateTimeStr(DateUtil.getStrFromDate(obj.getCreateTime()));
        User userObj = userService.findByAccountId((obj.getAccountId()));
        if (userObj == null) {
            temp.setDepartmentId("");
            temp.setDepartmentTitle("");
            temp.setRoles(null);
        } else {
            // 添加部门信息
            if (org.apache.commons.lang.StringUtils.isEmpty(userObj.getDepartmentId())) {
                temp.setDepartmentId("");
                temp.setDepartmentTitle("");

            } else {
                Department deptObj = departmentService.get(userObj.getDepartmentId());
                if (deptObj == null) {
                    temp.setDepartmentId("");
                    temp.setDepartmentTitle("");
                } else {
                    temp.setDepartmentId(deptObj.getId());
                    temp.setDepartmentTitle(deptObj.getTitle());
                }
            }
            String userId = userObj.getId();
            // 添加角色
            List<Role> list = iUserRoleService.findByUserIdAndTenantId(userId, userObj.getTenantId());
            temp.setRoles(list);
        }
        return temp;
    }

}
