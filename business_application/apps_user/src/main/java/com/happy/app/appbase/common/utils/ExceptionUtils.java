package com.happy.app.appbase.common.utils;

import com.happy.app.appbase.common.vo.Result;

import com.happy.ipss.exception.ParamException;
import com.happy.ipss.exception.HandlerException;
import lombok.extern.slf4j.Slf4j;

/**
 * 处理异常信息
 *
 * @author zhaozhen
 *
 */
@Slf4j
public class ExceptionUtils {
	/**
	 * 处理异常信息
	 *
	 * @return
	 */
	public static Result<Object> checkExceptionMessage(Exception e) {
		if (e instanceof ParamException) {
			log.error("HSF-ParamException" + e.getMessage());
			String message = e.getMessage();
			String errormessage = message;
			if (message.contains("密码")) {
				errormessage = "密码错误";
				return new ResultUtil<Object>().setErrorMsg(errormessage);
			}
			if (message.contains("账户名称格式由字母开头")) {
				errormessage = "账户名称格式错误";
				return new ResultUtil<Object>().setErrorMsg(errormessage);
			}
			String[] split = message.split(":");
			if (split.length > 1) {
				errormessage = split[1];

			}
			return new ResultUtil<Object>().setErrorMsg(errormessage);

		} else if (e instanceof HandlerException) {
			log.error("HSF-HandlerException" + e.getMessage());
			String errormessage = e.getMessage();
			if (errormessage.contains("token无效")) {
				errormessage = "登录凭证过期，请重新登录";
				return new ResultUtil<Object>().setErrorMsg(100, errormessage);
			}
			if (errormessage.contains("手机号已存在")) {
				errormessage = "手机号已绑定";
				return new ResultUtil<Object>().setErrorMsg(100, errormessage);
			}
			String[] split = errormessage.split(":");
			if (split.length > 1) {
				errormessage = split[1];
				return new ResultUtil<Object>().setErrorMsg(errormessage);
			} else {
				return new ResultUtil<Object>().setErrorMsg(errormessage);
			}
		} else {
			log.error("HSF-Exception" + e.getMessage());
			String message = e.getMessage();
			String[] split = message.split(":");
			if (split.length > 1) {
				String errormessage = split[1];
				if (e.getMessage().contains("HSFServiceAddressNotFoundException")) {
					errormessage = "远程服务不可用";
				} else if (e.getMessage().contains("HSF-0001")) {
					errormessage = "远程服务不可用";
				} else if (e.getMessage().contains("Required")) {
					errormessage = "缺少字段信息";
				} else if (e.getMessage().contains("Redis")) {
					errormessage = "Redis服务不可用";
				} else if (e.getMessage().contains("ExceptionHSFTimeOutException")) {
					errormessage = "远程服务请求超时";
				} else if (e.getMessage().contains("HSF-0002")) {
					errormessage = "远程服务请求超时";
				}
				return new ResultUtil<Object>().setErrorMsg(errormessage);
			}
		}
		return new ResultUtil<Object>().setErrorMsg("服务器错误");
	}
}
