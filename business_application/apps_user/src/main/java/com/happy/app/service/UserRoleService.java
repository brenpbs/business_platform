package com.happy.app.service;


import com.happy.app.appbase.base.AppBaseService;
import com.happy.app.entity.UserRole;

import java.util.List;

/**
 * 用户角色接口
 *
 */
public interface UserRoleService extends AppBaseService<UserRole,String> {

    /**
     * 通过roleId查找
     * @param roleId
     * @return
     */
    List<UserRole> findByRoleId(String roleId);

    /**
     * 删除用户角色
     * @param userId
     */
    void deleteByUserId(String userId);
}
