package com.happy.app.appbase.exception;

import lombok.Data;

/**
 *
 */
@Data
public class AppException extends RuntimeException {

    private String msg;

    public AppException(String msg){
        super(msg);
        this.msg = msg;
    }
}
