package com.happy.app.appbase.base;

import com.happy.app.appbase.common.utils.Page;
import com.happy.app.appbase.common.utils.PageData;
import com.happy.app.appbase.common.utils.Tools;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

/**
 * 封装request请求到PageData
 *
 * @author 任波 修改时间：20170116
 */
public class BaseController {


	private static final long serialVersionUID = 6357869213649815390L;

	/**
	 * 得到32位的uuid
	 *
	 * @return
	 */
	public String get32UUID() {
		return Tools.get32UUID();
	}

	/**
	 * new PageData对象
	 *
	 * @return
	 */
	public PageData getPageData() {
		return new PageData(this.getRequest());
	}

	/**
	 * 得到ModelAndView
	 *
	 * @return
	 */
	public ModelAndView getModelAndView() {
		return new ModelAndView();
	}

	/**
	 * 得到ModelAndView
	 *
	 * @return
	 */
	public ModelAndView getModelAndView(String viewName) {
		return new ModelAndView(viewName);
	}

	/**
	 * 得到request对象
	 *
	 * @return
	 */
	public HttpServletRequest getRequest() {
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes())
				.getRequest();
		return request;
	}

	/**
	 * 得到分页列表的信息
	 *
			* @return
			*/
	public Page getPage() {
		return new Page();
	}



}
