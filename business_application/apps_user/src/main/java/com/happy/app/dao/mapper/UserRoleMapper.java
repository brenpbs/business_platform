package com.happy.app.dao.mapper;

import com.happy.app.entity.Role;
import com.happy.app.entity.UserRole;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 *
 */
@Mapper
public interface UserRoleMapper extends BaseMapper<UserRole> {


	/**
	 * 通过用户id和tenantId获取
	 * @param userId tenantId
	 * @return
	 */
	@Select("SELECT r.id id, r.name name,r.air_line air_line,r.airport_code airport_code FROM t_user_role ur LEFT JOIN t_role r  ON ur.role_id = r.id WHERE ur.user_Id = #{userId}      and ur.tenant_id=#{tenantId}")
	List<Role> findByUserIdAndTenantId(@Param("userId") String userId, @Param("tenantId") String tenantId);
}
