package com.happy.app.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.happy.app.dao.DepartmentDao;
import com.happy.app.dao.UserDao;
import com.happy.app.dao.mapper.PermissionMapper;
import com.happy.app.dao.mapper.UserRoleMapper;
import com.happy.app.entity.Department;
import com.happy.app.entity.Permission;
import com.happy.app.entity.Role;
import com.happy.app.entity.User;
import com.happy.app.service.UserService;

import cn.hutool.core.util.StrUtil;
import lombok.extern.slf4j.Slf4j;

/**
 * 用户接口实现
 *
 */
@Slf4j
@Service
@Transactional
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDao userDao;

	@Autowired
	private UserRoleMapper userRoleMapper;

	@Autowired
	private PermissionMapper permissionMapper;

	@Autowired
	private DepartmentDao departmentDao;

	@Override
	public UserDao getRepository() {
		return userDao;
	}

	@Override
	public User findByUsername(String username) {

//		List<User> list = userDao.findByUsernameAndStatus(username, CommonConstant.USER_STATUS_NORMAL);
		List<User> list = userDao.findByUsername(username);
		if (list != null && list.size() > 0) {
			User user = list.get(0);
			// 关联部门
			if (StrUtil.isNotBlank(user.getDepartmentId())) {
				Department department = departmentDao.findByIdAndTenantId(user.getDepartmentId(), user.getTenantId());
				user.setDepartmentTitle(department.getTitle());
			}
			// 关联角色
			List<Role> roleList = userRoleMapper.findByUserIdAndTenantId(user.getId(), user.getTenantId());
			user.setRoles(roleList);
//			// 关联权限菜单
//			List<Permission> permissionList = permissionMapper.findByUserIdAndTenantId(user.getId(), user.getTenantId());
//			user.setPermissions(permissionList);
			return user;
		}
		return null;
	}

	@Override
	public User findByUsernameAndAccountType(String username, String accountType) {

		List<User> list = userDao.findByUsernameAndAccountType(username, accountType);
		if (list != null && list.size() > 0) {
			User user = list.get(0);
			// 关联部门
			if (StrUtil.isNotBlank(user.getDepartmentId())) {
				Department department = departmentDao.findByIdAndTenantId(user.getDepartmentId(), user.getTenantId());
				user.setDepartmentTitle(department.getTitle());
			}
			// 关联角色
			List<Role> roleList = userRoleMapper.findByUserIdAndTenantId(user.getId(), user.getTenantId());
			user.setRoles(roleList);
			// 关联权限菜单
			List<Permission> permissionList = permissionMapper.findByUserIdAndTenantId(user.getId(), user.getTenantId());
			user.setPermissions(permissionList);
			return user;
		}
		return null;
	}

	@Override
	public List<User> findByDepartmentId(String departmentId) {

		return userDao.findByDepartmentId(departmentId);
	}

	@Override
	public User findByAccountId(String accountId) {
		//
		List<User> list = userDao.findByAccountId(accountId);

		if (list != null && list.size() > 0) {
			User user = list.get(0);
			// 关联部门
			if (StrUtil.isNotBlank(user.getDepartmentId())) {
				Department department = departmentDao.findByIdAndTenantId(user.getDepartmentId(), user.getTenantId());
				if (department != null) {
					user.setDepartmentTitle(department.getTitle());
				}

			}
			// 关联角色
			List<Role> roleList = userRoleMapper.findByUserIdAndTenantId(user.getId(), user.getTenantId());
			user.setRoles(roleList);
			// 关联权限菜单
			List<Permission> permissionList = permissionMapper.findByUserIdAndTenantId(user.getId(), user.getTenantId());
			user.setPermissions(permissionList);
			return user;
		}
		return null;
	}

	@Override
	public List<User> findByDepartmentIdAndTenantId(String departmentId, String tenantId) {
		//
		return userDao.findByDepartmentIdAndTenantId(departmentId, tenantId);

	}
}
