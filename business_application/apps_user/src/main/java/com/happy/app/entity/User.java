package com.happy.app.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.happy.app.appbase.common.utils.SnowFlakeUtil;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 *
 */
@Data
@Entity
@Table(name = "t_user")
@TableName("t_user")
public class User {

	private static final long serialVersionUID = 1L;
	@Id
	@TableId
	@ApiModelProperty(value = "唯一标识")
	private String id = String.valueOf(SnowFlakeUtil.getFlowIdInstance().nextId());
	@ApiModelProperty(value = "用户名")
	@Column(unique = true, nullable = false)
	private String username;
	@ApiModelProperty(value = "所属部门id")
	private String departmentId;

	@ApiModelProperty(value = "租户id")
	private String tenantId;
	@ApiModelProperty(value = "账号类型")
	private String accountType;
	@ApiModelProperty(value = "账号id")
	private String accountId;
	@Transient
	@TableField(exist = false)
	@ApiModelProperty(value = "所属部门名称")
	private String departmentTitle;

	@Transient
	@TableField(exist = false)
	@ApiModelProperty(value = "用户拥有角色")
	private List<Role> roles;

	@Transient
	@TableField(exist = false)
	@ApiModelProperty(value = "用户拥有的权限")
	private List<Permission> permissions;

	@Transient
	@TableField(exist = false)
	@ApiModelProperty(value = "导入数据时使用")
	private Integer defaultRole;

}
