package com.happy.app.appbase.common.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.happy.app.appbase.common.vo.Result;
import org.apache.commons.lang.StringUtils;

/**
 * zhaozhen
 */
public class EmptyUtils {
	/**
	 * 传入 字符串list,存在为空则立刻返回
	 *
	 * @return
	 */
	public static Result<Object> isListExistEmpty(List<String> list) {
		if (list == null || list.size() == 0) {
			new ResultUtil<Object>().setErrorMsg("参数列表为空");
		}
		for (String string : list) {
			if (StringUtils.isEmpty(string)) {
				return new ResultUtil<Object>().setErrorMsg("缺少必需表单字段");
			}
		}
		return new ResultUtil<Object>().setData("");
	}

	/**
	 * 传入 字符串map,返回为空的字段
	 *
	 * @return
	 */
	public static Result<Object> isMapExistEmpty(Map<String, String> map) {
		List<String> result = new ArrayList<>();
		if (map == null || map.size() == 0) {
			new ResultUtil<Object>().setErrorMsg("参数列表为空");
		}

		Set<String> keySet = map.keySet();
		for (String string : keySet) {
			if (StringUtils.isEmpty(map.get(string))) {
				result.add(string);
			}
		}
		// 如果 result 长度大于0 则存在 为空的 字段 返回 为空的字段 信息
		if (result.size() > 0) {
			new ResultUtil<Object>().setErrorMsg("[" + result.toString() + "],不能为空");
		}
		// 返回成功
		return new ResultUtil<Object>().setData(result);
	}

	public static void main(String[] args) {
		List<String> list = new ArrayList<>();

		for (int i = 0; i < 10; i++) {
			list.add(i + "");
			System.out.println(i);
			if (i == 5) {
//				return;
			}
		}
		System.out.println(list.toString());
	}
}
