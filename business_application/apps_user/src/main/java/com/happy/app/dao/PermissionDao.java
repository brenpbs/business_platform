package com.happy.app.dao;

import com.happy.app.appbase.base.AppBaseDao;
import com.happy.app.entity.Permission;

import java.util.List;

/**
 * 权限数据处理层
 * @author Exrick
 */
public interface PermissionDao extends AppBaseDao<Permission,String> {

    /**
     * 通过层级查找
     * 默认升序
     * @param level
     * @return
     */
    List<Permission> findByLevelOrderBySortOrder(Integer level);


	/**
	 * 通过层级 and tenantId 查找
	 * 默认升序
	 * @param level
	 * @return
	 */
	List<Permission> findByLevelAndTenantIdOrderBySortOrder(Integer level, String tenantId);


    /**
     * 通过parendId tenantId查找
     * @param parentId tenantId
     * @return
     */
    List<Permission> findByParentIdAndTenantIdOrderBySortOrder(String parentId, String tenantId);

    /**
     * 通过类型和状态获取
     * @param type
     * @param status
     * @return
     */
    List<Permission> findByTypeAndStatusOrderBySortOrder(Integer type, Integer status);

    /**
     * 通过名称获取
     * @param title
     * @return
     */
    List<Permission> findByTitle(String title);
}
