package com.happy.app.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.baomidou.mybatisplus.annotations.TableName;
import com.happy.app.appbase.base.AppBaseEntity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 *
 */
@Data
@Entity
@Table(name = "t_user_type")
@TableName("t_user_type")
public class UserType  extends AppBaseEntity {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "用户类型name")
	private String typeName;
	@ApiModelProperty(value = "用户类型code")
	private String typeCode;
	@ApiModelProperty(value = "描述")
	private String typeDesc;
	@ApiModelProperty(value = "租户id")
	private String tenantId;
	@ApiModelProperty(value = "状态")
	private String status;
}
