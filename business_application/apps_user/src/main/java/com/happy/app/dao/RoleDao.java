package com.happy.app.dao;

import com.happy.app.appbase.base.AppBaseDao;
import com.happy.app.entity.Role;

import java.util.List;

/**
 * 角色数据处理层
 *
 */
public interface RoleDao extends AppBaseDao<Role, String> {

	/**
	 * 获取默认角色
	 * @param defaultRole
	 * @return
	 */
	List<Role> findByDefaultRole(Boolean defaultRole);

	/**findByTenantId
	 * @return
	 */
	List<Role> findByTenantId(String tenantId);
}
