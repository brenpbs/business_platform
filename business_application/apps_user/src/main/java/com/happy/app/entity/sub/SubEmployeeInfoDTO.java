package com.happy.app.entity.sub;

import java.util.List;

import com.happy.app.entity.Permission;
import com.happy.app.entity.Role;

import com.happy.ipss.user.api.dto.employee.EmployeeInfoDTO;
import lombok.Data;

@Data
public class SubEmployeeInfoDTO extends EmployeeInfoDTO {

	private String lastLoginTimeStr;
	private String updateTimeStr;
	private String createTimeStr;
	private List<Permission> permissions;
	private String departmentTitle;
	private String departmentId;
	private List<Role> roles;
//	EmployeeInfoDTO parentDto;
//
//	public SubEmployeeInfoDTO(EmployeeInfoDTO parentDto) {
//		super();
//		this.parentDto = parentDto;
//		//
//	}

}
