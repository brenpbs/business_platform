package com.happy.app.controller;

import com.alibaba.boot.hsf.annotation.HSFConsumer;
import com.happy.ipss.user.api.service.EmployeeService;
import org.springframework.context.annotation.Configuration;

/**
 * consumer 接口调用
 *
 * @author yangf
 **/
@Configuration
public class HsfConfig {

    @HSFConsumer()
    private EmployeeService employeeService;

}
