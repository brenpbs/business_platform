package com.happy.app.controller.manage;

import com.happy.app.appbase.common.constant.CommonConstant;
import com.happy.app.appbase.common.utils.CommonUtils;
import com.happy.app.appbase.common.utils.ResultUtil;
import com.happy.app.appbase.common.vo.Result;
import com.happy.app.entity.Department;
import com.happy.app.entity.User;
import com.happy.app.service.DepartmentService;
import com.happy.app.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Exrick
 */
@Slf4j
@CrossOrigin
@RestController
@Api(description = "部门管理接口")
@RequestMapping("/app/department")
@Transactional
public class DepartmentController {

    @Autowired
    private DepartmentService departmentService;

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/getByParentId", method = RequestMethod.GET)
    @ApiOperation(value = "通过id获取")
    public Result<List<Department>> getByParentId(String parentId, String tenantId) {
        if (StringUtils.isEmpty(parentId)) {
            return new ResultUtil<List<Department>>().setErrorMsg("部门id不能为空");
        }
        if (StringUtils.isEmpty(tenantId)) {
            return new ResultUtil<List<Department>>().setErrorMsg(CommonUtils.TENANTID_IS_NULL_EN);
        }

//		List<Department> list = departmentService.findByParentIdOrderBySortOrder(parentId);
        List<Department> list = departmentService
                .findByParentIdAndTenantIdOrderBySortOrder(parentId, tenantId);
        // lambda表达式
        list.forEach(item -> {
            if (!CommonConstant.PARENT_ID.equals(item.getParentId())) {
                Department parent = departmentService.get(item.getParentId());
                item.setParentTitle(parent.getTitle());
            } else {
                item.setParentTitle("一级部门");
            }
        });
        return new ResultUtil<List<Department>>().setData(list);
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    @ApiOperation(value = "添加")
    public Result<Department> add(@ModelAttribute Department department) {
        if (StringUtils.isEmpty(department.getTenantId())) {
            return new ResultUtil<Department>().setErrorMsg(CommonUtils.TENANTID_IS_NULL_EN);
        }
        Department d = departmentService.save(department);
        // 如果不是添加的一级 判断设置上级为父节点标识
        if (!CommonConstant.PARENT_ID.equals(department.getParentId())) {
            Department parent = departmentService.get(department.getParentId());
            if (parent.getIsParent() == null || !parent.getIsParent()) {
                parent.setIsParent(true);
                departmentService.update(parent);
                // 更新上级节点的缓存
            }
        }
        return new ResultUtil<Department>().setData(d);
    }

    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    @ApiOperation(value = "编辑")
    public Result<Department> edit(@ModelAttribute Department department) {
        if (StringUtils.isEmpty(department.getTenantId())) {
            return new ResultUtil<Department>().setErrorMsg(CommonUtils.TENANTID_IS_NULL_EN);
        }
        Department d = departmentService.update(department);
        return new ResultUtil<Department>().setData(d);
    }

    @RequestMapping(value = "/delByIds", method = RequestMethod.DELETE)
    @ApiOperation(value = "批量通过id删除")
    public Result<Object> delByIds(String ids, String tenantId) {
        if (ids == null || "".equals(ids)) {
            return new ResultUtil<Object>().setErrorMsg("部门id不能为空");
        }
        if (StringUtils.isEmpty(tenantId)) {
            return new ResultUtil<Object>().setErrorMsg(CommonUtils.TENANTID_IS_NULL_EN);
        }
        String[] ids2 = ids.split(",");
        for (String departmentId : ids2) {
//			List<User> list = userService.findByDepartmentId(departmentId);
            List<User> list = userService.findByDepartmentIdAndTenantId(departmentId, tenantId);
            if (list != null && list.size() > 0) {
                return new ResultUtil<Object>().setErrorMsg("删除失败，包含正被用户使用关联的部门");
            }
        }
        for (String departmentId : ids2) {
            departmentService.delete(departmentId);
        }
        return new ResultUtil<Object>().setSuccessMsg("批量通过id删除数据成功");
    }

}
