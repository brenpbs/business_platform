package com.happy.app.serviceimpl;

import com.happy.app.dao.DepartmentDao;
import com.happy.app.entity.Department;
import com.happy.app.service.DepartmentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 部门接口实现
 * @author Exrick
 */
@Slf4j
@Service
@Transactional
public class DepartmentServiceImpl implements DepartmentService {

	@Autowired
	private DepartmentDao departmentDao;

	@Override
	public DepartmentDao getRepository() {
		return departmentDao;
	}
//
//	@Override
//	public List<Department> findByParentIdOrderBySortOrder(String parentId) {
//
//		return departmentDao.findByParentIdOrderBySortOrder(parentId);
//	}

	@Override
	public List<Department> findByParentIdAndTenantIdAndStatusOrderBySortOrder(String parentId,
			String tenantId, Integer status) {

		return departmentDao.findByParentIdAndTenantIdAndStatusOrderBySortOrder(parentId, tenantId,
				status);
	}

	@Override
	public List<Department> findByParentIdAndTenantIdOrderBySortOrder(String parentId,
			String tenantId) {
		//   Auto-generated method stub
		return departmentDao.findByParentIdAndTenantIdOrderBySortOrder(parentId, tenantId);
	}

	@Override
	public Department findByIdAndTenantId(String id, String tenantId) {
		//
		return departmentDao.findByIdAndTenantId(id, tenantId);
	}
}
