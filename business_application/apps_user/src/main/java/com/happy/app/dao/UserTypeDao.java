package com.happy.app.dao;

import com.happy.app.appbase.base.AppBaseDao;
import com.happy.app.entity.UserType;

import java.util.List;

/**
 * 用户数据处理层
 *
 */
public interface UserTypeDao extends AppBaseDao<UserType, String> {

	/**
	 * 通过租户id和状态获取用户类型
	 *
	 * @param username
	 * @param status
	 * @return
	 */
	List<UserType> findByTenantIdAndStatus(String tenantId, String status);

	/**
	 * 通过状态获取用户类型
	 *
	 * @param username
	 * @param status
	 * @return
	 */
	List<UserType> findByStatus(String status);

}
