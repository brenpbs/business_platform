package com.happy.app.service;

import com.happy.app.appbase.base.AppBaseService;
import com.happy.app.entity.User;

import java.util.List;

/**
 * 用户接口
 *
 */
public interface UserService extends AppBaseService<User, String> {

	/**
	 * 通过用户名获取用户
	 *
	 * @param username
	 * @return
	 */
//	@Cacheable(key = "#username")
	User findByUsername(String username);

	/**
	 * 通过用户名,accountType获取用户
	 *
	 * @param username
	 * @return
	 */
	User findByUsernameAndAccountType(String username,String accountType);
	/**
	 * 通过部门id获取
	 *
	 * @param departmentId
	 * @return
	 */
	List<User> findByDepartmentId(String departmentId);

	/**
	 * 通过账号id获取用户
	 *
	 * @param mobile
	 * @return
	 */
	User findByAccountId(String accountId);

	/**
	 * 通过部门id和tenantId获取用户
	 *
	 * @param departmentId
	 * @param tenantId
	 * @return
	 */
	List<User> findByDepartmentIdAndTenantId(String departmentId, String tenantId);

}
