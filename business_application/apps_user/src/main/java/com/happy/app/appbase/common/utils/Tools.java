package com.happy.app.appbase.common.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.TimeZone;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * 说明：常用工具
 * 创建人：任波
 * @version
 */
public class Tools {
	/*// 国际国际空空
	public static final TransferTypeEnum ABROAD_AIR_TO_ABROAD_AIR = TransferTypeEnum.ABROAD_AIR_TO_ABROAD_AIR;
	// 国际地面
	public static final TransferTypeEnum ABROAD_AIR_TO_GROUND = TransferTypeEnum.ABROAD_AIR_TO_GROUND;
	// 国际国内空空
	public static final TransferTypeEnum ABROAD_AIR_TO_INTERNAL_AIR = TransferTypeEnum.ABROAD_AIR_TO_INTERNAL_AIR;
	// 地面国际
	public static final TransferTypeEnum GROUND_TO_ABROAD_AIR = TransferTypeEnum.GROUND_TO_ABROAD_AIR;
	// 地面国内
	public static final TransferTypeEnum GROUND_TO_INTERNAL_AIR = TransferTypeEnum.GROUND_TO_INTERNAL_AIR;
	// 国内国际空空
	public static final TransferTypeEnum INTERNAL_AIR_TO_ABROAD_AIR = TransferTypeEnum.INTERNAL_AIR_TO_ABROAD_AIR;
	// 国内地面
	public static final TransferTypeEnum INTERNAL_AIR_TO_GROUND = TransferTypeEnum.INTERNAL_AIR_TO_GROUND;
	// 国内空空
	public static final TransferTypeEnum INTERNAL_AIR_TO_INTERNAL_AIR = TransferTypeEnum.INTERNAL_AIR_TO_INTERNAL_AIR;
	// 地面地面
	public static final TransferTypeEnum GROUND_TO_GROUND = TransferTypeEnum.GROUND_TO_GROUND;*/

	private static final String date1 = "11:00"; // 饭点参数
	private static final String date2 = "13:00"; // 饭点参数
	private static final String date3 = "17:00"; // 饭点参数
	private static final String date4 = "19:00"; // 饭点参数
	private static final String date5 = "05:00"; // 早上优享时间参数
	private static final String date6 = "05:55"; // 早上优享时间参数

	/**
	 * 随机生成八位数验证码
	 *
	 * @return
	 */
	public static String getRandomNumBaCheckNull(String runum) {
		if (runum == null || "".equals(runum)) {
			int runumInt = new Random().nextInt(90000000) + 10000000;
			runum = runumInt + "";
		}
		return runum;
	}

	/**
	 * 随机生成六位数验证码
	 * @return
	 */
	public static int getRandomNum() {
		Random r = new Random();
		return r.nextInt(900000) + 100000;// (Math.random()*(999999-100000)+100000)
	}

	/**
	 * 检测字符串是否不为空(null,"","null")
	 * @param s
	 * @return 不为空则返回true，否则返回false
	 */
	public static boolean notEmpty(String s) {
		return s != null && !"".equals(s) && !"null".equals(s);
	}

	/**
	 * 检测字符串是否为空(null,"","null")
	 * @param s
	 * @return 为空则返回true，不否则返回false
	 */
	public static boolean isEmpty(String s) {
		return s == null || "".equals(s) || "null".equals(s);
	}

	/**
	 * 字符串转换为字符串数组
	 * @param str 字符串
	 * @param splitRegex 分隔符
	 * @return
	 */
	public static String[] str2StrArray(String str, String splitRegex) {
		if (isEmpty(str)) {
			return null;
		}
		return str.split(splitRegex);
	}

	/**
	 * 生产唯一32位字符串
	 * @return
	 */
	public static String get32UUID() {
		String uuid = UUID.randomUUID().toString().trim().replaceAll("-", "");
		return uuid;
	}

	/**
	 * 生产唯一36位字符串
	 */
	public static String get36UUID() {
		String uuid = UUID.randomUUID().toString();
		return uuid;
	}

	/**
	 * 用默认的分隔符(,)将字符串转换为字符串数组
	 * @param str	字符串
	 * @return
	 */
	public static String[] str2StrArray(String str) {
		return str2StrArray(str, ",\\s*");
	}

	/**
	 * 按照yyyy-MM-dd HH:mm:ss的格式，日期转字符串
	 * @param date
	 * @return yyyy-MM-dd HH:mm:ss
	 */
	public static String date2Str(Date date) {
		return date2Str(date, "yyyy-MM-dd HH:mm:ss");
	}

	/**
	 * 按照yyyy-MM-dd HH:mm:ss的格式，字符串转日期
	 * @param date
	 * @return
	 */
	public static Date str2Date(String date) {
		if (notEmpty(date)) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			sdf.setTimeZone(TimeZone.getTimeZone("Asia/Shanghai"));
			try {
				return sdf.parse(date);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			return new Date();
		} else {
			return null;
		}
	}

	/**
	 * 按照参数format的格式，日期转字符串
	 * @param date
	 * @param format
	 * @return
	 */
	public static String date2Str(Date date, String format) {
		if (date != null) {
			SimpleDateFormat sdf = new SimpleDateFormat(format);
			return sdf.format(date);
		} else {
			return "";
		}
	}

	/**
	 * 把时间根据时、分、秒转换为时间段
	 * @param StrDate
	 */
	public static String getTimes(String StrDate) {
		String resultTimes = "";

		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		df.setTimeZone(TimeZone.getTimeZone("Asia/Shanghai"));
		java.util.Date now;

		try {
			now = new Date();
			java.util.Date date = df.parse(StrDate);
			long times = now.getTime() - date.getTime();
			long day = times / (24 * 60 * 60 * 1000);
			long hour = (times / (60 * 60 * 1000) - day * 24);
			long min = ((times / (60 * 1000)) - day * 24 * 60 - hour * 60);
			long sec = (times / 1000 - day * 24 * 60 * 60 - hour * 60 * 60 - min * 60);

			StringBuffer sb = new StringBuffer();
			// sb.append("发表于：");
			if (hour > 0) {
				sb.append(hour + "小时前");
			} else if (min > 0) {
				sb.append(min + "分钟前");
			} else {
				sb.append(sec + "秒前");
			}

			resultTimes = sb.toString();
		} catch (ParseException e) {
			e.printStackTrace();
		}

		return resultTimes;
	}

	/**
	 * 写txt里的单行内容
	 * @param filePath  文件路径
	 * @param content  写入的内容
	 */
	public static void writeFile(String fileP, String content) {
		String filePath = String.valueOf(Thread.currentThread().getContextClassLoader().getResource("")) + "../../"; // 项目路径
		filePath = (filePath.trim() + fileP.trim()).substring(6).trim();
		if (filePath.indexOf(":") != 1) {
			filePath = File.separator + filePath;
		}
		try {
			OutputStreamWriter write = new OutputStreamWriter(new FileOutputStream(filePath), "utf-8");
			BufferedWriter writer = new BufferedWriter(write);
			writer.write(content);
			writer.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	  * 验证邮箱
	  * @param email
	  * @return
	  */
	public static boolean checkEmail(String email) {
		boolean flag = false;
		try {
			String check = "^([a-z0-9A-Z]+[-|_|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$";
			Pattern regex = Pattern.compile(check);
			Matcher matcher = regex.matcher(email);
			flag = matcher.matches();
		} catch (Exception e) {
			flag = false;
		}
		return flag;
	}

	/**
	 * 验证手机号码
	 * @param mobiles
	 * @return
	 */
	public static boolean checkMobileNumber(String mobileNumber) {
		boolean flag = false;
		try {
			Pattern regex = Pattern.compile("^(((13[0-9])|(15([0-3]|[5-9]))|(18[0,5-9]))\\d{8})|(0\\d{2}-\\d{8})|(0\\d{3}-\\d{7})$");
			Matcher matcher = regex.matcher(mobileNumber);
			flag = matcher.matches();
		} catch (Exception e) {
			flag = false;
		}
		return flag;
	}

	/**
	 * 检测KEY是否正确
	 * @param paraname  传入参数
	 * @param FKEY		接收的 KEY
	 * @return 为空则返回true，不否则返回false
	 */
	public static boolean checkKey(String paraname, String FKEY) {
		paraname = (null == paraname) ? "" : paraname;
		return MD5.md5(paraname + DateUtil.getDays() + ",fh,").equals(FKEY);
	}

	/**
	 * 读取txt里的单行内容
	 * @param filePath  文件路径
	 */
	public static String readTxtFile(String fileP) {
		try {

			String filePath = String.valueOf(Thread.currentThread().getContextClassLoader().getResource("")) + "../../"; // 项目路径
			filePath = filePath.replaceAll("file:/", "");
			filePath = filePath.replaceAll("%20", " ");
			filePath = filePath.trim() + fileP.trim();
			if (filePath.indexOf(":") != 1) {
				filePath = File.separator + filePath;
			}
			String encoding = "utf-8";
			File file = new File(filePath);
			if (file.isFile() && file.exists()) { // 判断文件是否存在
				InputStreamReader read = new InputStreamReader(new FileInputStream(file), encoding); // 考虑到编码格式
				BufferedReader bufferedReader = new BufferedReader(read);
				String lineTxt = null;
				while ((lineTxt = bufferedReader.readLine()) != null) {
					return lineTxt;
				}
				read.close();
			} else {
				System.out.println("找不到指定的文件,查看此路径是否正确:" + filePath);
			}
		} catch (Exception e) {
			System.out.println("读取文件内容出错");
		}
		return "";
	}

	/**
	 * 判断是否在饭点
	 * 分三种情况不 隔夜和隔一夜和隔两夜
	 * 不隔夜时 分为三种情况 1: 进港时间 <= 11:00 且  离岗时间 >= 11:00, 2: 11:00 <= 进港时间  <= 13:00, 3: 13:00<=进港时间 <=19:00 且  离岗时间 >=17:00
	 * 隔一夜时分为三种情况 1： 进港时间 <=11:00 , 2: 11:00 <= 进港时间 <= 19:00, 3: 进港时间 >= 19:00 且 离岗时间 >= 11:00
	 * 隔两夜返回true
	 * @throws ParseException
	 */

	public static boolean iseat(String outdepttm, String inarrvtm, boolean isgoday, boolean istowday) throws ParseException {
		SimpleDateFormat format = new SimpleDateFormat("HH:mm");
		TimeZone.setDefault(TimeZone.getTimeZone("GMT+8"));
		String param[] = inarrvtm.split(" ");
		String inarrvtmparam = param[1];
		String param2[] = outdepttm.split(" ");
		String outdepttmparam = param2[1];
		Date arrivetime = format.parse(inarrvtmparam);
		Date outtime = format.parse(outdepttmparam);
		Date date1 = format.parse(Tools.date1);// 11:00
		Date date2 = format.parse(Tools.date2);// 13:00
		Date date3 = format.parse(Tools.date3);// 17:00
		Date date4 = format.parse(Tools.date4);// 19:00
		if (!isgoday) {
			// 不隔夜
			if ((arrivetime.compareTo(date1) <= 0 && outtime.compareTo(date1) >= 0)
					|| (arrivetime.compareTo(date1) >= 0 && arrivetime.compareTo(date2) <= 0)
					|| (arrivetime.compareTo(date2) >= 0 && arrivetime.compareTo(date4) <= 0 && outtime.compareTo(date3) >= 0)) {
				return true;
			} else {
				return false;
			}
		} else {
			// 隔夜
			if (istowday) {// 是否隔两夜
				return true;
			} else {
				if ((arrivetime.compareTo(date1) <= 0) || (arrivetime.compareTo(date1) >= 0 && arrivetime.compareTo(date4) <= 0)
						|| (arrivetime.compareTo(date4) >= 0 && outtime.compareTo(date1) >= 0)) {
					return true;
				} else {
					return false;
				}
			}
		}

	}

	/**
	 * 判断是否可以享受两顿免餐食(银川机场)
	 * 分为三种情况隔夜和不隔夜和隔两夜
	 * 不隔夜：进港时间<=13:00 且 离港时间>=17:00
	 * 隔一夜分为：1   进港时间<13:00  2 13:00<进港时间<19:00 且 离岗时间>11:00  3:进港时间>19:00 且 离港时间 >17:00
	 * 隔两夜返回true
	 * @throws ParseException
	 */
	public static boolean isEatTwo(String outdepttm, String inarrvtm) throws ParseException {
		SimpleDateFormat format = new SimpleDateFormat("HH:mm");
		TimeZone.setDefault(TimeZone.getTimeZone("GMT+8"));
		String param[] = inarrvtm.split(" ");
		String inarrvtmparam = param[1];
		String param2[] = outdepttm.split(" ");
		String outdepttmparam = param2[1];
		Date arrivetime = format.parse(inarrvtmparam);
		Date outtime = format.parse(outdepttmparam);
		Date date1 = format.parse(Tools.date1);// 11:00
		Date date2 = format.parse(Tools.date2);// 13:00
		Date date3 = format.parse(Tools.date3);// 17:00
		Date date4 = format.parse(Tools.date4);// 19:00
		boolean isgoday = isgoday(outdepttm, inarrvtm);
		boolean isgotowday = isgotowday(outdepttm, inarrvtm);
		if (!isgoday) {
			if (arrivetime.compareTo(date2) <= 0 && outtime.compareTo(date3) >= 0) {
				return true;
			} else {
				return false;
			}
		} else {
			if(isgotowday) {//跨两夜
				return true;
			}else {
				if (arrivetime.compareTo(date2) <= 0
						|| (arrivetime.compareTo(date2) >= 0 && arrivetime.compareTo(date4) <= 0 && outtime.compareTo(date1) >= 0)
						|| (arrivetime.compareTo(date4) >= 0 && outtime.compareTo(date3) >= 0)) {
					return true;
				} else {
					return false;
				}
			}

		}

	}

	/**
	 * 计算是否隔夜按第二天零点作为节点
	 */

	public static boolean isgoday(String outdepttm, String inarrvtm) throws ParseException {
		// SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH");
		Date inarrvtmdate = format.parse(inarrvtm);
		Date outdepttmdate = format.parse(outdepttm);
		TimeZone curTimeZone = TimeZone.getTimeZone("GMT+8");
		Calendar c = Calendar.getInstance(curTimeZone);
		c.setTime(inarrvtmdate);
		c.set(Calendar.HOUR_OF_DAY, 24);
		// c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		Date z = c.getTime();
		// String zeroTime = format.format(z);
		// System.out.println(format.format(z));
		int result1 = inarrvtmdate.compareTo(z);// 进港航班计划到达时间和进港航班计划到达时间第二天的零点做比较
		int result2 = outdepttmdate.compareTo(z);// 出港航班计划起飞时间和进港航班计划到达时间第二天的零点做比较
		if (result1 <= 0 && result2 >= 0) {// 进港航班计划到达时间 小于 进港航班计划到达时间 并且 出港航班计划起飞时间 大于 进港航班计划到达时间 说明跨夜
			return true;
		} else {
			return false;
		}

	}

	/**
	 * 计算是否隔夜按第二天凌晨1点作为节点
	 */

	public static boolean isgodayINC(String outdepttm, String inarrvtm) throws ParseException {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH");
		SimpleDateFormat fo = new SimpleDateFormat("yyyy-MM-dd");
		Date inarrvtmdate = format.parse(inarrvtm);
		Date outdepttmdate = format.parse(outdepttm);
		Date inarr = fo.parse(inarrvtm);
		Date outdept = fo.parse(outdepttm);
		TimeZone curTimeZone = TimeZone.getTimeZone("GMT+8");
		Calendar c = Calendar.getInstance(curTimeZone);
		c.setTime(inarrvtmdate);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		if(inarr.equals(outdept)) {
			c.set(Calendar.HOUR_OF_DAY, 1);
			Date z = c.getTime();
			int result1 = inarrvtmdate.compareTo(z);// 进港航班计划到达时间和进港航班计划到达时间第二天的凌晨1点做比较
			int result2 = outdepttmdate.compareTo(z);// 出港航班计划起飞时间和进港航班计划到达时间第二天的凌晨1点做比较
			if (result1 <= 0 && result2 >= 0) {
				return true;
			} else {
				return false;
			}
		}else {
			c.set(Calendar.HOUR_OF_DAY, 25);
			Date z = c.getTime();
			int result1 = inarrvtmdate.compareTo(z);// 进港航班计划到达时间和进港航班计划到达时间第二天的凌晨1点做比较
			int result2 = outdepttmdate.compareTo(z);// 出港航班计划起飞时间和进港航班计划到达时间第二天的凌晨1点做比较
			if (result1 <= 0 && result2 >= 0) {
				return true;
			} else {
				return false;
			}
		}


	}

	/**
	 * 计算是否隔两夜
	 */
	public static boolean isgotowday(String outdepttm, String inarrvtm) throws ParseException {
		// SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH");
		Date inarrvtmdate = format.parse(inarrvtm);
		Date outdepttmdate = format.parse(outdepttm);
		TimeZone curTimeZone = TimeZone.getTimeZone("GMT+8");
		Calendar c = Calendar.getInstance(curTimeZone);
		c.setTime(inarrvtmdate);
		c.set(Calendar.HOUR_OF_DAY, 48);
		// c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		Date z = c.getTime();
		// String zeroTime = format.format(z);
		// System.out.println(format.format(z));
		int result1 = inarrvtmdate.compareTo(z);// 进港航班计划到达时间和进港航班计划到达时间第二天的零点做比较
		int result2 = outdepttmdate.compareTo(z);// 出港航班计划起飞时间和进港航班计划到达时间第三天的零点做比较
		if (result1 <= 0 && result2 >= 0) {// 进港航班计划到达时间 小于 进港航班计划到达时间 并且 出港航班计划起飞时间 大于 进港航班计划到达时间 说明跨两夜
			return true;
		} else {
			return false;
		}

	}

	/**
	 * 计算中转停留时间差
	 * @throws ParseException
	 */

	public static long getTimeDiff(String outdepttm, String inarrvtm) throws ParseException {
		// SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		long diffhour = -1;
//		TimeZone.setDefault(TimeZone.getTimeZone("GMT+8"));
		SimpleDateFormat format0 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		if (!"".equals(outdepttm) && !"".equals(inarrvtm)) {
			long l = format0.parse(outdepttm).getTime() - format0.parse(inarrvtm).getTime();
			diffhour = (l / (60 * 60 * 1000));// 中转停留时间差
		}

		return diffhour;

	}

	/**
	 * 将获取的日期往后添加一天
	 */
	public static String addOneDay(String inarrvtm) throws ParseException {
		// SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		TimeZone.setDefault(TimeZone.getTimeZone("GMT+8"));
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH");
		Date inarrvtmdate = format.parse(inarrvtm);
		TimeZone curTimeZone = TimeZone.getTimeZone("GMT+8");
		Calendar c = Calendar.getInstance(curTimeZone);
		c.setTime(inarrvtmdate);
		c.set(Calendar.HOUR_OF_DAY, 24);
		// Date z = c.getTime();
		String date = format.format(c.getTime());
		String dString = date.substring(0, 10);
		return dString;

	}

	/*
	 * // 判断中转类型 public static TransferTypeEnum getTrangerType(String intype, String outtype, String
	 * infltnbr, String outfltnbr) { // 计算中转类型 LocalAirportFlightService localAirportFlightService =
	 * (LocalAirportFlightService) SpringUtils.getBean("localAirportFlightService");
	 *
	 * if(intype.equals("AIRCRAFT")) { intype="1"; }else { intype="2"; } if(outtype.equals("AIRCRAFT"))
	 * { outtype="1"; }else { outtype="2"; } //PageData pageData = new PageData(); String inflighttype =
	 * "";// 进港航班类型 String outflighttype = "";// 出港航班类型 TransferTypeEnum value2 = null;
	 * LocalAirportFlightQueryDTO localAirportFlightQueryDTO = new LocalAirportFlightQueryDTO();
	 * localAirportFlightQueryDTO.setPageSize(20); localAirportFlightQueryDTO.setPageNum(1); try { if
	 * ("1".equals(intype)) {// 进港类型为飞机 localAirportFlightQueryDTO.setFlightNo(infltnbr); //
	 * pageData.put("flight_no", infltnbr); PageRspDTO<LocalAirportFlightDTO> pData =
	 * localAirportFlightService .findLocalAirportFlightByParam(localAirportFlightQueryDTO); //
	 * 根据进港航班号查询进港航班类型 if (pData != null) { LocalAirportFlightDTO pd=pData.getList().get(0); String
	 * aa=pd.getFlightType(); // pData.getList().get(0).getFlightType().toString(); inflighttype =
	 * String.valueOf(pData.getList().get(0).getFlightType()); } else { // return "0";//查无此航班
	 * inflighttype = "D";// 查不到数据，默认为国内 }
	 *
	 * } if ("1".equals(outtype)) {// 出港类型为飞机 localAirportFlightQueryDTO.setFlightNo(infltnbr);
	 *
	 * PageRspDTO<LocalAirportFlightDTO> pData_d = localAirportFlightService
	 * .findLocalAirportFlightByParam(localAirportFlightQueryDTO); if (pData_d != null) { outflighttype
	 * = pData_d.getList().get(0).getFlightType(); } else { // return "01"; outflighttype = "D";//
	 * 查不到数据，默认为国内 }
	 *
	 * } if ("D".equals(inflighttype) && "D".equals(outflighttype) && "1".equals(intype) &&
	 * "1".equals(outtype)) { // pageMap.put("value2", "1");//1：国内国内空空 value2
	 * =INTERNAL_AIR_TO_INTERNAL_AIR ;
	 *
	 * } else if ("D".equals(inflighttype) && !"D".equals(outflighttype) && "1".equals(intype) &&
	 * "1".equals(outtype)) { // pageMap.put("value2", "2");//2：国内国际空空 value2 =
	 * INTERNAL_AIR_TO_ABROAD_AIR;
	 *
	 * } else if (!"D".equals(inflighttype) && "D".equals(outflighttype) && "1".equals(intype) &&
	 * "1".equals(outtype)) { // pageMap.put("value2", "3");//3：国际国内空空 value2 =
	 * ABROAD_AIR_TO_INTERNAL_AIR;
	 *
	 * } else if (!"D".equals(inflighttype) && !"D".equals(outflighttype) && "1".equals(intype) &&
	 * "1".equals(outtype)) { // pageMap.put("value2", "4");//4：国际国际空空 value2 =
	 * ABROAD_AIR_TO_ABROAD_AIR;
	 *
	 * } else if ("2".equals(intype) && "1".equals(outtype) && !"D".equals(outflighttype)) { //
	 * pageMap.put("value2", "5");//5：地面国际 value2 = GROUND_TO_ABROAD_AIR;
	 *
	 * } else if ("2".equals(intype) && "1".equals(outtype) && "D".equals(outflighttype)) { //
	 * pageMap.put("value2", "6");//6：地面国内 value2 = INTERNAL_AIR_TO_GROUND;
	 *
	 * } else if ("1".equals(intype) && !"D".equals(inflighttype) && "2".equals(outtype)) { //
	 * pageMap.put("value2", "7");//7：国际地面 value2 = ABROAD_AIR_TO_GROUND;
	 *
	 * } else if ("1".equals(intype) && "D".equals(inflighttype) && "2".equals(outtype)) { //
	 * pageMap.put("value2", "8");//8：国内地面 value2 = INTERNAL_AIR_TO_GROUND;
	 *
	 * } else { // pageMap.put("value2", "9");//9：地面地面 value2 = GROUND_TO_GROUND; }
	 *
	 * } catch (Exception e) {
	 *
	 * e.printStackTrace(); }
	 *
	 * return value2;
	 *
	 * }
	 */
	/***产生[0-max)范围的随机数**/
	public static int rand(int max) {
		return new Random().nextInt(max);
	}

	/***产生[min-max)范围的随机数
	*如果@param min>=max 直接返回 min
	*如果 min<max && min<0 则返回[0-max)范围的随机数
	*/
	public static int rand(int min, int max) {
		if (min < max) {
			if (min > 0) {
				return rand(max - min) + min;
			} else {
				return rand(max);
			}
		} else {
			return min;
		}
	}

	public static void main(String[] args) {
		System.out.println(getRandomNum());
	}

}
