package com.happy.app.appbase.common.utils;

public class CommonUtils {

	/**
	 *  执行业务开始
	 */
	public static String RZ_1 = "执行业务开始：";
	/**
	 *  执行业务结束
	 */
	public static String RZ_4 = "执行业务结束：";

	/**
	 *  调用接口开始
	 */
	public static String RZ_2 = "调用接口开始：";

	/**
	 *  调用接口结束
	 */
	public static String RZ_3 = "调用接口结束：";
	/**
	 * 业务操作
	 */
	public static String RZ_5 = "业务操作";

	/**
	 * token 延时时间
	 */
	public static Long TOKEN_EXPIRY_60 = 60L;
	/**
	 * 租户id为空-TENANTID_IS_NULL
	 */
	public static String TENANTID_IS_NULL_EN = "TENANTID_IS_NULL";

	/**
	 * 租户不能为空
	 */
	public static String TENANTID_IS_NULL_CN = "租户不能为空";
	/**
	 * PASSWORD"123456"
	 */
	public static String PASSWORD = "00000000";
	/**
	 * 用户应用 code
	 * 生产环境"AS003"
	 * 测试环境 BJFMUJ9W7Z1N
	 * 01  CXLX_SHXCX
	 */
	public static String APPLICATIONCODE = "AS003";

	/**
	 * 分页，pagenumber 最大值为 200
	 */
	public static Integer PAGE_MAXT_PAGENUMBER = 200;

	/**
	 * redis服务不可用
	 */
	public static String REDIS_MESSAGE_ERROR_EN = "redis is error";

	/**
	 * redis服务不可用
	 */
	public static String REDIS_MESSAGE_ERROR_CN = "redis服务不可用";
	/**
	 * 远程服务地址不可用，请稍后重试 HSFServiceAddressNotFoundException
	 */
	public static String HSF_MESSAGE_ERROR_EN = "HSFServiceAddressNotFoundException";

	/**
	 * 远程服务不可用，请稍后重试HSFServiceAddressNotFoundException
	 */
	public static String HSF_MESSAGE_ERROR_CN = "远程服务不可用，请稍后重试";

	/**
	 * 默认角色 管理员
	 */
	public static String ROLE_DEFAULT_CODE = "496138616573952";

	/**
	 * 默认部门 总部
	 */
	public static String DEPARTMENT_DEFAULT_CODE = "40322777781112832";

	/**
	 * 验证码失效或KEY不正确
	 */
	public static String AUTH_CODE_IS_ERROR_CN = "验证码不正确";

	/**
	 * 账号不存在
	 */
	public static String ACCOUNT_IS_NULL_CN = "账号不存在";
	/**
	 * 账号被锁定
	 */
	public static String ACCOUNT_IS_LOCK_CN = "账号被锁定";

	/**
	 * 账号id不能为空
	 */
	public static String ACCOUNT_ID_IS_NULL_CN = "账号id不能为空";

	/**
	 * 本地用户状态 正常 0
	 */
	public static Integer USER_STATUS_NORMAL = 0;
	/**
	 * 本地用户状态 锁定 -1
	 */
	public static Integer USER_STATUS_LOCK = -1;

	/**
	 * 模板类型 AC:验证码(auth code) SN:短信通知(short note) PS:推广短息(Promotion of SMS)
	 * VN：语音通知（voice notification） VC：语音验证码(Voice verification code)
	 */
	public static String MESSAGE_API_AC = "AC";
	/**
	 * 模板类型 AC:验证码(auth code) SN:短信通知(short note) PS:推广短息(Promotion of SMS)
	 * VN：语音通知（voice notification） VC：语音验证码(Voice verification code)
	 */
	public static String MESSAGE_API_SN = "SN";
	/**
	 * 模板类型 AC:验证码(auth code) SN:短信通知(short note) PS:推广短息(Promotion of SMS)
	 * VN：语音通知（voice notification） VC：语音验证码(Voice verification code)
	 */
	public static String MESSAGE_API_PS = "PS";
	/**
	 * 模板类型 AC:验证码(auth code) SN:短信通知(short note) PS:推广短息(Promotion of SMS)
	 * VN：语音通知（voice notification） VC：语音验证码(Voice verification code)
	 */
	public static String MESSAGE_API_VN = "VN";
	/**
	 * 模板类型 AC:验证码(auth code) SN:短信通知(short note) PS:推广短息(Promotion of SMS)
	 * VN：语音通知（voice notification） VC：语音验证码(Voice verification code)
	 */
	public static String MESSAGE_API_VC = "VC";

	public static String MESSAGE_API_AUTH_CODE = "AUTH_CODE";
	public static String MESSAGE_API_SHORT_NOTE = "SHORT_NOTE";
	public static String MESSAGE_API_PROMOTION_OF_SMS = "PROMOTION_OF_SMS";
	public static String MESSAGE_API_VOICDE_NOTIFICATION = "VOICDE_NOTIFICATION";
	public static String MESSAGE_API_VOICDE_VERIFICATION_CODE = "VOICDE_VERIFICATION_CODE";
	/**
	 * 账户 delete code Y 正常
	 */
	public static String ACCOUNT_DELETE_CODE_Y = "Y";

	/**
	 * 账户 delete code N 锁定
	 */
	public static String ACCOUNT_DELETE_CODE_N = "N";

	/**
	 * 账户 delete code D 删除
	 */
	public static String ACCOUNT_DELETE_CODE_D = "D";

	/**
	 * 账户 delete code 1 正常
	 */
	public static Integer ACCOUNT_DELETE_CODE_1 = 1;

	/**
	 * 账户 delete code 2 锁定
	 */
	public static Integer ACCOUNT_DELETE_CODE_2 = 2;

	/**
	 * 账户 delete code 3 删除
	 */
	public static Integer ACCOUNT_DELETE_CODE_3 = 3;

	/**
	 * 失败code 500
	 */
	public static String FAILED_CODE = "500";

	/**
	 * 成功 code 200
	 */
	public static String SUCCESS_CODE = "200";
	/**
	 * integer code 200
	 */
	public static Integer SUCCESS_CODE_INTEGER = 200;
	/**
	 * 工作人员 E
	 */
	public static String EMPLOYEE_TYPE_E = "E";
	/**
	 * 服务人员 S
	 */
	public static String EMPLOYEE_TYPE_S = "S";
	/**
	 * 旅客 P
	 */
	public static String EMPLOYEE_TYPE_P = "P";
	/**
	 * 商户 V
	 */
	public static String EMPLOYEE_TYPE_V = "V";
	/**
	 * 租户 T
	 */
	public static String EMPLOYEE_TYPE_T = "T";
	/**
	 * 是否单个查询 SELECT_SINGLE
	 */
	public static String SELECT_SINGLE = "SELECT_SINGLE";

	/**
	 * 是否集合查询 SELECT_COLLECTION
	 */
	public static String SELECT_COLLECTION = "SELECT_COLLECTION";

	/**
	 * 添加
	 */
	public static String ADD_CN = "添加";

	/**
	 * 修改
	 */
	public static String EDIT_CN = "修改";

	/**
	 * 删除
	 */
	public static String DELETE_CN = "删除";

	/**
	 * 成功
	 */
	public static String SUCCESS_CN = "成功";

	/**
	 * 失败
	 */
	public static String FAILED_CN = "失败";

	/**
	 * 缺少必需表单字段
	 */
	public static String PARAM_IS_NULL_CN = "缺少必需表单字段";
	/**
	 * 参数错误
	 */
	public static String PARAM_IS_ERROR_CN = "参数错误";
	/**
	 * 参数值不合法
	 */
	public static String PARAM_VALUE_ERROR = "参数值不合法";
	// /**
	// * 对象传化错误
	// */
	// public static String PARAM_IS_ERROR_CN = "参数错误";
	/**
	 * 性别 男
	 */
	public static String GENDER_MALE = "MALE";
	/**
	 * 性别 女
	 */
	public static String GENDER_FEMALE = "FEMALE";
	/**
	 * 性别 未知
	 */
	public static String GENDER_UNKNOW = "UNKNOW";

	/**
	 * 性别 男
	 */
	public static String GENDE_M = "M";
	/**
	 * 性别 女
	 */
	public static String GENDE_F = "F";
	/**
	 * 性别 未知
	 */
	public static String GENDE_U = "U";

	public static void main(String[] args) {
		System.out.println();
	}
}
