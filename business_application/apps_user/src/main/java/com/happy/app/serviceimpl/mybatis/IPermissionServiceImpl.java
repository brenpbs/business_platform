package com.happy.app.serviceimpl.mybatis;

import com.happy.app.dao.mapper.PermissionMapper;
import com.happy.app.entity.Permission;
import com.happy.app.service.mybatis.IPermissionService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 *
 */
@Service
public class IPermissionServiceImpl extends ServiceImpl<PermissionMapper, Permission>
		implements IPermissionService {

	@Autowired
	private PermissionMapper permissionMapper;;

	@Override
	public List<Permission> findByUserId(String userId) {

		return permissionMapper.findByUserId(userId);
	}

	@Override
	public List<Permission> findByRoleId(String roleId) {

		return permissionMapper.findByRoleId(roleId);
	}

	@Override
	public List<Permission> findByUserIdAndTenantId(String userId, String tenantId) {
		//
		return permissionMapper.findByUserIdAndTenantId(userId, tenantId);
	}

	@Override
	public List<Permission> findByRoleIdAndTenantId(String roleId, String tenantId) {
		//
		return permissionMapper.findByRoleIdAndTenantId(roleId, tenantId);
	}
}
