package com.happy.app.dao.mapper;

import com.happy.app.entity.Permission;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 *
 */
@Mapper
public interface PermissionMapper extends BaseMapper<Permission> {

	/**
	 * 通过用户id获取
	 * @param userId
	 * @return
	 */
	@Select("SELECT DISTINCT p.id, p.name, p.title, p.path, p.icon,		p.type, p.component,		p.level, p.button_type, p.parent_id, p.sort_order,		p.description,		p.status, p.url		FROM t_user u		LEFT JOIN t_user_role ur ON		u.id = ur.user_id		LEFT JOIN t_role_permission rp ON ur.role_id =		rp.role_id		LEFT JOIN t_permission p ON p.id = rp.permission_id		WHERE		u.id = #{userId} AND p.status = 0		ORDER BY p.sort_order ASC")
	List<Permission> findByUserId(@Param("userId") String userId);

	/**
	 * 通过roleId获取
	 * @param roleId
	 * @return
	 */
	@Select("SELECT p.id, p.name, p.title, p.path, p.icon, p.type,		p.component, p.level,		p.button_type, p.parent_id, p.sort_order,		p.description, p.status,		p.url		FROM  t_role_permission rp 		LEFT JOIN		t_permission p ON p.id = rp.permission_id		WHERE rp.role_id = #{roleId}		AND p.status = 0		ORDER BY p.sort_order ASC")
	List<Permission> findByRoleId(@Param("roleId") String roleId);

	/**
	 * 通过用户id和租户id获取
	 * @param userId tenantId
	 * @return
	 */
	@Select("SELECT DISTINCT p.id, p.name, p.title, p.path, p.icon,		p.type, p.component,		p.level, p.button_type, p.parent_id, p.sort_order,		p.description,		p.status, p.url	FROM t_user u		LEFT JOIN t_user_role ur ON		u.id = ur.user_id		LEFT JOIN t_role_permission rp ON ur.role_id =		rp.role_id		LEFT JOIN t_permission p ON p.id = rp.permission_id		WHERE		u.id = #{userId} AND rp.tenant_id=#{tenantId} AND p.status = 0		ORDER BY		p.sort_order ASC")
	List<Permission> findByUserIdAndTenantId(@Param("userId") String userId, @Param("tenantId") String tenantId);

	/**
	 * 通过roleId和租户id获取
	 * @param roleId  tenantId
	 * @return
	 */
	@Select("SELECT p.id, p.name, p.title, p.path, p.icon, p.type,		p.component, p.level,		p.button_type, p.parent_id, p.sort_order,		p.description, p.status,		p.url		FROM   t_role_permission rp		LEFT JOIN		t_permission p ON p.id = rp.permission_id		WHERE rp.role_id = #{roleId}		AND rp.tenant_id=#{tenantId} AND p.status = 0		ORDER BY p.sort_order ASC")
	List<Permission> findByRoleIdAndTenantId(@Param("roleId") String roleId, @Param("tenantId") String tenantId);
}
