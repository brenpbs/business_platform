package com.happy.app.appbase.common.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * 说明：日期处理工具类 创建人：renbo 修改时间：20170706
 */
public class DateUtil {

    private final static SimpleDateFormat sdfYear = new SimpleDateFormat("yyyy");
    private final static SimpleDateFormat sdfDay = new SimpleDateFormat("yyyy-MM-dd");
    private final static SimpleDateFormat sdfDays = new SimpleDateFormat("yyyyMMdd");
    private final static SimpleDateFormat sdfTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private final static SimpleDateFormat sdfTimes = new SimpleDateFormat("yyyyMMddHHmmss");

    public static final DateFormat haomiaoFormat = new SimpleDateFormat("yyyyMMddHHmmssSSS");
    public static final DateFormat ymdFormat = new SimpleDateFormat("yyyyMMdd");
    public static final DateFormat y_m_dFormat = new SimpleDateFormat("yyyy-MM-dd");
    public static final DateFormat y_m_d_h_m_sFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    public static final DateFormat y_m_d_h_mFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    public static final DateFormat ymdhmFormat = new SimpleDateFormat("yyyyMMddHHmm");
    public static final DateFormat ymdhmFormatYmdhms = new SimpleDateFormat("yyyyMMddHHmmss");
    public static final DateFormat y_mFormat = new SimpleDateFormat("yyyy-MM");
    public static final DateFormat yFormat = new SimpleDateFormat("yyyy");
    public static final DateFormat mFormat = new SimpleDateFormat("MM");
    public static final DateFormat h_m_sFormat = new SimpleDateFormat("HH:mm:ss");
    public static final DateFormat h_mFormat = new SimpleDateFormat("HH:mm");
    public static final DateFormat ymdwFormat = DateFormat.getDateInstance(DateFormat.FULL);
    public static final long MillisecondsPerDay = 24 * 60 * 60 * 1000;
    public static final long MillisecondsPerHour = 60 * 60 * 1000;
    public static final long MillisecondsPerMinute = 60 * 1000;

    /**
     * 获取当前时间 精确到秒 如2013-06-20 12:34:07
     */
    public static String getCurDateStrMiao_() {
        return y_m_d_h_m_sFormat.format(new Date());
    }
    /**
     * 字符串转换为指定格式字符串
     */
    public static String getStrFromDateFull(Date date) {
        String ymdStr = null;
        try {
            ymdStr = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss SSS").format(date);
        } catch (Exception e) {
            return "";
        }
        return ymdStr;
    }
    public static Date getDateFromDate(DateFormat format, Date oldDate) {
        Date date = null;
        if (format == null)
            return null;
        if (oldDate == null)
            return null;
        try {
            String strFromDateFull = getStrFromDateFull(oldDate);
            date = format.parse(strFromDateFull);
        } catch (Exception e) {
            return null;
        }
        return date;
    }
    public static Date getY_M_D_DateFromDate(Date date) throws Exception {
        SimpleDateFormat parserSDF = new SimpleDateFormat("yyyy-MM-dd");
        String format = parserSDF.format(date);
        Date parse = parserSDF.parse(format);
        return parse;
    }
    /**
     * 字符串转换为指定格式日期对象
     */
    public static Date getDateFromStr(String format, String datestr) {
        Date date = null;
        if (format == null || format.trim().equals(""))
            return null;
        if (datestr == null || datestr.trim().equals(""))
            return null;
        try {
            date = new SimpleDateFormat(format).parse(datestr);
        } catch (Exception e) {
            return null;
        }
        return date;
    }
    public static Date addDay(Date date) {
        if (date != null) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            cal.add(Calendar.DAY_OF_MONTH, 1);
            date = cal.getTime();
        }
        return date;
    }
        /**
     * 字符串转换为指定格式字符串
     */
    public static String getStrFromDate(Date date) {
        String ymdStr = null;
        try {
            ymdStr = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);
        } catch (Exception e) {
            return "";
        }
        return ymdStr;
    }
    public static String getDateStrY_m_d(Date date) {
        return y_m_dFormat.format(date);
    }

    public static Date addMouthAndSetOne(int num) {
        //获取前一个月第一天
        Calendar calendar1 = Calendar.getInstance();
        calendar1.add(Calendar.MONTH, num);
        calendar1.set(Calendar.DAY_OF_MONTH, 1);
        return calendar1.getTime();
    }

    /**
     * 获取YYYY格式
     *
     * @return
     */
    public static String getSdfTimes() {
        return sdfTimes.format(new Date());
    }

    /**
     * 获取YYYY格式
     *
     * @return
     */
    public static String getYear() {
        return sdfYear.format(new Date());
    }

    /**
     * 获取YYYY-MM-DD格式
     *
     * @return
     */
    public static String getDay() {
        return sdfDay.format(new Date());
    }

    /**
     * 获取YYYYMMDD格式
     *
     * @return
     */
    public static String getDays() {
        return sdfDays.format(new Date());
    }

    /**
     * 获取YYYY-MM-DD HH:mm:ss格式
     *
     * @return
     */
    public static String getTime() {
        return sdfTime.format(new Date());
    }

    /**
     * @param s
     * @param e
     * @return boolean
     * @throws @author fh
     * @Title: compareDate
     * @Description: (日期比较 ， 如果s > = e 返回true 否则返回false)
     */
    public static boolean compareDate(String s, String e) {
        if (fomatDate(s) == null || fomatDate(e) == null) {
            return false;
        }
        return fomatDate(s).getTime() >= fomatDate(e).getTime();
    }

    /**
     * 格式化日期
     *
     * @return
     */
    public static Date fomatDate(String date) {
        DateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
        try {
            return fmt.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 校验日期是否合法
     *
     * @return
     */
    public static boolean isValidDate(String s) {
        DateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
        try {
            fmt.parse(s);
            return true;
        } catch (Exception e) {
            // 如果throw java.text.ParseException或者NullPointerException，就说明格式不对
            return false;
        }
    }

    /**
     * @param startTime
     * @param endTime
     * @return
     */
    public static int getDiffYear(String startTime, String endTime) {
        DateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
        try {
            // long aa=0;
            int years = (int) (((fmt.parse(endTime).getTime() - fmt.parse(startTime).getTime()) / (1000 * 60 * 60 * 24))
                    / 365);
            return years;
        } catch (Exception e) {
            // 如果throw java.text.ParseException或者NullPointerException，就说明格式不对
            return 0;
        }
    }

    /**
     * <li>功能描述：时间相减得到天数
     *
     * @param beginDateStr
     * @param endDateStr
     * @return long
     * @author Administrator
     */
    public static long getDaySub(String beginDateStr, String endDateStr) {
        long day = 0;
        java.text.SimpleDateFormat format = new java.text.SimpleDateFormat("yyyy-MM-dd");
        java.util.Date beginDate = null;
        java.util.Date endDate = null;

        try {
            beginDate = format.parse(beginDateStr);
            endDate = format.parse(endDateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        day = (endDate.getTime() - beginDate.getTime()) / (24 * 60 * 60 * 1000);
        // System.out.println("相隔的天数="+day);

        return day;
    }

    /**
     * 得到n天之后的日期
     *
     * @param days
     * @return
     */
    public static String getAfterDayDate(String days) {
        int daysInt = Integer.parseInt(days);

        Calendar canlendar = Calendar.getInstance(); // java.util包
        canlendar.add(Calendar.DATE, daysInt); // 日期减 如果不够减会将月变动
        Date date = canlendar.getTime();

        SimpleDateFormat sdfd = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dateStr = sdfd.format(date);

        return dateStr;
    }

    /**
     * 得到n天之后是周几
     *
     * @param days
     * @return
     */
    public static String getAfterDayWeek(String days) {
        int daysInt = Integer.parseInt(days);
        Calendar canlendar = Calendar.getInstance(); // java.util包
        canlendar.add(Calendar.DATE, daysInt); // 日期减 如果不够减会将月变动
        Date date = canlendar.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("E");
        String dateStr = sdf.format(date);
        return dateStr;
    }

    public static String subStr(String str) {
        return str.substring(0, str.length() - 2);
    }

    /**
     * 获取时间 小时:分;秒 HH:mm:ss
     *
     * @return
     */
    public static String getTimeShort() {
        SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");
        Date currentTime = new Date();
        String dateString = formatter.format(currentTime);
        return dateString;
    }

    /**
     * 获取现在时间
     *
     * @return 返回短时间字符串格式yyyy-MM-dd
     */
    public static String getStringDateShort() {
        Date currentTime = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String dateString = formatter.format(currentTime);
        return dateString;
    }

    public static void main(String[] args) {
        System.out.println(getStringDateShort());
        // System.out.println(getAfterDayWeek("3"));
    }

    public static Date addDateMinut(Date day, int hour) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = day;
        if (date == null)
            return null;
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.HOUR, hour);// 24小时制
        date = cal.getTime();
        cal = null;
        return date;
    }

}
