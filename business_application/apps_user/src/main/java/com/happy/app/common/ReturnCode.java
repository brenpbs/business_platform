package com.happy.app.common;

import com.happy.ipss.dto.CommonRspDTO;
import com.happy.ipss.dto.PageRspDTO;
import lombok.Data;

/**
 * 统一返回类
 *
 * @author zhangchongyang
 */
@Data
public class ReturnCode extends CommonRspDTO {

    /**
     * 返回数据
     */
    private Object data;

    public ReturnCode() {
        super("8200", "成功");
    }

    public ReturnCode(String code, String message) {
        super(code, message);
    }

    public ReturnCode(CommonRspDTO commonRspDTO) {
        this(commonRspDTO.getCode(), commonRspDTO.getMessage());
    }

    public ReturnCode(CommonRspDTO commonRspDTO, Object data) {
        this(commonRspDTO.getCode(), commonRspDTO.getMessage());
        this.data = data;
    }

    public ReturnCode(PageRspDTO pageRspDTO) {
        this(pageRspDTO.getCode(), pageRspDTO.getMessage(), pageRspDTO);
    }

    public ReturnCode(String code, String message, Object data) {
        this(code, message);
        this.data = data;
    }

    public static ReturnCode success() {
        return new ReturnCode(CommonRspDTO.success());
    }

    public static ReturnCode success(Object data) {
        return new ReturnCode(CommonRspDTO.success(), data);
    }
}
