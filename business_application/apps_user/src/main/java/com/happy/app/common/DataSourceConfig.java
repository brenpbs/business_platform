//package com.cwag.pss.app.common;
//
//import java.util.Map;
//
//import javax.persistence.EntityManager;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Qualifier;
//import org.springframework.boot.autoconfigure.orm.jpa.HibernateSettings;
//import org.springframework.boot.autoconfigure.orm.jpa.JpaProperties;
//import org.springframework.boot.context.properties.ConfigurationProperties;
//import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.annotation.Primary;
//import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
//import org.springframework.orm.jpa.JpaTransactionManager;
//import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
//import org.springframework.transaction.PlatformTransactionManager;
//import org.springframework.transaction.annotation.EnableTransactionManagement;
//
//import com.taobao.txc.datasource.cobar.TxcDataSource;
//
///**
// * 2019-01-10 12:03
// *
// * @author yangk
// **/
//@Configuration
//@EnableTransactionManagement
//@EnableJpaRepositories(entityManagerFactoryRef = "entityManagerFactoryPrimary", transactionManagerRef = "transactionManagerPrimary", basePackages = {
//		"com.cwag.pss.ipss.member.provider.repository" })
//public class DataSourceConfig {
//
//	@Bean("txcDataSource")
//	@ConfigurationProperties(prefix = "spring.datasource")
//	public TxcDataSource txcDataSource() {
//		return new TxcDataSource();
//	}
//
//	@Primary
//	@Bean(name = "entityManagerPrimary")
//	public EntityManager entityManager(EntityManagerFactoryBuilder builder,
//			@Qualifier("txcDataSource") TxcDataSource txcDataSource) {
//		return entityManagerFactoryPrimary(builder, txcDataSource).getObject()
//				.createEntityManager();
//	}
//
//	@Primary
//	@Bean(name = "entityManagerFactoryPrimary")
//	public LocalContainerEntityManagerFactoryBean entityManagerFactoryPrimary(
//			EntityManagerFactoryBuilder builder, TxcDataSource txcDataSource) {
//		return builder.dataSource(txcDataSource).properties(getVendorProperties())
//				// 设置实体类所在位置
//				.packages("com.cwag.pss.ipss.member.provider.po")
//				.persistenceUnit("primaryPersistenceUnit").build();
//	}
//
//	@Autowired
//	private JpaProperties jpaProperties;
//
//	private Map getVendorProperties() {
//		HibernateSettings hibernateSettings = new HibernateSettings();
//		hibernateSettings.ddlAuto(() -> "validate");
//		jpaProperties.setShowSql(true);
//		return jpaProperties.getHibernateProperties(hibernateSettings);
//	}
//
//	@Primary
//	@Bean(name = "transactionManagerPrimary")
//	public PlatformTransactionManager transactionManagerPrimary(EntityManagerFactoryBuilder builder,
//			@Qualifier("txcDataSource") TxcDataSource txcDataSource) {
//		return new JpaTransactionManager(
//				entityManagerFactoryPrimary(builder, txcDataSource).getObject());
//	}
//}
