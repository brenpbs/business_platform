package com.happy.app.dao;

import com.happy.app.appbase.base.AppBaseDao;
import com.happy.app.entity.Department;

import java.util.List;

/**
 * 部门数据处理层
 *
 * @author Exrick
 */
public interface DepartmentDao extends AppBaseDao<Department, String> {

//	/**
//	 * 通过父id获取 升序
//	 *
//	 * @param parentId
//	 * @return
//	 */
//	List<Department> findByParentIdAndTenantIdOrderBySortOrder(String parentId, String tenantId);

	/**
	 * 通过父id和状态获取 升序
	 *
	 * @param parentId
	 * @param status
	 * @return
	 */
	List<Department> findByParentIdAndTenantIdAndStatusOrderBySortOrder(String parentId,
			String tenantId, Integer status);

	/**
	 * 通过父id和租户id获取 升序
	 *
	 * @param parentId
	 * @param status
	 * @return
	 */
	List<Department> findByParentIdAndTenantIdOrderBySortOrder(String parentId, String tenantId);

	/**
	 * 通过id和租户id获取
	 *
	 * @param id
	 * @param tenantId
	 * @return
	 */
	Department findByIdAndTenantId(String id, String tenantId);

}
