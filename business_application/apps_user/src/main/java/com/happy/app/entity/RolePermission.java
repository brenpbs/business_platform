package com.happy.app.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.baomidou.mybatisplus.annotations.TableName;
import com.happy.app.appbase.base.AppBaseEntity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author Exrick
 */
@Data
@Entity
@Table(name = "t_role_permission")
@TableName("t_role_permission")
public class RolePermission extends AppBaseEntity {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "租户id")
	private String tenantId;

	@ApiModelProperty(value = "角色id")
	private String roleId;

	@ApiModelProperty(value = "权限id")
	private String permissionId;
}
