package com.happy.app.entity.sub;

import java.util.List;

import com.happy.app.entity.Permission;
import com.happy.app.entity.Role;

import com.happy.ipss.user.api.dto.employee.EmployeePageResultDTO;
import lombok.Data;

@Data
public class SubEmployeePageResultDTO extends EmployeePageResultDTO {

	private String lastLoginTimeStr;
	private String updateTimeStr;
	private String createTimeStr;
	private List<Permission> permissions;
	private String departmentTitle;
	private String departmentId;
	private List<Role> roles;
//	EmployeePageResultDTO parentDto;
//
//	public SubEmployeePageResultDTO(EmployeePageResultDTO parentDto) {
//		super();
//		this.parentDto = parentDto;
//		//
//	}

}
