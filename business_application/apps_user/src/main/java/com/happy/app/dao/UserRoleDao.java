package com.happy.app.dao;

import com.happy.app.appbase.base.AppBaseDao;
import com.happy.app.entity.UserRole;

import java.util.List;

/**
 * 用户角色数据处理层
 *
 */
public interface UserRoleDao extends AppBaseDao<UserRole,String> {

    /**
     * 通过roleId查找
     * @param roleId
     * @return
     */
    List<UserRole> findByRoleId(String roleId);

    /**
     * 删除用户角色
     * @param userId
     */
    void deleteByUserId(String userId);
}
