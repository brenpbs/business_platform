package com.happy.app.appbase.common.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.TimeZone;

public class ToolsUtil {

	private static final String date1 = "11:00"; //饭点参数
	private static final String date2 = "13:00"; //饭点参数
	private static final String date3 = "17:00"; //饭点参数
	private static final String date4 = "19:00"; //饭点参数
	private static final String date5 = "05:00"; //早上优享时间参数
	private static final String date6 = "05:55"; //早上优享时间参数

	/**
	 * 随机生成四位数验证码
	 * @return
	 */
	public static int getRandomNum(){
		 Random r = new Random();
		 return r.nextInt(9000)+1000;//(Math.random()*(9999-1000)+1000)
	}

	/**
	 * 计算中转停留时间差
	 * @throws ParseException
	 */

	public static long getTimeDiff(String outdepttm,String inarrvtm ) throws ParseException{
		//SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		long diffhour=-1;
		TimeZone.setDefault(TimeZone.getTimeZone("GMT+8"));
		SimpleDateFormat format0 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		if(!"".equals(outdepttm) && !"".equals(inarrvtm)){
			long l=format0.parse(outdepttm).getTime()-format0.parse(inarrvtm).getTime();
			 diffhour=(l/(60*60*1000));//中转停留时间差
		}

		return diffhour;

	}



	/**
	 * 计算离港航班起飞时间是否在早上5:00-5:55之间
	 * @throws ParseException
	 */

	public static boolean isZHYXTime(String outdepttm) throws ParseException{
		SimpleDateFormat format = new SimpleDateFormat("HH:mm");
		String param2[]=outdepttm.split(" ");
	    String outdepttmparam = param2[1];
	    Date outtime = format.parse(outdepttmparam);
	    Date date5 = format.parse(ToolsUtil.date5);//05:00
	    Date date6 = format.parse(ToolsUtil.date6);//05:55
	    if(outtime.compareTo(date5)>=0 && outtime.compareTo(date6)<=0){
	    	return true;
	    }else{
	    	return false;
	    }

	}
	/**
	 * 判断是否在饭点
	 * 分三种情况不 隔夜和隔一夜和隔两夜
	 * 不隔夜时 分为三种情况 1: 进港时间 <= 11:00 且  离岗时间 >= 11:00, 2: 11:00 <= 进港时间  <= 13:00, 3: 13:00<=进港时间 <=19:00 且  离岗时间 >=17:00
	 * 隔一夜时分为三种情况 1： 进港时间 <=11:00 , 2: 11:00 <= 进港时间 <= 19:00, 3: 进港时间 >= 19:00 且 离岗时间 >= 11:00
	 * 隔两夜返回true
	 * @throws ParseException
	 */

	public static boolean iseat(String outdepttm,String inarrvtm,boolean isgoday,boolean istowday) throws ParseException{
		SimpleDateFormat format = new SimpleDateFormat("HH:mm");
		TimeZone.setDefault(TimeZone.getTimeZone("GMT+8"));
		String param[]=inarrvtm.split(" ");
	    String inarrvtmparam = param[1];
	    String param2[]=outdepttm.split(" ");
	    String outdepttmparam = param2[1];
 	    Date arrivetime = format.parse(inarrvtmparam);
	    Date outtime = format.parse(outdepttmparam);
	    Date date1 = format.parse(ToolsUtil.date1);//11:00
	    Date date2 = format.parse(ToolsUtil.date2);//13:00
	    Date date3 = format.parse(ToolsUtil.date3);//17:00
	    Date date4 = format.parse(ToolsUtil.date4);//19:00
		if(!isgoday){
			//不隔夜
		    if((arrivetime.compareTo(date1)<=0 && outtime.compareTo(date1)>=0) || (arrivetime.compareTo(date1)>=0 && arrivetime.compareTo(date2)<=0) || (arrivetime.compareTo(date2)>=0 && arrivetime.compareTo(date4)<=0 && outtime.compareTo(date3)>=0)){
		    	return true;
		    }else{
		    	return false;
		    }
		}else{
			//隔夜
			if(istowday){//是否隔两夜
				return true;
			}else{
				if((arrivetime.compareTo(date1)<=0) || (arrivetime.compareTo(date1)>=0 && arrivetime.compareTo(date4)<=0) || (arrivetime.compareTo(date4)>=0 && outtime.compareTo(date1)>=0)){
					return true;
				}else{
					return false;
				}
			}
		}

	}

	/**
	 * 判断是否可以享受两顿免餐食(西宁机场)
	 * 分为两种情况隔夜和不隔夜
	 * 不隔夜：进港时间<=13:00 且 离港时间>=17:00
	 * 隔夜分为：1   进港时间<13:00  2 13:00<进港时间<19:00 且 离岗时间>11:00  3:进港时间>19:00 且 离港时间 >17:00
	 * @throws ParseException
	 */
	public static int isEatTwo(String outdepttm,String inarrvtm ) throws ParseException{
		SimpleDateFormat format = new SimpleDateFormat("HH:mm");
		TimeZone.setDefault(TimeZone.getTimeZone("GMT+8"));
		String param[]=inarrvtm.split(" ");
	    String inarrvtmparam = param[1];
	    String param2[]=outdepttm.split(" ");
	    String outdepttmparam = param2[1];
 	    Date arrivetime = format.parse(inarrvtmparam);
	    Date outtime = format.parse(outdepttmparam);
	    Date date1 = format.parse(ToolsUtil.date1);//11:00
	    Date date2 = format.parse(ToolsUtil.date2);//13:00
	    Date date3 = format.parse(ToolsUtil.date3);//17:00
	    Date date4 = format.parse(ToolsUtil.date4);//19:00
	    boolean isgoday = isgoday(outdepttm,inarrvtm);
	    if(!isgoday){
	    	if(arrivetime.compareTo(date2)<=0 && outtime.compareTo(date3)>=0){
	    		return 2;
	    	}else{
	    		return 1;
	    	}
	    }else{
	    	if(arrivetime.compareTo(date2)<=0 || (arrivetime.compareTo(date2)>=0 && arrivetime.compareTo(date4)<=0 && outtime.compareTo(date1)>=0) || (arrivetime.compareTo(date4)>=0 && outtime.compareTo(date3)>=0)){
	    		return 2;
	    	}else{
	    		return 1;
	    	}
	    }

	}

	/**
	 * 计算是否隔夜
	 */

	public static boolean isgoday(String outdepttm,String inarrvtm ) throws ParseException{
			//SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH");
		 	Date inarrvtmdate = format.parse(inarrvtm);
	        Date outdepttmdate = format.parse(outdepttm);
	        TimeZone curTimeZone = TimeZone.getTimeZone("GMT+8");
	        Calendar c = Calendar.getInstance(curTimeZone);
	        c.setTime(inarrvtmdate);
	        c.set(Calendar.HOUR_OF_DAY,24);
	        //c.set(Calendar.HOUR_OF_DAY, 0);
	        c.set(Calendar.MINUTE, 0);
	        c.set(Calendar.SECOND, 0);
	        Date z = c.getTime();
	        //String zeroTime = format.format(z);
	        //System.out.println(format.format(z));
	        int result1 = inarrvtmdate.compareTo(z) ;//进港航班计划到达时间和进港航班计划到达时间第二天的零点做比较
	        int result2 = outdepttmdate.compareTo(z);//出港航班计划起飞时间和进港航班计划到达时间第二天的零点做比较
	        if(result1 <= 0 && result2 >= 0){//进港航班计划到达时间  小于  进港航班计划到达时间   并且  出港航班计划起飞时间   大于  进港航班计划到达时间  说明跨夜
	        	return true;
	        }else{
	        	return false;
	        }

	}

	/**
	 * 计算是否隔两夜
	 */
	public static boolean isgotowday(String outdepttm,String inarrvtm ) throws ParseException{
		//SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH");
	 	Date inarrvtmdate = format.parse(inarrvtm);
        Date outdepttmdate = format.parse(outdepttm);
        TimeZone curTimeZone = TimeZone.getTimeZone("GMT+8");
        Calendar c = Calendar.getInstance(curTimeZone);
        c.setTime(inarrvtmdate);
        c.set(Calendar.HOUR_OF_DAY,48);
        //c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        Date z = c.getTime();
        //String zeroTime = format.format(z);
        //System.out.println(format.format(z));
        int result1 = inarrvtmdate.compareTo(z) ;//进港航班计划到达时间和进港航班计划到达时间第二天的零点做比较
        int result2 = outdepttmdate.compareTo(z);//出港航班计划起飞时间和进港航班计划到达时间第三天的零点做比较
        if(result1 <= 0 && result2 >= 0){//进港航班计划到达时间  小于  进港航班计划到达时间   并且  出港航班计划起飞时间   大于  进港航班计划到达时间  说明跨两夜
        	return true;
        }else{
        	return false;
        }

	}


	/**
	 * 将获取的日期往后添加一天
	 */
	public static String addOneDay(String inarrvtm ) throws ParseException{
		//SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		TimeZone.setDefault(TimeZone.getTimeZone("GMT+8"));
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH");
	 	Date inarrvtmdate = format.parse(inarrvtm);
        TimeZone curTimeZone = TimeZone.getTimeZone("GMT+8");
        Calendar c = Calendar.getInstance(curTimeZone);
        c.setTime(inarrvtmdate);
        c.set(Calendar.HOUR_OF_DAY,24);
       // Date z = c.getTime();
		String date = format.format(c.getTime());
		String dString = date.substring(5, 10);
		return dString;


	}



	/**
	 * 将获取的日期往前提前一天
	 */
	public static String backOneDay(String inarrvtm ) throws ParseException{
		TimeZone.setDefault(TimeZone.getTimeZone("GMT+8"));
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH");
	 	Date inarrvtmdate = format.parse(inarrvtm);
        TimeZone curTimeZone = TimeZone.getTimeZone("GMT+8");
        Calendar c = Calendar.getInstance(curTimeZone);
        c.setTime(inarrvtmdate);
        c.set(Calendar.HOUR_OF_DAY,-24);
       // Date z = c.getTime();
		String date = format.format(c.getTime());
		String dString = date.substring(0, 10);
		return dString;


	}





}
