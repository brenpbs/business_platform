package com.happy.app.controller.manage;

import com.happy.app.appbase.common.utils.CommonUtils;
import com.happy.app.appbase.common.utils.PageUtil;
import com.happy.app.appbase.common.utils.ResultUtil;
import com.happy.app.appbase.common.vo.PageVo;
import com.happy.app.appbase.common.vo.Result;
import com.happy.app.entity.Permission;
import com.happy.app.entity.Role;
import com.happy.app.entity.RolePermission;
import com.happy.app.entity.UserRole;
import com.happy.app.service.RolePermissionService;
import com.happy.app.service.RoleService;
import com.happy.app.service.UserRoleService;
import com.happy.app.service.mybatis.IPermissionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.lang.Nullable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

/**
 *
 */
@CrossOrigin
@RestController
@Api(description = "角色管理接口")
@RequestMapping("/app/role")
@Transactional
public class RoleController {

	@Autowired
	private RoleService roleService;

	@Autowired
	private UserRoleService userRoleService;

	@Autowired
	private RolePermissionService rolePermissionService;

	@Autowired
	private IPermissionService iPermissionService;


	@RequestMapping(value = "/getAllList", method = RequestMethod.GET)
	@ApiOperation(value = "获取全部角色")
	public Result<Object> roleGetAll(String tenantId) {
		if (StringUtils.isEmpty(tenantId)) {
			return new ResultUtil<Object>().setErrorMsg(CommonUtils.TENANTID_IS_NULL_EN);
		}
//		List<Role> list = roleService.getAll();
		List<Role> list = roleService.findByTenantId(tenantId);
		return new ResultUtil<Object>().setData(list);
	}

	@RequestMapping(value = "/getAllByPage", method = RequestMethod.GET)
	@ApiOperation(value = "分页获取角色")
	public Result<Page<Role>> getRoleByPage(@ModelAttribute PageVo page, String tenantId) {
		if (StringUtils.isEmpty(tenantId)) {
			return new ResultUtil<Page<Role>>().setErrorMsg(CommonUtils.TENANTID_IS_NULL_EN);
		}
//		Page<Role> list = roleService.findAll(PageUtil.initPage(page));
		// -----------------------------------
		Page<Role> list = roleService.findAll(new Specification<Role>() {
			@Nullable
			@Override
			public Predicate toPredicate(Root<Role> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {

				Path<String> tenantIdField = root.get("tenantId");

				List<Predicate> list = new ArrayList<Predicate>();

				list.add(cb.equal(tenantIdField, tenantId));

				Predicate[] arr = new Predicate[list.size()];
				cq.where(list.toArray(arr));
				return null;
			}
		}, PageUtil.initPage(page));
		// -----------------------------------
		for (Role role : list.getContent()) {
			List<Permission> permissions = iPermissionService.findByRoleIdAndTenantId(role.getId(),
					tenantId);
			role.setPermissions(permissions);
		}
		return new ResultUtil<Page<Role>>().setData(list);
	}

	@RequestMapping(value = "/setDefault", method = RequestMethod.POST)
	@ApiOperation(value = "设置或取消默认角色")
	public Result<Object> setDefault(@RequestParam String id, @RequestParam Boolean isDefault,
			String tenantId) {
		if (StringUtils.isEmpty(tenantId)) {
			return new ResultUtil<Object>().setErrorMsg(CommonUtils.TENANTID_IS_NULL_EN);
		}
		Role role = roleService.get(id);
		if (role == null) {
			return new ResultUtil<Object>().setErrorMsg("角色不存在");
		}
		role.setDefaultRole(isDefault);
		roleService.update(role);
		return new ResultUtil<Object>().setSuccessMsg("设置成功");
	}

	@RequestMapping(value = "/editRolePerm", method = RequestMethod.POST)
	@ApiOperation(value = "编辑角色分配权限")
	public Result<Object> editRolePerm(String roleId,
			@RequestParam(required = false) String[] permIds, String tenantId) {
		if (StringUtils.isEmpty(roleId)) {
			return new ResultUtil<Object>().setErrorMsg("角色id不能为空");
		}
		if (StringUtils.isEmpty(tenantId)) {
			return new ResultUtil<Object>().setErrorMsg(CommonUtils.TENANTID_IS_NULL_EN);
		}
		// 删除其关联权限
		rolePermissionService.deleteByRoleId(roleId);
		// 分配新权限
		for (String permId : permIds) {
			RolePermission rolePermission = new RolePermission();
			rolePermission.setRoleId(roleId);
			rolePermission.setPermissionId(permId);
			rolePermission.setTenantId(tenantId);
			rolePermissionService.save(rolePermission);
		}
		return new ResultUtil<Object>().setData(null);
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@ApiOperation(value = "保存数据")
	public Result<Role> save(@ModelAttribute Role role) {
		if (StringUtils.isEmpty(role.getTenantId())) {
			return new ResultUtil<Role>().setErrorMsg(CommonUtils.TENANTID_IS_NULL_EN);
		}
		Role r = roleService.save(role);
		return new ResultUtil<Role>().setData(r);
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST)
	@ApiOperation(value = "更新数据")
	public Result<Role> edit(@ModelAttribute Role entity) {
		if (StringUtils.isEmpty(entity.getTenantId())) {
			return new ResultUtil<Role>().setErrorMsg(CommonUtils.TENANTID_IS_NULL_EN);
		}
		Role r = roleService.update(entity);
		return new ResultUtil<Role>().setData(r);
	}

	@RequestMapping(value = "/delAllByIds", method = RequestMethod.DELETE)
	@ApiOperation(value = "批量通过ids删除")
	public Result<Object> delByIds(String ids, String tenantId) {
		if (ids == null || "".equals(ids)) {
			return new ResultUtil<Object>().setErrorMsg("角色id不能为空");
		}
		if (StringUtils.isEmpty(tenantId)) {
			return new ResultUtil<Object>().setErrorMsg(CommonUtils.TENANTID_IS_NULL_EN);
		}
		String[] ids2 = ids.split(",");
		for (String id : ids2) {
			List<UserRole> list = userRoleService.findByRoleId(id);
			if (list != null && list.size() > 0) {
				return new ResultUtil<Object>().setErrorMsg("删除失败，包含正被用户使用关联的角色");
			}
		}
		for (String id : ids2) {
			roleService.delete(id);
			// 删除关联权限
			rolePermissionService.deleteByRoleId(id);
		}
		return new ResultUtil<Object>().setSuccessMsg("批量通过id删除数据成功");
	}

}
