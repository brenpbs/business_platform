package com.happy.app.controller.api.service;

import com.happy.app.appbase.common.utils.CommonUtils;
import com.happy.app.appbase.common.utils.ExceptionUtils;
import com.happy.app.appbase.common.utils.ResultUtil;
import com.happy.app.appbase.common.vo.Result;
import com.happy.ipss.user.api.dto.LoginByAccountDTO;
import com.happy.ipss.user.api.dto.employee.EmployeeInfoDTO;
import com.happy.ipss.user.api.service.EmployeeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@Service
@Transactional
public class CommonService {
    @Value("${appcode.applicationCode}")
    private String applicationCode;
    @Value("${appcode.messageAppCode}")
    private String messageAppCode;
    @Value("${appcode.messageCode}")
    private String messageCode;

    @Autowired
    private EmployeeService employeeService;

    /**
     * 工作人员登录-账号密码方式
     *
     * @param accountName
     * @param password
     * @return
     */
    public Result<Object> loginByAccount(LoginByAccountDTO loginDto, String accountType, Boolean saveLogin, String runum) {
        Map<String, String> resMap = new HashMap<String, String>();
        String apiToken = "";
        String accountId = "";
        log.info("mp : 流水号 : " + runum + ",业务处理: 调用中台 loginByLoginName begin" + " 数据： " + "");
        try {
            if (CommonUtils.EMPLOYEE_TYPE_E.equals(accountType)) {
                apiToken = employeeService.loginByLoginName(loginDto);
                accountId = employeeService.authToken(apiToken, CommonUtils.TOKEN_EXPIRY_60);
                EmployeeInfoDTO info = employeeService.findInfo(accountId);
                resMap.put("nameCn", info.getEmployeeDTO().getEmployeeName());
                resMap.put("phone", info.getPhone());
            } else {
                return new ResultUtil<Object>().setErrorMsg(CommonUtils.PARAM_VALUE_ERROR);
            }
        } catch (Exception e) {
            return ExceptionUtils.checkExceptionMessage(e);
        }
        log.info("mp : 流水号 : " + runum + ",业务处理: 调用中台 loginByLoginName end" + " 数据： " + apiToken);
        resMap.put("accountId", accountId);
        resMap.put("accessToken", apiToken);
        resMap.put("accessType", accountType);
        resMap.put("accessName", loginDto.getLoginName());
        return new ResultUtil<Object>().setData(resMap);
    }

}
