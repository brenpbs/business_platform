package com.happy.app.service.mybatis;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.service.IService;
import com.happy.app.entity.Permission;

/**
 *
 */
public interface IPermissionService extends IService<Permission> {

	/**
	 * 通过用户id获取
	 * @param userId
	 * @return
	 */
	List<Permission> findByUserId(String userId);

	/**
	 * 通过roleId获取
	 * @param roleId
	 * @return
	 */
	List<Permission> findByRoleId(@Param("roleId") String roleId);

	/**
	 * 通过用户id和租户id获取
	 * @param userId tenantId
	 * @return
	 */
	List<Permission> findByUserIdAndTenantId(@Param("userId") String userId,
			@Param("tenantId") String tenantId);

	/**
	 * 通过roleId和租户id获取
	 * @param roleId  tenantId
	 * @return
	 */
	List<Permission> findByRoleIdAndTenantId(@Param("roleId") String roleId,
			@Param("tenantId") String tenantId);
}
