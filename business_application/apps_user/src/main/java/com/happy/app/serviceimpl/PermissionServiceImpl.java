package com.happy.app.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.happy.app.dao.PermissionDao;
import com.happy.app.entity.Permission;
import com.happy.app.service.PermissionService;

/**
 * 权限接口实现
 * @author Exrick
 */
@Service
@Transactional
public class PermissionServiceImpl implements PermissionService {

	@Autowired
	private PermissionDao permissionDao;

	@Override
	public PermissionDao getRepository() {
		return permissionDao;
	}

	@Override
	public List<Permission> findByLevelOrderBySortOrder(Integer level) {

		return permissionDao.findByLevelOrderBySortOrder(level);
	}

	@Override
	public List<Permission> findByParentIdAndTenantIdOrderBySortOrder(String parentId, String tenantId) {

		return permissionDao.findByParentIdAndTenantIdOrderBySortOrder(parentId, tenantId);
	}

	@Override
	public List<Permission> findByTypeAndStatusOrderBySortOrder(Integer type, Integer status) {

		return permissionDao.findByTypeAndStatusOrderBySortOrder(type, status);
	}

	@Override
	public List<Permission> findByTitle(String title) {

		return permissionDao.findByTitle(title);
	}

	@Override
	public List<Permission> findByLevelAndTenantIdOrderBySortOrder(Integer level, String tenantId) {
		//
		return permissionDao.findByLevelAndTenantIdOrderBySortOrder(level, tenantId);
	}
}
