package com.happy.app.service.mybatis;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.service.IService;
import com.happy.app.entity.Role;
import com.happy.app.entity.UserRole;

/**
 *
 */
public interface IUserRoleService extends IService<UserRole> {

	/**
	 * 通过用户id和tenantId获取
	 * @param userId tenantId
	 * @return
	 */
	List<Role> findByUserIdAndTenantId(@Param("userId") String userId, @Param("tenantId") String tenantId);
}
