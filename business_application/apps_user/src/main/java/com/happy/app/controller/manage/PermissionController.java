package com.happy.app.controller.manage;

import java.util.ArrayList;
import java.util.List;

import com.happy.app.appbase.base.BaseController;
import com.happy.app.appbase.common.constant.CommonConstant;
import com.happy.app.appbase.common.utils.CommonUtils;
import com.happy.app.appbase.common.utils.PageData;
import com.happy.app.appbase.common.utils.ResultUtil;
import com.happy.app.appbase.common.utils.Tools;
import com.happy.app.appbase.common.vo.Result;
import com.happy.app.entity.Permission;
import com.happy.app.entity.RolePermission;
import com.happy.app.service.PermissionService;
import com.happy.app.service.RolePermissionService;
import com.happy.app.service.mybatis.IPermissionService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.dubbo.rpc.RpcContext;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * @author Exrick
 */
@Slf4j
@CrossOrigin
@RestController
@Api(description = "菜单/权限管理接口")
@RequestMapping("/app/permission")
@Transactional
public class PermissionController extends BaseController {

	@Autowired
	private PermissionService permissionService;

	@Autowired
	private RolePermissionService rolePermissionService;

	@Autowired
	private IPermissionService iPermissionService;

	@RequestMapping(value = "/getMenuList", method = RequestMethod.GET)
	@ApiOperation(value = "获取用户页面菜单数据")
	public Result<List<Permission>> getAllMenuList(String userId, String tenantId) {
		PageData pd = new PageData();
		pd = this.getPageData();
		String runum = pd.getString("runum");
		runum = Tools.getRandomNumBaCheckNull(runum);
		RpcContext.getContext().setAttachment("busSeq", runum);
		log.info("mp : 流水号 : " + runum + ",业务请求: 获取用户页面菜单数据" + " 数据： " + JSONObject.fromObject(pd));
		if (StringUtils.isEmpty(userId)) {
			return new ResultUtil<List<Permission>>().setErrorMsg("用户id不能为空");
		}
		if (StringUtils.isEmpty(tenantId)) {
			return new ResultUtil<List<Permission>>().setErrorMsg(CommonUtils.TENANTID_IS_NULL_EN);
		}
		// 用户所有权限 已排序去重
		log.info("mp : 流水号 : " + runum + ",业务处理: 用户所有权限" + " 数据： " + "userId:" + userId + ",tenantId:" + tenantId);
		List<Permission> list = iPermissionService.findByUserIdAndTenantId(userId, tenantId);
		log.info("mp : 流水号 : " + runum + ",业务处理: 用户所有权限" + " 数据： " + list.size());

		List<Permission> menuList = new ArrayList<>();
		// 筛选一级页面
		for (Permission p : list) {
			if (CommonConstant.PERMISSION_PAGE.equals(p.getType()) && CommonConstant.LEVEL_ONE.equals(p.getLevel())) {
				menuList.add(p);
			}
		}

		log.info("mp : 流水号 : " + runum + ",业务处理: 筛选一级页面" + " 数据： " + JSONArray.fromObject(menuList));
		// 筛选二级页面
		List<Permission> secondMenuList = new ArrayList<>();
		for (Permission p : list) {
			if (CommonConstant.PERMISSION_PAGE.equals(p.getType()) && CommonConstant.LEVEL_TWO.equals(p.getLevel())) {
				secondMenuList.add(p);
			}
		}
		log.info("mp : 流水号 : " + runum + ",业务处理: 筛选二级页面" + " 数据： " + JSONArray.fromObject(secondMenuList));
		// 筛选二级页面拥有的按钮权限
		List<Permission> buttonPermissions = new ArrayList<>();
		for (Permission p : list) {
			if (CommonConstant.PERMISSION_OPERATION.equals(p.getType()) && CommonConstant.LEVEL_THREE.equals(p.getLevel())) {
				buttonPermissions.add(p);
			}
		}

		log.info("mp : 流水号 : " + runum + ",业务处理: 筛选二级页面拥有的按钮权限" + " 数据： " + JSONArray.fromObject(buttonPermissions));
		// 匹配二级页面拥有权限
		for (Permission p : secondMenuList) {
			List<String> permTypes = new ArrayList<>();
			for (Permission pe : buttonPermissions) {
				if (p.getId().equals(pe.getParentId())) {
					permTypes.add(pe.getButtonType());
				}
			}
			p.setPermTypes(permTypes);
		}
		log.info("mp : 流水号 : " + runum + ",业务处理: 匹配二级页面拥有权限" + " 数据： " + JSONArray.fromObject(secondMenuList));
		// 匹配一级页面拥有二级页面
		for (Permission p : menuList) {
			List<Permission> secondMenu = new ArrayList<>();
			for (Permission pe : secondMenuList) {
				if (p.getId().equals(pe.getParentId())) {
					secondMenu.add(pe);
				}
			}
			p.setChildren(secondMenu);
		}

		log.info("mp : 流水号 : " + runum + ",业务响应: 获取用户页面菜单数据" + " 数据： " + JSONArray.fromObject(menuList));
		return new ResultUtil<List<Permission>>().setData(menuList);
	}

	@RequestMapping(value = "/getAllList", method = RequestMethod.GET)
	@ApiOperation(value = "获取权限菜单树")
//	@Cacheable(key = "'allList'")
	public Result<List<Permission>> getAllList(String tenantId) {
		log.info("zbb-tenantId:" + tenantId);
		if (StringUtils.isEmpty(tenantId)) {
			return new ResultUtil<List<Permission>>().setErrorMsg(CommonUtils.TENANTID_IS_NULL_EN);
		}
		// 一级
//		List<Permission> list = permissionService.findByLevelOrderBySortOrder(CommonConstant.LEVEL_ONE);
		List<Permission> list = permissionService.findByLevelAndTenantIdOrderBySortOrder(CommonConstant.LEVEL_ONE, tenantId);

		// 二级
		for (Permission p1 : list) {
			List<Permission> children1 = permissionService.findByParentIdAndTenantIdOrderBySortOrder(p1.getId(), tenantId);
			p1.setChildren(children1);
			// 三级
			for (Permission p2 : children1) {
				List<Permission> children2 = permissionService.findByParentIdAndTenantIdOrderBySortOrder(p2.getId(), tenantId);
				p2.setChildren(children2);
			}
		}
		return new ResultUtil<List<Permission>>().setData(list);
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	@ApiOperation(value = "添加")
//	@CacheEvict(key = "'menuList'")
	public Result<Permission> add(@ModelAttribute Permission permission) {
		if (StringUtils.isEmpty(permission.getTenantId())) {
			return new ResultUtil<Permission>().setErrorMsg(CommonUtils.TENANTID_IS_NULL_EN);
		}
		// 判断拦截请求的操作权限按钮名是否已存在
		if (CommonConstant.PERMISSION_OPERATION.equals(permission.getType())) {
			List<Permission> list = permissionService.findByTitle(permission.getTitle());
			if (list != null && list.size() > 0) {
				return new ResultUtil<Permission>().setErrorMsg("名称已存在");
			}
		}
		Permission u = permissionService.save(permission);
		return new ResultUtil<Permission>().setData(u);
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST)
	@ApiOperation(value = "编辑")
	public Result<Permission> edit(@ModelAttribute Permission permission) {
		if (StringUtils.isEmpty(permission.getTenantId())) {
			return new ResultUtil<Permission>().setErrorMsg(CommonUtils.TENANTID_IS_NULL_EN);
		}
		// 判断拦截请求的操作权限按钮名是否已存在
		if (CommonConstant.PERMISSION_OPERATION.equals(permission.getType())) {
			// 若名称修改
			Permission p = permissionService.get(permission.getId());
			if (!p.getTitle().equals(permission.getTitle())) {
				List<Permission> list = permissionService.findByTitle(permission.getTitle());
				if (list != null && list.size() > 0) {
					return new ResultUtil<Permission>().setErrorMsg("名称已存在");
				}
			}
		}
		Permission u = permissionService.update(permission);
		return new ResultUtil<Permission>().setData(u);
	}

	@RequestMapping(value = "/delByIds", method = RequestMethod.DELETE)
	@ApiOperation(value = "批量通过id删除")
//	@CacheEvict(key = "'menuList'")
	public Result<Object> delByIds(String ids, String tenantId) {
		if (ids == null || "".equals(ids)) {
			return new ResultUtil<Object>().setErrorMsg("权限id不能为空");
		}
		if (StringUtils.isEmpty(tenantId)) {
			return new ResultUtil<Object>().setErrorMsg(CommonUtils.TENANTID_IS_NULL_EN);
		}
		String[] ids2 = ids.split(",");
		for (String id : ids2) {
			List<RolePermission> list = rolePermissionService.findByPermissionId(id);
			if (list != null && list.size() > 0) {
				return new ResultUtil<Object>().setErrorMsg("删除失败，包含正被角色使用关联的菜单或权限");
			}
		}
		for (String id : ids2) {
			permissionService.delete(id);
		}
		return new ResultUtil<Object>().setSuccessMsg("批量通过id删除数据成功");
	}
}
