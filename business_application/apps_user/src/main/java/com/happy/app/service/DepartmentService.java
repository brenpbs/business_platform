package com.happy.app.service;

import com.happy.app.appbase.base.AppBaseService;
import com.happy.app.entity.Department;

import java.util.List;

/**
 * 部门接口
 * @author Exrick
 */
public interface DepartmentService extends AppBaseService<Department, String> {

//	/**
//	 * 通过父id获取 升序
//	 * @param parentId
//	 * @return
//	 */
//	List<Department> findByParentIdAndTenantIdOrderBySortOrder(String parentId, String tenantId);

	/**
	 * 通过父id和状态获取
	 * @param parentId
	 * @param status
	 * @return
	 */
	List<Department> findByParentIdAndTenantIdAndStatusOrderBySortOrder(String parentId,
			String tenantId, Integer status);

	/**
	 * 通过父id和租户id获取
	 * @param parentId
	 * @param status
	 * @return
	 */

	List<Department> findByParentIdAndTenantIdOrderBySortOrder(String parentId, String tenantId);

	/**
	 * 通过id和租户id获取
	 *
	 * @param id
	 * @param tenantId
	 * @return
	 */
	Department findByIdAndTenantId(String id, String tenantId);
}
