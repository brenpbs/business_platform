package com.happy.app.service;

import com.happy.app.appbase.base.AppBaseService;
import com.happy.app.entity.Role;

import java.util.List;

/**
 * 角色接口
 *
 */
public interface RoleService extends AppBaseService<Role, String> {

	/**
	 * 获取默认角色
	 * @param defaultRole
	 * @return
	 */
	List<Role> findByDefaultRole(Boolean defaultRole);

	/**findByTenantId
	 * @param defaultRole
	 * @return
	 */
	List<Role> findByTenantId(String tenantId);
}
