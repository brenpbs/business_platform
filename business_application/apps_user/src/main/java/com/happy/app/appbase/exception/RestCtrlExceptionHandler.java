package com.happy.app.appbase.exception;

import com.happy.app.appbase.common.vo.Result;
import com.happy.app.appbase.common.utils.ResultUtil;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 *
 */
@Slf4j
@RestControllerAdvice
public class RestCtrlExceptionHandler {

	@ExceptionHandler(AppException.class)
	@ResponseStatus(value = HttpStatus.OK)
	public Result<Object> handleXCloudException(AppException e) {
//		e.printStackTrace();
		String errorMsg = "App exception";
		if (e != null) {
			errorMsg = e.getMsg();
			log.error(e.toString());
		}
		if (!StringUtils.isEmpty(e.getMessage())) {
			if (e.getMessage().contains("登录凭证过期，请重新登录") || e.getMessage().contains("token无效")) {
				return new ResultUtil<>().setErrorMsg(101, "登录凭证过期，请重新登录");
			}
		}
		return new ResultUtil<>().setErrorMsg(500, errorMsg);
	}

	@ExceptionHandler(Exception.class)
	@ResponseStatus(value = HttpStatus.OK)
	public Result<Object> handleException(Exception e) {
//		e.printStackTrace();
		String errorMsg = "Exception";
		if (e != null) {
			errorMsg = e.getMessage();
			log.error(e.toString());
		}
		if (!StringUtils.isEmpty(e.getMessage())) {
			if (e.getMessage().contains("登录凭证过期，请重新登录") || e.getMessage().contains("token无效")) {
				return new ResultUtil<>().setErrorMsg(101, "登录凭证过期，请重新登录");
			}
		}
		return new ResultUtil<>().setErrorMsg(500, errorMsg);
	}
}
