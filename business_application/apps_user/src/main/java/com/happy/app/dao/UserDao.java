package com.happy.app.dao;

import com.happy.app.appbase.base.AppBaseDao;
import com.happy.app.entity.User;

import java.util.List;

/**
 * 用户数据处理层
 *
 */
public interface UserDao extends AppBaseDao<User, String> {

	/**
	 * 通过用户名获取用户
	 *
	 * @param username
	 * @return
	 */
	List<User> findByUsername(String username);
	/**
	 * 通过用户名,accountType获取用户
	 *
	 * @param username
	 * @return
	 */
	List<User> findByUsernameAndAccountType(String username,String accountType);
	/**
	 * 通过部门id获取
	 *
	 * @param departmentId
	 * @return
	 */
	List<User> findByDepartmentId(String departmentId);

	/**
	 * 通过部门id和tenantId获取用户
	 *
	 * @param departmentId
	 * @param tenantId
	 * @return
	 */
	List<User> findByDepartmentIdAndTenantId(String departmentId, String tenantId);

	/**
	 * 通过账号获取用户
	 *
	 * @param mobile
	 * @param status
	 * @return
	 */
	List<User> findByAccountId(String accountId);

}
