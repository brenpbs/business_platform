package com.happy.app.common.aop;

import com.happy.app.appbase.common.utils.CommonUtils;
import com.happy.app.appbase.common.utils.ExceptionUtils;
import com.happy.app.appbase.common.vo.Result;
import com.happy.app.appbase.config.IgnoredUrlsProperties;
import com.happy.app.appbase.exception.AppException;
import com.happy.ipss.exception.HandlerException;
import com.happy.ipss.user.api.service.EmployeeService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * 前置通知处理，token有效性
 *
 * @author zhaozhen
 *
 */
@Slf4j
@Aspect
@Component
public class TokenAspect {
	@Autowired
	private IgnoredUrlsProperties ignoredUrlsProperties;
	// ###################中台接口对象##################
	@Autowired
	private EmployeeService employeeService;
	// @Autowired
	// private MessageApiController messageApiController;

	// ###################中台接口对象 end##################
	// private Logger log = Logger.getLogger(getClass());
	// java -jar xxx.jar --spring.profiles.active=dev
	// -DENV=pro

	@Pointcut("execution(* *..controller..*Controller*.*(..))")
	public void webLog() {
	}

	/**
	 * token不过滤的路径
	 *
	 * @param ignoreUrlList
	 * @param getRequestURL
	 * @return
	 */
	public boolean checkIgnoreUrlList(List<String> ignoreUrlList, String getRequestURL) {
		boolean ignoreFlag = false;
		for (String string : ignoreUrlList) {
			if (getRequestURL.contains(string)) {
				ignoreFlag = true;
			}
		}
		return ignoreFlag;
	}

	@Before("webLog()")
	public void doBefore(JoinPoint joinPoint) throws AppException {
//		log.debug("↓↓↓↓↓↓");
		// 接收到请求，记录请求内容
		ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder
				.getRequestAttributes();
		HttpServletRequest request = attributes.getRequest();
		String getRequestURL = request.getRequestURL().toString();
//		String busSeq = request.getParameter("runum");
//		RpcContext.getContext().setAttachment("busSeq", busSeq);
		// 记录下请求内容
//		log.debug("URL : " + getRequestURL);
//		log.debug("HTTP_METHOD : " + request.getMethod() + ",IP : " + request.getRemoteAddr()
//				+ ",CLASS_METHOD : " + joinPoint.getSignature().getDeclaringTypeName() + "."
//				+ joinPoint.getSignature().getName());
//		Object[] args = joinPoint.getArgs();
//		log.debug("ARGS : " + Arrays.toString(args));
		// token时间
		Long expiry = CommonUtils.TOKEN_EXPIRY_60;
		// token不过滤的路径
		List<String> ignoreUrlList = new ArrayList<String>();

		ignoreUrlList = ignoredUrlsProperties.getUrls();

		boolean checkIgnoreUrlList = checkIgnoreUrlList(ignoreUrlList, getRequestURL);
		// checkIgnoreUrlList =true 路径不校验
		if (checkIgnoreUrlList) {

		} else {
			String accessName = request.getHeader("accessName");
			String accessType = request.getHeader("accessType");
			String accessToken = request.getHeader("accessToken");
			// 解析token
			if (StringUtils.isEmpty(accessToken)) {
				throw new HandlerException("缺失登录凭证");
			}
			// 去中台验证token
			// 根据用户验证
			if (StringUtils.isEmpty(accessType)) {
				throw new HandlerException("参数异常");
			}
			String accountId = "";
			try {
				if (CommonUtils.EMPLOYEE_TYPE_E.equals(accessType)) {
					accountId = employeeService.authToken(accessToken, expiry);
				}  else {
					throw new HandlerException(CommonUtils.PARAM_IS_ERROR_CN);
				}
			} catch (Exception e) {
				log.info("HSF-Exception:" + e.getMessage());
				Result<Object> checkExceptionMessage = ExceptionUtils.checkExceptionMessage(e);
				throw new AppException(checkExceptionMessage.getMessage());
			}
		}
//		log.debug("......");
	}
//
//	@AfterReturning(returning = "ret", pointcut = "webLog()")
//	public void doAfterReturning(Object ret) throws Throwable {
//		// 处理完请求，返回内容
//		log.debug("RESPONSE : " + ret);
//
//		log.debug("↑↑↑↑↑↑");
//		log.debug("");
//	}

}
