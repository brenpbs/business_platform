package com.happy.app.entity;

import java.util.Date;
import java.util.List;

import lombok.Data;

@Data
public class UserB {
	private String tenantId;
	private String tenantCode;
	private String tenantName;
	private String tenantContactName;
	private String tenantContactPhone;
	// ------------------------
	private String id;
	private String username;
	private String password;
	private String departmentId;
	private String accountType;
	private String accountId;
	private Integer status;
	private Integer type;
	private String nickName;
	private String mobile;
	private String email;
	private Integer sex;
	private String description;
	private String departmentTitle;
	private List<Role> roles;
	private List<Permission> permissions;
	private Integer defaultRole;
	private String createBy;
	private Date createTime;
	private String updateBy;
	private Date updateTime;
	private Integer delFlag;

}
