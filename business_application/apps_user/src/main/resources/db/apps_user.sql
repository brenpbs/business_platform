/*
Navicat MySQL Data Transfer

Source Server         : 测试业务中台apps_app
Source Server Version : 50720
Source Host           : rm-2ze9l0n62f3p22hrwgo.mysql.rds.aliyuncs.com:3306
Source Database       : apps_user

Target Server Type    : MYSQL
Target Server Version : 50720
File Encoding         : 65001

Date: 2020-04-28 17:14:11
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `t_department`
-- ----------------------------
DROP TABLE IF EXISTS `t_department`;
CREATE TABLE `t_department` (
  `id` varchar(255) NOT NULL COMMENT '主键id',
  `tenant_id` varchar(255) DEFAULT NULL COMMENT '租户id',
  `create_by` varchar(255) DEFAULT NULL COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `del_flag` int(11) DEFAULT NULL COMMENT '删除标记',
  `update_by` varchar(255) DEFAULT NULL COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `parent_id` varchar(255) DEFAULT NULL COMMENT '父节点',
  `sort_order` decimal(10,2) DEFAULT NULL COMMENT '排序',
  `status` int(11) DEFAULT NULL COMMENT '状态',
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `is_parent` bit(1) DEFAULT NULL COMMENT '是否父节点',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='部门表';

-- ----------------------------
-- Records of t_department
-- ----------------------------
INSERT INTO `t_department` VALUES ('163820274456727552', '03abdab7709544e0a951dac75e7c7d91', null, '2019-07-17 15:35:38', '0', null, '2019-07-17 15:35:38', '163819468068229120', '1.00', '0', '中转业务服务', '');
INSERT INTO `t_department` VALUES ('168095543531147264', '03abdab7709544e0a951dac75e7c7d91', null, '2019-07-29 10:44:02', '0', null, '2020-02-13 17:46:29', '94695034883936256', '1.00', '0', '出租车', '');
INSERT INTO `t_department` VALUES ('240317043537088512', '03abdab7709544e0a951dac75e7c7d91', null, '2020-02-13 17:46:29', '0', null, '2020-02-13 17:46:36', '168095543531147264', '1.00', '0', '33', '');
INSERT INTO `t_department` VALUES ('240317074012901376', '03abdab7709544e0a951dac75e7c7d91', null, '2020-02-13 17:46:36', '0', null, '2020-02-13 17:46:44', '240317043537088512', '1.00', '0', '44', '');
INSERT INTO `t_department` VALUES ('240317107781242880', '03abdab7709544e0a951dac75e7c7d91', null, '2020-02-13 17:46:44', '0', null, '2020-02-13 17:46:44', '240317074012901376', '1.00', '0', '55', '');
INSERT INTO `t_department` VALUES ('40322777781112832', '03abdab7709544e0a951dac75e7c7d91', '', '2018-08-10 20:40:40', '0', '', '2018-08-11 00:03:06', '0', '1.00', '0', '总部', '');

-- ----------------------------
-- Table structure for `t_permission`
-- ----------------------------
DROP TABLE IF EXISTS `t_permission`;
CREATE TABLE `t_permission` (
  `id` varchar(255) NOT NULL COMMENT '主键id',
  `tenant_id` varchar(255) DEFAULT NULL COMMENT '租户id',
  `create_by` varchar(255) DEFAULT NULL COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `del_flag` int(11) DEFAULT NULL COMMENT '删除标记',
  `update_by` varchar(255) DEFAULT NULL COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `description` varchar(255) DEFAULT NULL COMMENT '描述',
  `name` varchar(255) DEFAULT NULL COMMENT '权限名称',
  `parent_id` varchar(255) DEFAULT NULL COMMENT '父节点',
  `type` int(11) DEFAULT NULL COMMENT '权限类型',
  `sort_order` decimal(10,2) DEFAULT NULL COMMENT '排序',
  `component` varchar(255) DEFAULT NULL COMMENT 'component',
  `path` varchar(255) DEFAULT NULL COMMENT '路井',
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `icon` varchar(255) DEFAULT NULL COMMENT '图标',
  `level` int(11) DEFAULT NULL COMMENT '级别',
  `button_type` varchar(255) DEFAULT NULL COMMENT '按钮类型',
  `status` int(11) DEFAULT NULL COMMENT '状态',
  `url` varchar(255) DEFAULT NULL COMMENT 'URL',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='权限表';

-- ----------------------------
-- Records of t_permission
-- ----------------------------
INSERT INTO `t_permission` VALUES ('16687383932047360', '03abdab7709544e0a951dac75e7c7d91', '', '2018-06-06 15:22:03', '0', '', '2018-09-19 22:07:34', '', '', '5129710648430594', '1', '1.21', '', '/xboot/role/save*', '添加角色', '', '3', 'add', '0', '');
INSERT INTO `t_permission` VALUES ('16689632049631232', '03abdab7709544e0a951dac75e7c7d91', '', '2018-06-06 15:30:59', '0', '', '2018-09-19 22:07:37', '', '', '5129710648430594', '1', '1.22', '', '/xboot/role/edit*', '编辑角色', '', '3', 'edit', '0', '');
INSERT INTO `t_permission` VALUES ('16689745006432256', '03abdab7709544e0a951dac75e7c7d91', '', '2018-06-06 15:31:26', '0', '', '2018-08-10 21:41:23', '', '', '5129710648430594', '1', '1.23', '', '/xboot/role/delAllByIds/**', '删除角色', '', '3', 'delete', '0', '');
INSERT INTO `t_permission` VALUES ('16689883993083904', '03abdab7709544e0a951dac75e7c7d91', null, '2018-06-06 15:31:59', '0', null, '2018-06-06 15:31:59', null, null, '5129710648430594', '1', '1.24', null, '/xboot/role/editRolePerm/**', '分配权限', null, '3', 'editPerm', '0', null);
INSERT INTO `t_permission` VALUES ('16690313745666048', '03abdab7709544e0a951dac75e7c7d91', '', '2018-06-06 15:33:41', '0', '', '2018-09-19 22:07:46', '', '', '5129710648430594', '1', '1.25', '', '/xboot/role/setDefault*', '设为默认角色', '', '3', 'setDefault', '0', '');
INSERT INTO `t_permission` VALUES ('16694861252005888', '03abdab7709544e0a951dac75e7c7d91', '', '2018-06-06 15:51:46', '0', '', '2018-09-19 22:07:52', '', '', '5129710648430595', '1', '1.31', '', '/xboot/permission/add*', '添加菜单', '', '3', 'add', '0', '');
INSERT INTO `t_permission` VALUES ('16695107491205120', '03abdab7709544e0a951dac75e7c7d91', '', '2018-06-06 15:52:44', '0', '', '2018-09-19 22:07:57', '', '', '5129710648430595', '1', '1.32', '', '/xboot/permission/edit*', '编辑菜单', '', '3', 'edit', '0', '');
INSERT INTO `t_permission` VALUES ('16695243126607872', '03abdab7709544e0a951dac75e7c7d91', '', '2018-06-06 15:53:17', '0', '', '2018-08-10 21:41:33', '', '', '5129710648430595', '1', '1.33', '', '/xboot/permission/delByIds/**', '删除菜单', '', '3', 'delete', '0', '');
INSERT INTO `t_permission` VALUES ('40238597734928384', '03abdab7709544e0a951dac75e7c7d91', null, '2018-08-10 15:06:10', '0', null, '2018-08-10 15:06:10', null, 'department-manage', '5129710648430592', '0', '1.20', 'sys/department-manage/departmentManage', 'department-manage', '部门管理', 'md-git-branch', '2', '', '0', null);
INSERT INTO `t_permission` VALUES ('45235621697949696', '03abdab7709544e0a951dac75e7c7d91', '', '2018-08-24 10:02:33', '0', '', '2019-06-13 11:26:36', '', '', '40238597734928384', '1', '1.21', '', '/app/department/add*', '添加部门', '', '3', 'add', '0', '');
INSERT INTO `t_permission` VALUES ('45235787867885568', '03abdab7709544e0a951dac75e7c7d91', '', '2018-08-24 10:03:13', '0', '', '2019-06-13 11:26:29', '', '', '40238597734928384', '1', '1.22', '', '/app/department/edit*', '编辑部门', '', '3', 'edit', '0', '');
INSERT INTO `t_permission` VALUES ('45235939278065664', '03abdab7709544e0a951dac75e7c7d91', '', '2018-08-24 10:03:49', '0', '', '2019-06-13 11:26:21', '', '', '40238597734928384', '1', '1.23', '', '/app/department/delByIds/*', '删除部门', '', '3', 'delete', '0', '');
INSERT INTO `t_permission` VALUES ('5129710648430592', '03abdab7709544e0a951dac75e7c7d91', '', '2018-06-04 19:02:29', '0', '', '2018-11-20 17:01:47', '', 'sys', '', '0', '1.00', 'Main', '/sys', '系统管理', 'ios-settings', '1', '', '0', '');
INSERT INTO `t_permission` VALUES ('5129710648430594', '03abdab7709544e0a951dac75e7c7d91', '', '2018-06-04 19:02:35', '0', '', '2018-09-23 23:32:16', '', 'role-manage', '5129710648430592', '0', '1.60', 'sys/role-manage/roleManage', 'role-manage', '角色管理', 'md-contacts', '2', '', '0', '');
INSERT INTO `t_permission` VALUES ('5129710648430595', '03abdab7709544e0a951dac75e7c7d91', '', '2018-06-04 19:02:37', '0', '', '2018-09-23 23:32:02', '', 'menu-manage', '5129710648430592', '0', '1.70', 'sys/menu-manage/menuManage', 'menu-manage', '菜单权限管理', 'md-menu', '2', '', '0', '');
INSERT INTO `t_permission` VALUES ('77228582530715648', '03abdab7709544e0a951dac75e7c7d91', null, '2018-11-20 16:51:11', '0', null, '2018-11-20 16:51:11', null, 'user', null, '0', '2.00', 'Main', '/user', '用户管理', 'md-people', '1', null, '0', null);
INSERT INTO `t_permission` VALUES ('77229132248780800', '03abdab7709544e0a951dac75e7c7d91', '', '2018-11-20 16:53:21', '0', '', '2018-11-20 16:54:30', '', 'user-manage', '77228582530715648', '0', '1.10', 'user/user-manage/userManage', 'user-manage', '业务人员管理', 'md-person', '2', '', '0', '');

-- ----------------------------
-- Table structure for `t_role`
-- ----------------------------
DROP TABLE IF EXISTS `t_role`;
CREATE TABLE `t_role` (
  `id` varchar(255) NOT NULL COMMENT '主键id',
  `tenant_id` varchar(255) DEFAULT NULL COMMENT '租户id',
  `create_by` varchar(255) DEFAULT NULL COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(255) DEFAULT NULL COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `name` varchar(255) DEFAULT NULL COMMENT '角色名称',
  `del_flag` int(11) DEFAULT NULL COMMENT '删除标记',
  `default_role` bit(1) DEFAULT NULL COMMENT '默认角色',
  `description` varchar(255) DEFAULT NULL COMMENT '描述',
  `air_line` varchar(255) DEFAULT NULL COMMENT '航空公司',
  `airport_code` varchar(255) DEFAULT NULL COMMENT '机场编号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色表';

-- ----------------------------
-- Records of t_role
-- ----------------------------
INSERT INTO `t_role` VALUES ('496138616573952', '03abdab7709544e0a951dac75e7c7d91', '', '2018-04-22 23:03:49', '', '2018-08-12 16:14:31', 'ROLE_ADMIN', '0', '', '超级管理员 拥有所有权限', '西安航空公司', 'XIY');

-- ----------------------------
-- Table structure for `t_role_permission`
-- ----------------------------
DROP TABLE IF EXISTS `t_role_permission`;
CREATE TABLE `t_role_permission` (
  `id` varchar(255) NOT NULL COMMENT '主键id',
  `tenant_id` varchar(255) DEFAULT NULL COMMENT '租户id',
  `permission_id` varchar(255) DEFAULT NULL COMMENT '权限id',
  `role_id` varchar(255) DEFAULT NULL COMMENT '角色id',
  `create_by` varchar(255) DEFAULT NULL COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `del_flag` int(11) DEFAULT NULL COMMENT '删除标记',
  `update_by` varchar(255) DEFAULT NULL COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色权限表';

-- ----------------------------
-- Records of t_role_permission
-- ----------------------------
INSERT INTO `t_role_permission` VALUES ('267487292971552768', '03abdab7709544e0a951dac75e7c7d91', '5129710648430592', '496138616573952', null, '2020-04-28 17:11:22', '0', null, '2020-04-28 17:11:22');
INSERT INTO `t_role_permission` VALUES ('267487293999157248', '03abdab7709544e0a951dac75e7c7d91', '40238597734928384', '496138616573952', null, '2020-04-28 17:11:22', '0', null, '2020-04-28 17:11:22');
INSERT INTO `t_role_permission` VALUES ('267487294024323072', '03abdab7709544e0a951dac75e7c7d91', '45235621697949696', '496138616573952', null, '2020-04-28 17:11:22', '0', null, '2020-04-28 17:11:22');
INSERT INTO `t_role_permission` VALUES ('267487294045294593', '03abdab7709544e0a951dac75e7c7d91', '45235787867885568', '496138616573952', null, '2020-04-28 17:11:22', '0', null, '2020-04-28 17:11:22');
INSERT INTO `t_role_permission` VALUES ('267487294066266112', '03abdab7709544e0a951dac75e7c7d91', '45235939278065664', '496138616573952', null, '2020-04-28 17:11:22', '0', null, '2020-04-28 17:11:22');
INSERT INTO `t_role_permission` VALUES ('267487294087237632', '03abdab7709544e0a951dac75e7c7d91', '5129710648430594', '496138616573952', null, '2020-04-28 17:11:22', '0', null, '2020-04-28 17:11:22');
INSERT INTO `t_role_permission` VALUES ('267487294108209152', '03abdab7709544e0a951dac75e7c7d91', '16687383932047360', '496138616573952', null, '2020-04-28 17:11:22', '0', null, '2020-04-28 17:11:22');
INSERT INTO `t_role_permission` VALUES ('267487294124986369', '03abdab7709544e0a951dac75e7c7d91', '16689632049631232', '496138616573952', null, '2020-04-28 17:11:22', '0', null, '2020-04-28 17:11:22');
INSERT INTO `t_role_permission` VALUES ('267487294145957888', '03abdab7709544e0a951dac75e7c7d91', '16689745006432256', '496138616573952', null, '2020-04-28 17:11:22', '0', null, '2020-04-28 17:11:22');
INSERT INTO `t_role_permission` VALUES ('267487294422781952', '03abdab7709544e0a951dac75e7c7d91', '16689883993083904', '496138616573952', null, '2020-04-28 17:11:22', '0', null, '2020-04-28 17:11:22');
INSERT INTO `t_role_permission` VALUES ('267487294439559169', '03abdab7709544e0a951dac75e7c7d91', '16690313745666048', '496138616573952', null, '2020-04-28 17:11:22', '0', null, '2020-04-28 17:11:22');
INSERT INTO `t_role_permission` VALUES ('267487294460530688', '03abdab7709544e0a951dac75e7c7d91', '5129710648430595', '496138616573952', null, '2020-04-28 17:11:22', '0', null, '2020-04-28 17:11:22');
INSERT INTO `t_role_permission` VALUES ('267487294489890816', '03abdab7709544e0a951dac75e7c7d91', '16694861252005888', '496138616573952', null, '2020-04-28 17:11:22', '0', null, '2020-04-28 17:11:22');
INSERT INTO `t_role_permission` VALUES ('267487294858989568', '03abdab7709544e0a951dac75e7c7d91', '16695107491205120', '496138616573952', null, '2020-04-28 17:11:22', '0', null, '2020-04-28 17:11:22');
INSERT INTO `t_role_permission` VALUES ('267487294879961088', '03abdab7709544e0a951dac75e7c7d91', '16695243126607872', '496138616573952', null, '2020-04-28 17:11:22', '0', null, '2020-04-28 17:11:22');
INSERT INTO `t_role_permission` VALUES ('267487294900932608', '03abdab7709544e0a951dac75e7c7d91', '77228582530715648', '496138616573952', null, '2020-04-28 17:11:22', '0', null, '2020-04-28 17:11:22');
INSERT INTO `t_role_permission` VALUES ('267487294917709825', '03abdab7709544e0a951dac75e7c7d91', '77229132248780800', '496138616573952', null, '2020-04-28 17:11:22', '0', null, '2020-04-28 17:11:22');
INSERT INTO `t_role_permission` VALUES ('267487294938681344', '03abdab7709544e0a951dac75e7c7d91', '77229343343906816', '496138616573952', null, '2020-04-28 17:11:22', '0', null, '2020-04-28 17:11:22');
INSERT INTO `t_role_permission` VALUES ('267487294955458561', '03abdab7709544e0a951dac75e7c7d91', '77229962439954432', '496138616573952', null, '2020-04-28 17:11:22', '0', null, '2020-04-28 17:11:22');

-- ----------------------------
-- Table structure for `t_user`
-- ----------------------------
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user` (
  `id` varchar(255) NOT NULL COMMENT '主键ID',
  `tenant_id` varchar(255) DEFAULT NULL COMMENT '租户id',
  `username` varchar(255) DEFAULT NULL COMMENT '账户名',
  `department_id` varchar(255) DEFAULT NULL COMMENT '部门id',
  `account_type` varchar(255) DEFAULT NULL COMMENT '账户类型，E:工作人员S:服务人员P:旅客',
  `account_id` varchar(255) DEFAULT NULL COMMENT '账户id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='账户表';

-- ----------------------------
-- Records of t_user
-- ----------------------------
INSERT INTO `t_user` VALUES ('186653199686438912', '03abdab7709544e0a951dac75e7c7d91', 'dujianqin', '40322777781112832', 'E', '3df0e4dc6d3748178d8f47d0f9c4ea06');

-- ----------------------------
-- Table structure for `t_user_role`
-- ----------------------------
DROP TABLE IF EXISTS `t_user_role`;
CREATE TABLE `t_user_role` (
  `id` varchar(255) NOT NULL COMMENT '主键ID',
  `tenant_id` varchar(255) DEFAULT NULL COMMENT '租户id',
  `role_id` varchar(255) DEFAULT NULL COMMENT '角色ID',
  `user_id` varchar(255) DEFAULT NULL COMMENT '用户ID',
  `create_by` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `del_flag` int(11) DEFAULT NULL,
  `update_by` varchar(255) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='账户角色关系表';

-- ----------------------------
-- Records of t_user_role
-- ----------------------------
INSERT INTO `t_user_role` VALUES ('225437403303645184', '03abdab7709544e0a951dac75e7c7d91', '496138616573952', '186653199686438912', null, '2020-01-03 16:20:06', '0', null, '2020-01-03 16:20:06');
