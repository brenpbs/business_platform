package com.happy.app.luggagetrack.controller;
import cn.hutool.core.date.DateUtil;
import com.happy.app.luggagetrack.base.Result;
import com.happy.app.luggagetrack.base.ResultUtil;
import com.happy.ipss.dto.PageDTO;
import com.happy.ipss.exception.HandlerException;
import com.happy.ipss.luggage.api.dto.LuggageFindDTO;
import com.happy.ipss.luggage.api.dto.LuggageFindResultDTO;
import com.happy.ipss.luggage.api.dto.LuggageOperationFindDTO;
import com.happy.ipss.luggage.api.dto.LuggageOperationFindResultDTO;
import com.happy.ipss.luggage.api.service.LuggageService;
import com.happy.ipss.passenger.api.dto.PassengerParamDTO;
import com.happy.ipss.passenger.api.dto.PassengerResultDTO;
import com.happy.ipss.passenger.api.service.PassengerService;
import com.happy.ipss.trip.api.dto.TripInfoParamDTO;
import com.happy.ipss.trip.api.dto.TripInfoResultDTO;
import com.happy.ipss.trip.api.service.TripInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
@CrossOrigin
@RestController
@Slf4j
@RequestMapping(value = "/luggagetrack")
@Api(description = "行李查询")
public class LuggageTrackController {
    @Autowired
    private PassengerService passengerService;
    @Autowired
    private TripInfoService tripInfoService;
    @Autowired
    private LuggageService luggageService;
    @RequestMapping(value = "/findLuggageByCertificateNumber/{certificateNumber}/{flightDate}", method = RequestMethod.GET)
    @ApiOperation(value = "小程序端根据旅客证件号查询行程、会员和行李状态")
    public Result<Map> findLuggageByCertificateNumber(@PathVariable String certificateNumber,@PathVariable String flightDate) {
        Map<String,Object> map = new HashMap<>();
        try {
            //按证件号查询出旅客的个人信息，调用会员中心201接口
            PassengerResultDTO passengerResultDTO;
            PassengerParamDTO passengerParamDTO = new PassengerParamDTO();
            passengerParamDTO.setCertificateNumber(certificateNumber);
            PageDTO<PassengerResultDTO> pagePassengerResultDTO = passengerService.findByParam(passengerParamDTO);
            if(pagePassengerResultDTO!=null&&pagePassengerResultDTO.getList().size()>0){
                passengerResultDTO = pagePassengerResultDTO.getList().get(0);
                map.put("passengerResultDTO",passengerResultDTO);
            }else {
                return new ResultUtil<Map>().setErrorMsg(311,"旅客信息不存在");
            }
            //根据上一步返回的旅客信息唯一OneId查询旅客的行程，调用行程中心的101接口
            TripInfoParamDTO tripInfoParamDTO = new TripInfoParamDTO();
            tripInfoParamDTO.setOneId(passengerResultDTO.getOneId());
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            tripInfoParamDTO.setFlightDate(DateUtil.parse(flightDate, "yyyy-MM-dd"));
            TripInfoResultDTO tripInfoResultDTO = tripInfoService.findTripInfo(tripInfoParamDTO);
            map.put("tripInfoResultDTO",tripInfoResultDTO);
            //根据上一步返回的旅客行程编号查询旅客本次行程的行李信息，调用行李中心的301接口
            LuggageFindDTO luggageFindDTO = new LuggageFindDTO();
            luggageFindDTO.setTrip_number(tripInfoResultDTO.getTripNumber());
            PageDTO<LuggageFindResultDTO> pageLuggageFindReslutDTO = luggageService.findLuggageByParam(luggageFindDTO);
            map.put("pageLuggageFindReslutDTO",pageLuggageFindReslutDTO);
            return new ResultUtil<Map>().setData(map);
        } catch (HandlerException he) {
            he.printStackTrace();
            return new ResultUtil<Map>().setErrorMsg(he.getMessage());
        } catch (Exception e){
            e.printStackTrace();
            return new ResultUtil<Map>().setErrorMsg(e.getMessage());
        }
    }

    @PostMapping("findPassengerList")
    @ApiOperation(value = "管理端根据条件查询旅客信息列表")
    public Result<PageDTO> findPassengerList(@RequestBody PassengerParamDTO passengerParamDTO) {
        try {
            //查询出旅客的个人信息列表，调用会员中心201接口
            PageDTO<PassengerResultDTO> pagePassengerResultDTO = passengerService.findByParam(passengerParamDTO);
            return new ResultUtil<PageDTO>().setData(pagePassengerResultDTO);
        } catch (HandlerException he) {
            he.printStackTrace();
            return new ResultUtil<PageDTO>().setErrorMsg(he.getMessage());
        } catch (Exception e){
            e.printStackTrace();
            return new ResultUtil<PageDTO>().setErrorMsg(e.getMessage());
        }
    }

    @PostMapping("findTripByOneID")
    @ApiOperation(value = "管理端根据旅客OneID查询行程信息")
    public Result<PageDTO> findTripByOneID(@RequestBody TripInfoParamDTO tripInfoParamDTO) {
        try {
            PageDTO<TripInfoResultDTO> pageTripResultDTO = tripInfoService.findTripInfos(tripInfoParamDTO);
            return new ResultUtil<PageDTO>().setData(pageTripResultDTO);
        } catch (HandlerException he) {
            he.printStackTrace();
            return new ResultUtil<PageDTO>().setErrorMsg(he.getMessage());
        } catch (Exception e){
            e.printStackTrace();
            return new ResultUtil<PageDTO>().setErrorMsg(e.getMessage());
        }
    }

    @PostMapping("findLuggageByTripID")
    @ApiOperation(value = "管理端根据行程ID查询行李信息")
    public Result<PageDTO> findLuggageByTripID(@RequestBody LuggageFindDTO luggageFindDTO) {
        try {
            PageDTO<LuggageFindResultDTO> pageLuggageResultDTO = luggageService.findLuggageByParam(luggageFindDTO);
            return new ResultUtil<PageDTO>().setData(pageLuggageResultDTO);
        } catch (HandlerException he) {
            he.printStackTrace();
            return new ResultUtil<PageDTO>().setErrorMsg(he.getMessage());
        } catch (Exception e){
            e.printStackTrace();
            return new ResultUtil<PageDTO>().setErrorMsg(e.getMessage());
        }
    }

    @PostMapping("findLuggageOperationByLuggageID")
    @ApiOperation(value = "管理端根据行李ID查询行李操作信息")
    public Result<PageDTO> findLuggageOperationByLuggageID(@RequestBody LuggageOperationFindDTO luggageOperationFindDTO) {
        try {
            PageDTO<LuggageOperationFindResultDTO> pageLuggageOperationResultDTO = luggageService.findLuggageOperationByParam(luggageOperationFindDTO);
            return new ResultUtil<PageDTO>().setData(pageLuggageOperationResultDTO);
        } catch (HandlerException he) {
            he.printStackTrace();
            return new ResultUtil<PageDTO>().setErrorMsg(he.getMessage());
        } catch (Exception e){
            e.printStackTrace();
            return new ResultUtil<PageDTO>().setErrorMsg(e.getMessage());
        }
    }


}