package com.happy.app.luggagetrack.config;
import com.alibaba.boot.hsf.annotation.HSFConsumer;
import com.happy.ipss.luggage.api.service.LuggageService;
import com.happy.ipss.passenger.api.service.PassengerService;
import com.happy.ipss.trip.api.service.TripInfoService;
import org.springframework.context.annotation.Configuration;
/**
 * HSF服务提供者
 **/
@Configuration
public class HsfConfig {
    @HSFConsumer
    PassengerService passengerService;
    @HSFConsumer
    TripInfoService tripInfoService;
    @HSFConsumer
    LuggageService luggageService;
}
