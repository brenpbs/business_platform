/*
Navicat MySQL Data Transfer

Source Server         : 测试业务中台ipss_mground
Source Server Version : 50720
Source Host           : rm-2ze9l0n62f3p22hrwgo.mysql.rds.aliyuncs.com:3306
Source Database       : ipss_user

Target Server Type    : MYSQL
Target Server Version : 50720
File Encoding         : 65001

Date: 2020-04-28 17:14:23
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `account`
-- ----------------------------
DROP TABLE IF EXISTS `account`;
CREATE TABLE `account` (
  `account_id` varchar(32) NOT NULL COMMENT '账户Id',
  `account_name` varchar(32) NOT NULL COMMENT '账户名(必须以字母开头)',
  `phone` varchar(14) NOT NULL COMMENT '账户手机',
  `password` varchar(32) NOT NULL COMMENT '密码',
  `account_type` char(1) NOT NULL COMMENT '账户类型 E：工作人员  S：服务人员 P：旅客',
  `state` char(1) NOT NULL COMMENT '账户状态 Y：正常   N：锁定   D：已删除',
  `last_login_time` datetime DEFAULT NULL COMMENT '最后一次登录时间',
  `last_login_ip` varchar(100) DEFAULT NULL COMMENT '最后一次登录Ip',
  `nationality` varchar(50) DEFAULT NULL COMMENT '国籍',
  `province` varchar(50) DEFAULT NULL COMMENT '省',
  `city` varchar(50) DEFAULT NULL COMMENT '市',
  `district` varchar(50) DEFAULT NULL COMMENT '区/县',
  `subdistrict` varchar(100) DEFAULT NULL COMMENT '街道',
  `reserve1` varchar(200) DEFAULT NULL COMMENT '备用1',
  `reserve2` varchar(200) DEFAULT NULL COMMENT '备用2',
  `reserve3` varchar(200) DEFAULT NULL COMMENT '备用3',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `maintainer` varchar(32) NOT NULL COMMENT '维护人',
  PRIMARY KEY (`account_id`),
  UNIQUE KEY `sy_account_id` (`account_id`) USING BTREE,
  KEY `sy_phone` (`phone`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='账户表';

-- ----------------------------
-- Records of account
-- ----------------------------
INSERT INTO `account` VALUES ('3df0e4dc6d3748178d8f47d0f9c4ea06', 'dujianqin', '13700240901', 'ae4bdda02ff57f0d0d4f53dd3d6b12ce', 'E', 'Y', '2020-04-20 15:10:51', null, null, null, null, null, null, null, null, null, '', '2019-09-18 15:45:32', '2020-04-20 15:10:51', 'cxlxsystem');

-- ----------------------------
-- Table structure for `employee`
-- ----------------------------
DROP TABLE IF EXISTS `employee`;
CREATE TABLE `employee` (
  `account_id` varchar(32) NOT NULL COMMENT '帐号ID',
  `tenant_id` varchar(32) NOT NULL COMMENT '租户Id',
  `code` varchar(20) DEFAULT NULL COMMENT '人员编码',
  `employee_name` varchar(32) DEFAULT NULL COMMENT '姓名',
  `avatar` varchar(500) DEFAULT NULL COMMENT '头像',
  `idcard_no` varchar(18) DEFAULT NULL COMMENT '身份证号',
  `gender` char(1) DEFAULT NULL COMMENT '性别  M：男   F：女  U：未知',
  `email` varchar(50) DEFAULT NULL COMMENT 'Email',
  `office_phone` varchar(20) DEFAULT NULL COMMENT '办公电话',
  `reserve1` varchar(200) DEFAULT NULL COMMENT '备用1',
  `reserve2` varchar(200) DEFAULT NULL COMMENT '备用2',
  `reserve3` varchar(200) DEFAULT NULL COMMENT '备用3',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `maintainer` varchar(32) NOT NULL COMMENT '维护人',
  PRIMARY KEY (`account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='工作人员信息表';

-- ----------------------------
-- Records of employee
-- ----------------------------
INSERT INTO `employee` VALUES ('3df0e4dc6d3748178d8f47d0f9c4ea06', '03abdab7709544e0a951dac75e7c7d91', '', '杜建琴', '', '', 'U', '13700240901@163.com', '', '', '', '', '', '2019-09-18 15:45:32', '2020-01-03 16:20:06', 'XIYadmin');
