package com.happy.ipss.user.provider.config;

import com.taobao.txc.client.aop.TxcTransactionScaner;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * GTS配置
 */
@Configuration
public class GTSConfig {

    /**
     * 定义声明式事务，要想让事务annotation感知的话，要在这里定义一下
     * @return TxcTransactionScaner
     */
    @Bean(name = "txcScanner")
    @ConfigurationProperties(prefix="aliyun")
    public TxcTransactionScaner txcTransactionScaner(@Value("${txcScanner.enable}") boolean enable,@Value("${txcScanner.groupName}")String groupName) {
        return enable?new TxcTransactionScaner(groupName):new TxcTransactionScaner("myapp","txc_test_public.1129361738553704.QD",1,"https://test-cs-gts.aliyuncs.com");
    }
}
