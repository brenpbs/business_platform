package com.happy.ipss.user.provider.po;


import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;


/**
 * 账户信息（工作人员、服务人员、旅客）
 */
@Entity
@Table(name = "account")
@EntityListeners(AuditingEntityListener.class)
@DynamicUpdate
@Setter
@Getter
public class Account {
    @Id
    /**
     *账户ID
     */
    @Column(name = "account_Id")
    private String accountId;
    /**
     * 账户名称
     */
    @Column(name = "account_Name")
    private String accountName;
    /**
     * 手机号
     */
    @Column(name = "phone")
    private String phone;
    /**
     * 账户密码
     */
    @Column(name = "password")
    private String password;
    /**
     * 账户类型 E:工作人员  S:服务人员  P:旅客
     */
    @Column(name = "account_Type")
    private String accountType;
    /**
     * 状态 Y：正常  N:锁定  D:已删除
     */
    @Column(name = "state")
    private String state;
    /**
     * 最后登录时间
     */
    @Column(name = "last_Login_Time")
    private Date lastLoginTime;
    /**
     * 最后登录IP
     */
    @Column(name = "last_Login_Ip")
    private String lastLoginIp;

    /**
     * 国籍
     */
    @Column(name = "nationality")
    private String nationality;
    /**
     * 省
     */
    @Column(name = "province")
    private String province;
    /**
     * 市
     */
    @Column(name = "city")
    private String city;
    /**
     * 区/县
     */
    @Column(name = "district")
    private String district;
    /**
     * 街道
     */
    @Column(name = "subdistrict")
    private String subdistrict;
    /**
     * 备用字段1
     */
    @Column(name = "reserve1")
    private String reserve1;
    /**
     * 备用字段2
     */
    @Column(name = "reserve2")
    private String reserve2;
    /**
     * 备用字段3
     */
    @Column(name = "reserve3")
    private String reserve3;
    /**
     * 备注
     */
    @Column(name = "remark")
    private String remark;
    /**
     * 创建时间
     */
    @CreatedDate
    @Column(name = "create_Time")
    private Date createTime;
    /**
     * 修改时间
     */
    @LastModifiedDate
    @Column(name = "update_Time")
    private Date updateTime;
    /**
     * 维护人
     */
    @Column(name = "maintainer")
    private String maintainer;

}
