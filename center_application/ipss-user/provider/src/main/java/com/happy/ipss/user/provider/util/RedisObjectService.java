//package com.happy.ipss.user.provider.util;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.data.redis.core.RedisTemplate;
//import org.springframework.data.redis.core.ValueOperations;
//import org.springframework.stereotype.Component;
//
//import javax.annotation.Resource;
//import java.util.concurrent.TimeUnit;
//
///**
// * @author yf 2017年12月5日 上午11:01:00
// * redis Object Service 模板类
// * 依赖ValueOperations接口，并实现部分功能，调用者需要指定具体的T类型。
// * 主要用于存储简单的对象。如需增加其他操作，请自行补全此工具类（多key值操作,getAndSet,bit操作,操作字符串指定坐标内容）
// * 提供单key值操作
// * string append
// * int提供redis原子操作
// * @version 1.0
// */
//@Component
//public class RedisObjectService<T> {
//
//    @Autowired
//    protected RedisTemplate<String, Object> redisTemplate;
//
//
//    @Resource
//    protected ValueOperations<String, T> valueOperations;
//
//    /**
//     * 验证码前缀
//     */
//    private static final String AUTH_CODE_PREFIX ="AUTH_CODE_";
//    /**
//     * token前缀
//     */
//    private static final String TOKEN_PREFIX ="TOKEN_";
//
//    /**
//     * 密码输入错误锁定次数
//     */
//    private static final String LOCK_PREFIX ="LOCK_";
//
//    /**
//     * @param key  key
//     * @param obj 值
//     * @param timeout 过期时间
//     * @param unit    时间类型TimeUnit 枚举类型
//     */
//    private void set(String key, T obj, long timeout, TimeUnit unit) {
//        RedisExceptionUtil.checkKeyException(key);
//        RedisExceptionUtil.checkObjectIsNull(unit);
//        //如果时间传入时间小于0。抛出不支持操作异常。
//        if (timeout < 0) {
//            throw new UnsupportedOperationException();
//        }
//        valueOperations.set(key, obj, timeout, unit);
//    }
//
//
//    /**
//     * 根据key值获取指定对象
//     * @param key key
//     * @return  对象
//     */
//    private T get(String key) {
//        RedisExceptionUtil.checkKeyException(key);
//        return valueOperations.get(key);
//    }
//
//    /**
//     * 删除指定key
//     * @param key 指定key
//     */
//    private void remove(String key) {
//        RedisExceptionUtil.checkKeyException(key);
//        if(exist(key)){
//            valueOperations.getOperations().delete(key);
//        }
//    }
//
//    /**
//     * 判断key是否存在
//     * @param key key
//     * @return  是否存在
//     */
//    private boolean exist(String key) {
//        RedisExceptionUtil.checkKeyException(key);
//        return get(key) != null;
//    }
//
//
//    /**
//     * 设置验证码
//     * @param phone 手机号
//     * @param code 验证码
//     * @param timeout 超时时间 分钟
//     */
//    public void setAuthCode(String phone,T code,long timeout){
//        set(AUTH_CODE_PREFIX+phone,code,timeout,TimeUnit.MINUTES);
//    }
//
//    /**
//     * 获取验证码
//     * @param phone 手机号
//     */
//    public T getAuthCode(String phone){
//      return  get(AUTH_CODE_PREFIX+phone);
//    }
//    /**
//     * 删除验证码
//     * @param phone 手机号
//     */
//    public void removeAuthCode(String phone){
//        remove(AUTH_CODE_PREFIX+phone);
//    }
//
//
//    /**
//     * 删除token
//     * @param accountId 账户Id
//     */
//    public void removeToken(String accountId){
//        remove(TOKEN_PREFIX+accountId);
//    }
//    /**
//     * 设置token
//     * @param accountId 账户Id
//     * @param type 用户类型
//     * @param timeout 超时时间 分钟
//     */
//    public void setToken(String accountId,T type,long timeout){
//        set(TOKEN_PREFIX+accountId,type,timeout,TimeUnit.MINUTES);
//    }
//    /**
//     * 获取token
//     * @param accountId 账户Id
//     */
//    public T getToken(String accountId){
//        return  get(TOKEN_PREFIX+accountId);
//    }
//
//    //密码输入锁定次数
//
//    /**
//     * 账户Id
//     * @param accountId
//     */
//    public void removeLock(String accountId){
//        remove(LOCK_PREFIX+accountId);
//    }
//
//    /**
//     * 设置锁定次数
//     * @param accountId 账户Id
//     * @param type 用户类型
//     * @param timeout 超时时间 分钟
//     */
//    public void setLock(String accountId,T type,long timeout){
//        set(LOCK_PREFIX+accountId,type,timeout,TimeUnit.SECONDS);
//    }
//    /**
//     * 获取锁定次数
//     * @param accountId 账户Id
//     */
//    public T getLock(String accountId){
//        return  get(LOCK_PREFIX+accountId);
//    }
//
//}
