package com.happy.ipss.user.provider.util;

import com.happy.ipss.exception.HandlerException;
import org.springframework.util.StringUtils;

/**
 * @author yf 2017年12月7日 上午11:14:39
 *
 * @version 1.0
 */
public class RedisExceptionUtil {
    private RedisExceptionUtil(){
        throw new HandlerException("This is a Util Class");
    }

    /**
     * 检测keys值为空的情况。keys为不定长参数
     * @param keys key是否为空
     */
    public static void checkKeyException(String... keys) {
        for (String key : keys) {
            //为空抛出异常
            if (StringUtils.isEmpty(key)) {
                throw new NullPointerException("redis->key不能为空 ");
            }
        }
    }

    /**
     * 检测传入对象是否为null
     * @param obj 对象是否为空
     */
    public static void checkObjectIsNull(Object obj) {
        if (obj == null) {
            throw new NullPointerException("对象不能为空 ");
        }
    }
}
