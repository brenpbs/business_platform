package com.happy.ipss.user.provider.util;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.github.pagehelper.PageInfo;
import com.happy.ipss.dto.BasePageDTO;
import com.happy.ipss.dto.CommonRspDTO;
import com.happy.ipss.dto.Order;
import com.happy.ipss.dto.PageRspDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * 分页 2018-10-26 15:27
 *
 * @author yangk
 **/
public class IpssPageUtil {

    private IpssPageUtil(){
    }

    /**
     * Mybatise PageHelper方式
     *
     * @param pageInfo 分页对象
     * @param <T> 泛型
     * @return PageRspDTO对象
     */
    public static <T> PageRspDTO success(PageInfo<T> pageInfo) {
        long total = 0;
        long pages = 0;
        List<T> list = new ArrayList<>();
        if (pageInfo != null) {
            total = pageInfo.getTotal();
            pages = pageInfo.getPages();
            list = pageInfo.getList();
        }
        return new PageRspDTO(CommonRspDTO.SUCCESS_CODE, "成功", total, pages, list);
    }
    /**
     *
     * JPA springframework.data.domain 方式
     *
     * @param page  分页对象
     * @param <T> 泛型
     * @return PageRspDTO对象
     */
    public static <T> PageRspDTO success(Page<T> page) {
        long total = 0;
        long pages = 0;
        List<T> list = new ArrayList<>();
        if (page != null) {
            total = page.getTotalElements();
            pages = page.getTotalPages();
            list = page.getContent();
        }
        return new PageRspDTO(CommonRspDTO.SUCCESS_CODE, "成功", total, pages, list);
    }

    /**
     * 将排序字段改为JPA使用的Sort对象
     * @param orders
     * @return
     */
    public static Sort orderByToSort(List<Order> orders){
        List<Sort.Order> orderList;
        Sort sort = null;
        if (null != orders &&!orders.isEmpty()){
            orderList = orders.stream()
                    .map(order -> new Sort.Order(Sort.Direction.fromString(order.getDirectionEnum().getCode()),order.getProperty())
                    ).collect(Collectors.toList());

            sort = new Sort(orderList);
        }
        return sort;
    }

    public static void changsToMybatisParam(BasePageDTO basePageDTO){
        List<Order> orders = basePageDTO.getOrderBy();
        if (ObjectUtil.isNotNull(orders) && !orders.isEmpty()){
            for (Order order:orders) {
                order.setProperty(underline(order.getProperty())+" "+order.getDirectionEnum().getCode());
            }
        }
        basePageDTO.setOrderBy(orders);
    }

    /**
     * 驼峰转下划线
     * @param string
     * @return
     */
    private static String underline(String string) {
        if (StrUtil.isBlank(string)){
            return string;
        }
        StringBuffer str = new StringBuffer(string);
        Pattern pattern = Pattern.compile("[A-Z]");
        Matcher matcher = pattern.matcher(str);
        StringBuffer sb = new StringBuffer(str);
        if(matcher.find()) {
            sb = new StringBuffer();
            //将当前匹配子串替换为指定字符串，并且将替换后的子串以及其之前到上次匹配子串之后的字符串段添加到一个StringBuffer对象里。
            //正则之前的字符和被替换的字符
            matcher.appendReplacement(sb,"_"+matcher.group(0).toLowerCase());
            //把之后的也添加到StringBuffer对象里
            matcher.appendTail(sb);
        }else {
            return sb.toString();
        }
        return underline(sb.toString());
    }
}
