package com.happy.ipss.user.provider.util;

import com.happy.ipss.user.api.constant.AccountStateParamEnum;
import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class StringEnumTypeHandler extends BaseTypeHandler<AccountStateParamEnum> {
    @Override
    public void setNonNullParameter(PreparedStatement ps, int i, AccountStateParamEnum parameter, JdbcType jdbcType)
            throws SQLException {
        ps.setString(i, parameter.getCode());
    }

    @Override
    public AccountStateParamEnum getNullableResult(ResultSet rs, String columnName) throws SQLException {
        String val = rs.getString(columnName);
        return getEnum(val);
    }

    @Override
    public AccountStateParamEnum getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
        return getEnum(rs.getString(columnIndex));
    }

    @Override
    public AccountStateParamEnum getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
        return getEnum(cs.getNString(columnIndex));
    }

    /**
     * 根据值获得对应的枚举
     * @param val
     * @return
     */
    private AccountStateParamEnum getEnum(String val){
        Class<AccountStateParamEnum> sexClass = AccountStateParamEnum.class;
        AccountStateParamEnum[] sexs = sexClass.getEnumConstants();

        for(AccountStateParamEnum se:sexs){
            if(se.getCode().equals(val)){
                return se;
            }
        }
        return null;
    }
}
