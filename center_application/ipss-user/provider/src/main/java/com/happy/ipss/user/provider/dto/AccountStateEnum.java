package com.happy.ipss.user.provider.dto;

/**
 * 账户状态 2018-10-21 10:42
 *
 * @author yangk
 */
public enum AccountStateEnum {
    /**
     * 正常
     */
    NORMAL("Y"),
    /**
     * 锁定
     */
    LOCK("N"),
    /**
     * 删除
     */
    DELETE("D");


    AccountStateEnum(String code) {
        this.code = code;
    }

    /**
     * 枚举代码
     */
    private String code;

    /**
     * 返回枚举代码
     *
     * @return 枚举代码
     */
    public String getCode() {
        return code;
    }
}
