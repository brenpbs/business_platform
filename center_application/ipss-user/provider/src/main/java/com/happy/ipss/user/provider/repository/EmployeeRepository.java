package com.happy.ipss.user.provider.repository;

import com.happy.ipss.user.provider.po.Account;
import com.happy.ipss.user.provider.po.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

/**
 * 工作人员
 */
@Component
public interface EmployeeRepository extends JpaRepository<Employee, String> {

    long countByTenantIdAndCodeAndAccountIdNot(String tenantId,String code,String accountId);
    Employee findByAccountId(String accountId);
}
