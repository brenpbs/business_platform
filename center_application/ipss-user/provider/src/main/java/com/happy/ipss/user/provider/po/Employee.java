package com.happy.ipss.user.provider.po;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;

/**
 * 工作人员
 */
@Getter
@Setter
@Entity
@Table(name = "employee")
@EntityListeners(AuditingEntityListener.class)
@DynamicUpdate
public class Employee {

    /**
     * 帐号ID
     */
    @Id
    @Column(name = "account_id")
    private String accountId;

    /**
     * 人员编码
     */
    @Column(name = "code")
    private String code;

    /**
     * 租户ID
     */
    @Column(name = "tenant_id")
    private String tenantId;

    /**
     * 姓名
     */
    @Column(name = "employee_name")
    private String employeeName;

    /**
     * 头像
     */
    @Column(name = "avatar")
    private String avatar;

    /**
     * 身份证号
     */
    @Column(name = "idcard_no")
    private String idcardNo;

    /**
     * 性别  M：男   F：女  U：未知
     */
    @Column(name = "gender")
    private String gender;

    /**
     * Email
     */
    @Column(name = "email")
    private String email;

    /**
     * 办公电话
     */
    @Column(name = "office_phone")
    private String officePhone;

    /**
     * 备用1
     */
    @Column(name = "reserve1")
    private String reserve1;

    /**
     * 备用2
     */
    @Column(name = "reserve2")
    private String reserve2;

    /**
     * 备用3
     */
    @Column(name = "reserve3")
    private String reserve3;

    /**
     * 备注
     */
    @Column(name = "remark")
    private String remark;

    /**
     * 创建时间
     */
    @CreatedDate
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 修改时间
     */
    @LastModifiedDate
    @Column(name = "update_time")
    private Date updateTime;

    /**
     * 维护人
     */
    @Column(name = "maintainer")
    private String maintainer;

}
