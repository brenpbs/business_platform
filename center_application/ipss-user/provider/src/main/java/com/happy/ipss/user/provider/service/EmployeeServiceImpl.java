package com.happy.ipss.user.provider.service;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.boot.hsf.annotation.HSFProvider;
//import com.taobao.hsf.app.spring.util.annotation.HSFProvider;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.happy.ipss.dto.PageRspDTO;
import com.happy.ipss.exception.HandlerException;
import com.happy.ipss.exception.ParamException;
import com.happy.ipss.user.api.constant.AccountTypeEnum;
import com.happy.ipss.user.api.dto.LoginByAccountDTO;
import com.happy.ipss.user.api.dto.employee.EmployeeDTO;
import com.happy.ipss.user.api.dto.employee.EmployeeInfoDTO;
import com.happy.ipss.user.api.dto.employee.EmployeePageResultDTO;
import com.happy.ipss.user.api.dto.employee.EmployeeParamDTO;
import com.happy.ipss.user.api.service.EmployeeService;
import com.happy.ipss.user.provider.dto.AccountStateEnum;
import com.happy.ipss.user.provider.po.Account;
import com.happy.ipss.user.provider.po.Employee;
import com.happy.ipss.user.provider.repository.AccountRepository;
import com.happy.ipss.user.provider.repository.EmployeeRepository;
import com.happy.ipss.user.provider.util.IpssPageUtil;
import com.happy.ipss.user.provider.util.TokenUtil;
import com.happy.ipss.util.BeanValidator;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

/**
 * 工作人员管理
 *
 * @author yww lc
 */
@HSFProvider(serviceInterface = EmployeeService.class, enableTXC = true)
public class EmployeeServiceImpl implements EmployeeService {
    /**
     * token加密盐
     */
    @Value("${login.token.salt}")
    private String tokenSalt;
    /**
     * 验证码失效时间 单位：分钟
     */
    @Value("${login.authcode.time-out}")
    private long authCodeTimeOut;
    /**
     * 验证码短信模板
     */
    @Value("${login.authcode.template-code}")
    private String authCodeTemplateCode;
    /**
     * 验证码长度
     */
    @Value("${login.authcode.length}")
    private int authCodeLength;
    /**
     * token 过期时间 单位：分钟
     */
    @Value("${login.token.time-out}")
    private long tokenTimeOut;
    @Autowired
    private AccountUtil accountUtil;
    @Autowired
    private SqlSessionTemplate sqlSessionTemplate;
//    @Autowired
//    private RedisObjectService<String> redisObjectService;
    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private EmployeeRepository employeeRepository;

    @Override
    public PageRspDTO<EmployeePageResultDTO> findInfoByParam(EmployeeParamDTO employeeParamDTO) {
        BeanValidator.validate(employeeParamDTO);
        final String template = "EmployeeMapper.selectByParam";
        PageInfo<EmployeePageResultDTO> employeePageResultDTOPageInfo = PageHelper.startPage(employeeParamDTO.getPageNum(), employeeParamDTO.getPageSize()).
                doSelectPageInfo(() -> sqlSessionTemplate.selectList(template, employeeParamDTO));
        return IpssPageUtil.success(employeePageResultDTOPageInfo);
    }

    @Override
    public String loginByLoginName(LoginByAccountDTO loginByAccountDTO) {
        return accountUtil.loginByLoginName(loginByAccountDTO, AccountTypeEnum.EMPLOYEE);
    }
    /**
     * YH1113:认证token服务
     *
     * @param token  token值 必填
     * @param expiry token认证成功后延长多少分钟，不填取默认值  可选
     * @return 账户Id
     * @throws com.cwag.pss.ipss.common.exception.HandlerException 业务异常
     */
    @Override
    public String authToken(String token, Long expiry) {
        if (StrUtil.isBlank(token)) {
            throw new ParamException("YH111301:token不能为空");
        }
        String accountId = TokenUtil.getAccountIdByToken(token, tokenSalt);
//        if (StrUtil.isBlank(accountId)) {
//            throw new ParamException("YH111302:token格式错误");
//        }
//        String type = redisObjectService.getToken(accountId);
//        if (StrUtil.isBlank(type)) {
//            throw new HandlerException("YH111303:token无效");
//        }
//        if (expiry <= 0) {
//            expiry = tokenTimeOut;
//        }
//        //延时处理
//        redisObjectService.setToken(accountId, "U", expiry);
        return accountId;
    }
    //查询工作人员信息
    @Override
    public EmployeeInfoDTO findInfo(String accountId) {
        if (StrUtil.isBlank(accountId)) {
            throw new ParamException("YH210301:账户ID不能为空");
        }
        Account account = accountRepository.findByAccountId(accountId);
        if (null == account || AccountStateEnum.DELETE.getCode().equals(account.getState())) {
            throw new HandlerException("YH210302:该账户不存在");
        }
        Employee employee = employeeRepository.findByAccountId(accountId);
        EmployeeDTO employeeDTO = new EmployeeDTO();
        EmployeeInfoDTO employeeInfoDto = new EmployeeInfoDTO();
        BeanUtil.copyProperties(employee, employeeDTO);
        BeanUtil.copyProperties(account, employeeInfoDto);
        employeeInfoDto.setEmployeeDTO(employeeDTO);
        return employeeInfoDto;
    }
}
