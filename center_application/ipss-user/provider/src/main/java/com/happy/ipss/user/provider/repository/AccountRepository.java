package com.happy.ipss.user.provider.repository;

import com.happy.ipss.user.provider.po.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

/**
 * 账户
 */
@Component
public interface AccountRepository extends JpaRepository<Account, String> {


    /**
     * 通过手机号查询未删除的账户
     *
     * @param phone       手机号
     * @param accountType 账户类型
     * @param state       状态
     * @return 账户
     */
    Account findByPhoneAndAccountTypeAndStateNot(String phone, String accountType, String state);

    /**
     * 通过账户名称查询未删除的账户
     *
     * @param accountName 账户名称
     * @param accountType 账户类型
     * @param state       账户状态
     * @return 账户
     */
    Account findByAccountNameAndAccountTypeAndStateNot(String accountName, String accountType, String state);
    Account findByAccountId(String accountId);


}
