//package com.happy.ipss.user.provider.config;
//
//import com.aliyun.openservices.shade.com.alibaba.fastjson.support.spring.FastJsonRedisSerializer;
//import com.fasterxml.jackson.annotation.JsonAutoDetect;
//import com.fasterxml.jackson.annotation.PropertyAccessor;
//import com.fasterxml.jackson.databind.ObjectMapper;
//import org.springframework.beans.factory.annotation.Qualifier;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.data.redis.connection.RedisConnectionFactory;
//import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
//import org.springframework.data.redis.core.*;
//import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
//import org.springframework.data.redis.serializer.StringRedisSerializer;
//
//@Configuration
//public class RedisConfig {
//    /**
//     *
//     * @param jackson2JsonRedisSerializer jackson2JsonRedisSerializer
//     * @param jedisConnectionFactory jedisConnectionFactory
//     * @return RedisTemplate
//     */
////    @Bean(name = "redisTemplate")
////    RedisTemplate<String, Object> objRedisTemplate(Jackson2JsonRedisSerializer<Object> jackson2JsonRedisSerializer, JedisConnectionFactory jedisConnectionFactory) {
////        RedisTemplate<String, Object> redisTemplate = new RedisTemplate<>();
////        //连接
////        redisTemplate.setConnectionFactory(jedisConnectionFactory);
////        redisTemplate.setDefaultSerializer(jackson2JsonRedisSerializer);
////        redisTemplate.setKeySerializer(new StringRedisSerializer());
////        redisTemplate.setHashKeySerializer(new StringRedisSerializer());
////        return redisTemplate;
////    }
//    @Bean(name="redisTemplate")
//    public RedisTemplate<Object, Object> redisTemplate(RedisConnectionFactory redisConnectionFactory) {
//        RedisTemplate<Object, Object> template = new RedisTemplate<>();
//        template.setConnectionFactory(redisConnectionFactory);
//
//        FastJsonRedisSerializer<Object> serializer = new FastJsonRedisSerializer<Object>(Object.class);
//        // value值的序列化采用fastJsonRedisSerializer
//        template.setValueSerializer(serializer);
//        template.setHashValueSerializer(serializer);
//        // key的序列化采用StringRedisSerializer
//        template.setKeySerializer(new StringRedisSerializer());
//        template.setHashKeySerializer(new StringRedisSerializer());
//
//        template.setConnectionFactory(redisConnectionFactory);
//        return template;
//    }
//
//    /**
//     * 功能描述 跟JacksonJsonRedisSerializer实际上是一样的
//     * 它不仅可以将对象序列化，还可以将对象转换为json字符串并保存到redis中，但需要和jackson配合一起使用。用JacksonJsonRedisSerializer序列化的话，
//     * 被序列化的对象不用实现Serializable接口。Jackson是利用反射和getter和setter方法进行读取的，如果不想因为getter和setter方法来影响存储，
//     * 就要使用注解来定义被序列化的对象。
//     * @return Jackson2JsonRedisSerializer
//     */
//    @Bean
//    Jackson2JsonRedisSerializer<Object> jackson2JsonRedisSerializer() {
//        Jackson2JsonRedisSerializer<Object> jackson2JsonRedisSerializer = new Jackson2JsonRedisSerializer<>(
//                Object.class);
//        ObjectMapper om = new ObjectMapper();
//        om.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
//        om.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
//        jackson2JsonRedisSerializer.setObjectMapper(om);
//        return jackson2JsonRedisSerializer;
//    }
//
//
//    /**
//     * 实例化 HashOperations 对象,可以使用 Hash 类型操作
//     * @param redisTemplate redisTemplate
//     * @return HashOperations
//     */
//    @Bean(name = "hashOperations")
//    public HashOperations<String, String, Object> hashOperations(@Qualifier("redisTemplate") RedisTemplate<String, Object> redisTemplate) {
//        return redisTemplate.opsForHash();
//    }
//
//
//    /**
//     * 实例化 ValueOperations 对象,可以使用 String 操作
//     * @param redisTemplate redisTemplate
//     * @return ValueOperations
//     */
//
//    @Bean(name = "valueOperations")
//    public ValueOperations<String, Object> valueOperations(@Qualifier("redisTemplate") RedisTemplate<String, Object> redisTemplate) {
//        return redisTemplate.opsForValue();
//    }
//
//
//    /**
//     * 实例化 ListOperations 对象,可以使用 List 操作
//     * @param redisTemplate redisTemplate
//     * @return ListOperations
//     */
//    @Bean
//    public ListOperations<String, Object> listOperations(@Qualifier("redisTemplate") RedisTemplate<String, Object> redisTemplate) {
//        return redisTemplate.opsForList();
//    }
//
//    /**
//     * 实例化 SetOperations 对象,可以使用 Set 操作
//     * @param redisTemplate redisTemplate
//     * @return SetOperations
//     */
//    @Bean
//    public SetOperations<String, Object> setOperations(@Qualifier("redisTemplate") RedisTemplate<String, Object> redisTemplate) {
//        return redisTemplate.opsForSet();
//    }
//
//    /**
//     * 实例化 ZSetOperations 对象,可以使用 ZSet 操作
//     * @param redisTemplate redisTemplate
//     * @return ZSetOperations
//     */
//    @Bean
//    public ZSetOperations<String, Object> zSetOperations(@Qualifier("redisTemplate") RedisTemplate<String, Object> redisTemplate) {
//        return redisTemplate.opsForZSet();
//    }
//
//}
