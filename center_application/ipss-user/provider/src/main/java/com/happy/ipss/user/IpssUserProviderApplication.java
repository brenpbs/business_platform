package com.happy.ipss.user;


import com.taobao.pandora.boot.PandoraBootstrap;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@ComponentScan({"com.happy.ipss.user.provider"})
@EnableJpaAuditing
public class IpssUserProviderApplication {
    public static void main(String[] args) {
        //jvm 参数 -Dhsf.server.ip=127.0.0.1 -Dhsf.server.port=12200 -Dpandora.qos.port=12211 -Dhsf.http.port=12212
        // 启动 Pandora Boot 用于加载 Pandora 容器
        PandoraBootstrap.run(args);
        SpringApplication.run(IpssUserProviderApplication.class, args);
        // 标记服务启动完成,并设置线程 wait。防止用户业务代码运行完毕退出后，导致容器退出。
        PandoraBootstrap.markStartupAndWait();
    }
}

