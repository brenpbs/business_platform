package com.happy.ipss.user.provider.service;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.ReUtil;
import com.happy.ipss.exception.HandlerException;
import com.happy.ipss.exception.ParamException;
import com.happy.ipss.user.api.constant.AccountTypeEnum;
import com.happy.ipss.user.api.constant.RegexConstant;
import com.happy.ipss.user.api.dto.LoginByAccountDTO;
import com.happy.ipss.user.provider.dto.AccountStateEnum;
import com.happy.ipss.user.provider.po.Account;
import com.happy.ipss.user.provider.repository.AccountRepository;
import com.happy.ipss.user.provider.util.TokenUtil;
import com.happy.ipss.util.BeanValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * 2018-10-31 16:24
 *
 * @author yangk
 **/
@Service
@Slf4j
public class AccountUtil {

    /**
     * token加密盐
     */
    @Value("${login.token.salt}")
    private String tokenSalt;
    /**
     * 多少秒连续登录失败N次锁定账户
     */
    @Value("${login.lock.time-limt}")
    private int lockTimeLimt;
    /**
     * 多少次锁定
     */
    @Value("${login.lock.count}")
    private int lockCount;
    /**
     * token 过期时间
     */
    @Value("${login.token.time-out}")
    private long tokenTimeOut;
    @Autowired
    private AccountRepository accountRepository;
//    @Autowired
//    private RedisObjectService<String> redisObjectService;

    /**
     * 通过账户密码登录
     *
     * @param loginByAccountDTO 登录信息
     * @param accountTypeEnum   账户类型
     * @return token
     * @throws ParamException   参数异常
     * @throws HandlerException 业务异常
     */

    protected String loginByLoginName(LoginByAccountDTO loginByAccountDTO, AccountTypeEnum accountTypeEnum) {
        BeanValidator.validate(loginByAccountDTO);
        //判断登录名是否为手机号
        Boolean isPhone = ReUtil.isMatch(RegexConstant.REGEX_PHONE, loginByAccountDTO.getLoginName());
        Boolean isUserNmae = ReUtil.isMatch(RegexConstant.REGEX_USERNAME, loginByAccountDTO.getLoginName());
        //如果不为手机号，则认为账户名称，判断账户名称格式是否正确
        if (!isPhone && !isUserNmae) {
            throw new ParamException("YH230401:账户名称格式不正确");
        }
        //判断是否存在账户
        Account account = null;
        if (isPhone) {
            account = accountRepository.findByPhoneAndAccountTypeAndStateNot(loginByAccountDTO.getLoginName(), accountTypeEnum.getCode(), AccountStateEnum.DELETE.getCode());
        } else {
            account = accountRepository.findByAccountNameAndAccountTypeAndStateNot(loginByAccountDTO.getLoginName(), accountTypeEnum.getCode(), AccountStateEnum.DELETE.getCode());
        }
        if (ObjectUtil.isNull(account)) {
            throw new HandlerException("YH230402:账户不存在");
        }
        //判断账户已锁定
        if (AccountStateEnum.LOCK.getCode().equals(account.getState())) {
            throw new HandlerException("YH230403:账户已锁定");
        }

        //加密传入密码，判断密码是否正确
        if (!account.getPassword().equals(loginByAccountDTO.getPassword())) {
            //如果密码输入次数等于N次  则锁定账户
//            String errorStr = redisObjectService.getLock(account.getAccountId());
//            int errorCount = StrUtil.isNotBlank(errorStr) ? Integer.parseInt(errorStr) : 0;
//            //记录登录错误次数
//            if (ObjectUtil.isNotNull(errorCount)) {
//                errorCount++;
//                //判断是否等于限制次数
//                if (errorCount == lockCount) {
//                    //锁定账户
//                    account.setState(AccountStateEnum.LOCK.getCode());
//                    accountRepository.save(account);
//                    log.info("账户{}连续登录失败{}次，账户被锁定！", account.getAccountId(), lockCount);
//                    redisObjectService.removeLock(account.getAccountId());
//                    throw new HandlerException("YH230405:您的账户连续登录失败" + lockCount + "次，该账户已被锁定");
//                }
//                redisObjectService.setLock(account.getAccountId(), String.valueOf(errorCount), lockTimeLimt);
//            } else {
//                errorCount = 1;
//                redisObjectService.setLock(account.getAccountId(), String.valueOf(errorCount), lockTimeLimt);
//            }
//            throw new HandlerException("YH230404:密码错误，您还有" + (lockCount - errorCount) + "次机会尝试！");
            throw new HandlerException("YH230404:密码错误");
        }
        //密码正确后生成token；token存储账户Id
        String token = TokenUtil.getJWTToken(account.getAccountId(), tokenSalt);
        //将账户Id存储在redis中，key值为账户Id；
        // 设置有效时长
        Long expire = loginByAccountDTO.getExpiry();
        if (expire == null || expire <= 0) {
            expire = tokenTimeOut;
        }
//        redisObjectService.setToken(account.getAccountId(), accountTypeEnum.getCode(), expire);
//        //登录成功后删除redis中的失败次数
//        redisObjectService.removeLock(account.getAccountId());
        return token;
    }

}
