package com.happy.ipss.user.provider.dto;

import com.happy.ipss.dto.BasePageDTO;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AccountQueryParamDTO extends BasePageDTO {

    /**
     * 登录名--模糊匹配
     */
    private String accountName;
    /**
     * 手机号--模糊匹配
     */
    private String phone;
    /**
     * 状态
     */
    private String state;
    /**
     * 类型
     */
    private String type;
}
