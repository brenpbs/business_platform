package com.happy.ipss.user.provider.aspect;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.date.TimeInterval;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import com.alibaba.dubbo.rpc.RpcContext;
import com.happy.ipss.exception.ParamException;
import com.happy.ipss.exception.HandlerException;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

/**
 * 通过Aop拦截  统一打印日志
 */
@Component
@Aspect
@Slf4j
public class CheckAndLogAop {

    @Pointcut("execution(* com.cwag.pss.ipss.*.provider.service..*.*(..))")
    public void apiLogAop() {
        //
    }

    private static final String ERROR_LOG ="流水号:{},业务描述:{},中心:响应 ===== {}.{}() end =====,响应时间:{}毫秒,响应内容:{}";
    @Around("apiLogAop()")
    public Object aroundApi(ProceedingJoinPoint point) throws Throwable {
        String declaringType = point.getSignature().getDeclaringTypeName();
        String signature = point.getSignature().getName();
        String args = point.getArgs() == null ? "" : JSONUtil.toJsonStr(point.getArgs());
        String busSeq =RpcContext.getContext().getAttachment("busSeq");
        String reqDesc = RpcContext.getContext().getAttachment("reqDesc");
        log.info("流水号:{},业务描述:{},中心:请求 ===== {}.{}() start =====,参数:{}",busSeq,reqDesc,declaringType, signature, args);
        TimeInterval timer = DateUtil.timer();
        Object response = null;
        try {
            //执行该方法
            response = point.proceed();
        } catch (HandlerException| ParamException e) {
            if (!StrUtil.isBlank(e.getMessage())) {
                log.error(ERROR_LOG,busSeq,reqDesc,point.getSignature().getDeclaringTypeName(), point.getSignature().getName(), timer.interval(), JSONUtil.toJsonStr(e.getMessage()));
            }
            throw e;
        } catch (Throwable e) {
            log.error(ERROR_LOG,busSeq,reqDesc,point.getSignature().getDeclaringTypeName(), point.getSignature().getName(), timer.interval(), JSONUtil.toJsonStr(e));
            throw e;
        }
        String info =response == null ? "" : JSONUtil.toJsonStr(response);
        log.info(ERROR_LOG, busSeq,reqDesc,point.getSignature().getDeclaringTypeName(), point.getSignature().getName(), timer.interval(), info);
        return response;
    }
}
