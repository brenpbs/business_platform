package com.happy.ipss.user.api.constant;

/**
 * 常用正则
 */
public final class RegexConstant {
    private RegexConstant(){
        super();
    }
    /**
     * 正则--用户名：由字母开头,长度为3-20位组成
     */
    public static final String REGEX_USERNAME = "^[a-zA-Z][a-zA-Z0-9]{3,19}$";
    /**
     * 正则--手机号
     */
    public static final String REGEX_PHONE = "^1\\d{10}$";
    /**
     * 正则--密码：由大小写字母、数字组成  长度为6-20位
     */
    public static final String REGEX_CIPHER = "^[a-zA-Z0-9]{6,20}$";
    /**
     * 正则--身份证
     */
    public static final String REGEX_ID_CARD = "(^\\d{18}$)|(^\\d{15}$)";
}
