package com.happy.ipss.user.api.dto.account;


import com.happy.ipss.user.api.constant.RegexConstant;
import com.happy.ipss.user.api.dto.BaseAddDTO;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;


/**
 * 添加账户 入参
 */
@Setter
@Getter
public class AccountAddDTO extends BaseAddDTO {

    /**
     * 账号名称-字母开头; 必填;
     * 账户名称格式由字母开头,长度为3-20位组成
     */
    @NotNull(message = "账户名称不能为空")
    @Pattern(regexp = RegexConstant.REGEX_USERNAME,message = "账户名称格式由字母开头,长度为3-20位组成")
    private String accountName;
    /**
     * 电话 必填
     */
    @NotBlank(message = "手机号不能为空")
//    @Pattern(regexp = RegexConstant.REGEX_PHONE, message = "手机号格式不正确")
    private String phone;
    /**
     * 密码-明文 必填;
     * 由大小写字母+数字组成  长度为6-20位
     */
    @NotBlank(message = "YH110103:密码不能为空")
    @Pattern(regexp = RegexConstant.REGEX_CIPHER, message = "密码格式不正确")
    private String password;

}
