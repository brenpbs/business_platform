package com.happy.ipss.user.api.constant;

/**
 * 组织类型 2018-10-24 15:14
 *
 * @author yangk
 **/
public enum GroupTypeEnum {
    /**
     * 租户
     */
    TENANT("T"),
    /**
     * 商户
     */
    VENDOR("V");

    GroupTypeEnum(String code) {
        this.code = code;
    }

    /**
     * 枚举代码
     */
    private String code;

    /**
     * 返回枚举代码
     *
     * @return 枚举代码
     */
    public String getCode() {
        return code;
    }

}
