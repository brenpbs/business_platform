package com.happy.ipss.user.api.constant;

/**
 * 第三方账户类型 2018-10-24 10:14
 *
 * @author yangk
 */
public enum ThirdPartyTypeEnum {

    QQ("QQ"),
    WE_CHAT("WECHAT"),
    MICRO_BLOG("MICROBLOG");

    ThirdPartyTypeEnum(String code) {
        this.code = code;
    }

    /**
     * 枚举代码
     */
    private String code;

    /**
     * 返回枚举代码
     *
     * @return 枚举代码
     */
    public String getCode() {
        return code;
    }

}
