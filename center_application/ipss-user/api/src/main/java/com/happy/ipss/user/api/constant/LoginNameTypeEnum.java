package com.happy.ipss.user.api.constant;

public enum LoginNameTypeEnum {
    /**
     * 手机号
     */
    PHONE,
    /**
     * 账户名
     */
    ACCOUNT_NAME;
}
