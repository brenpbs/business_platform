package com.happy.ipss.user.api.constant;

/**
 * 商户状态 2018-10-24 9:14
 *
 * @author yangk
 **/
public enum VendorStateEnum {
    /**
     * 正常
     */
    NORMAL("Y"),
    /**
     * 已删除
     */
    DELETE("D");

    VendorStateEnum(String code) {
        this.code = code;
    }

    /**
     * 枚举代码
     */
    private String code;

    /**
     * 返回枚举代码
     *
     * @return 枚举代码
     */
    public String getCode() {
        return code;
    }
}
