package com.happy.ipss.user.api.dto.account;

import com.happy.ipss.dto.BasePageDTO;
import com.happy.ipss.user.api.constant.AccountStateParamEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
/**
 * 分页查询账户信息 入参
 */
@Setter
@Getter
public class AccountParamDTO extends BasePageDTO {

    /**
     * 登录名--模糊匹配
     */
    @Length(max = 32,message = "账户名最长为20字符")
    private String accountName;
    /**
     * 手机号--模糊匹配
     */
    @Length(max = 14,message = "手机号最长为14字符")
    private String phone;
    /**
     * 状态
     */
    private AccountStateParamEnum state;

}
