package com.happy.ipss.user.api.constant;

/**
 * 性别
 */
public enum GenderEnum {
    /**
     * 男
     */
    MALE("M"),
    /**
     * 女
     */
    FEMALE("F"),
    /**
     * 未知
     */
    UNKNOW("U");

    GenderEnum(String code) {
        this.code = code;
    }

    /**
     * 枚举代码
     */
    private String code;

    /**
     * 返回枚举代码
     *
     * @return 枚举代码
     */
    public String getCode() {
        return code;
    }
}
