package com.happy.ipss.user.api.dto.account;

import com.happy.ipss.user.api.dto.BaseAddDTO;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

@Getter
@Setter
public class AccountUpdateDTO extends BaseAddDTO {

    /**
     * 账户ID 必填
     */
    @NotBlank(message = "账户ID不能为空")
    private String accountId;

    /**
     * 国籍
     */
    @Length(min = 1,max = 50,message = "国籍长度为1~50字符")
    private String nationality;
    /**
     * 省
     */
    @Length(min = 1,max = 50,message = "省长度为1~50字符")
    private String province;
    /**
     * 市
     */
    @Length(min = 1,max = 50,message = "市长度为1~50字符")
    private String city;
    /**
     * 区/县
     */
    @Length(min = 1,max = 50,message = "区长度为1~50字符")
    private String district;
    /**
     * 街道
     */
    @Length(min = 1,max = 100,message = "街道长度为1~50字符")
    private String subdistrict;
}
