package com.happy.ipss.user.api.dto.account;


import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * 账户详情
 */
@Getter
@Setter
public class AccountDTO {
    /**
     * 账号ID
     */
    private String accountId;
    /**
     * 账号名称
     */
    private String accountName;
    /**
     * 手机号
     */
    private String phone;
    /**
     * 账户类型 E:工作人员  S:服务人员  P:旅客
     */
    private String accountType;
    /**
     * 状态 Y：正常  N:锁定  D:已删除
     */
    private String state;
    /**
     * 最后登录时间
     */
    private Date lastLoginTime;
    /**
     * 最后登录IP
     */
    private String lastLoginIp;
    /**
     * 国籍
     */
    private String nationality;
    /**
     * 省
     */
    private String province;
    /**
     * 市
     */
    private String city;
    /**
     * 区/县
     */
    private String district;
    /**
     * 街道
     */
    private String subdistrict;
    /**
     * 备用1
     */
    private String reserve1;
    /**
     * 备用2
     */
    private String reserve2;
    /**
     * 备用3
     */
    private String reserve3;
    /**
     * 备注
     */
    private String remark;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 修改时间
     */
    private Date updateTime;
    /**
     * 维护人
     */
    private String maintainer;
}
