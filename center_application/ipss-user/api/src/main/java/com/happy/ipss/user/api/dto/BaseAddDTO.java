package com.happy.ipss.user.api.dto;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

/**
 * 添加基础类
 */
@Setter
@Getter
public class BaseAddDTO {

    /**
     * 备用1 ;最大长度不能超过200字符
     */
    @Length(max = 200, message = "备用1最大长度不能超过200字符")
    private String reserve1;

    /**
     * 备用2 ;最大长度不能超过200字符
     */
    @Length(max = 200, message = "备用2最大长度不能超过200字符")
    private String reserve2;
    /**
     * 备用3 ;最大长度不能超过200字符
     */
    @Length(max = 200, message = "备用3最大长度不能超过200字符")
    private String reserve3;
    /**
     * 备注 ;最大长度不能超过500字符
     */
    @Length(max = 500, message = "备注最大长度不能超过500字符")
    private String remark;
    /**
     * 维护人 必填;最大长度不能超过32字符
     */
    @NotBlank(message = "维护人不能为空")
    @Length(max = 32, message = "维护人最大长度不能超过32字符")
    private String maintainer;

}
