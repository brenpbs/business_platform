package com.happy.ipss.user.api.dto.employee;


import com.happy.ipss.user.api.constant.GenderEnum;
import com.happy.ipss.user.api.dto.account.AccountAddDTO;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

/**
 * 添加工作人员DTO
 */
@Getter
@Setter
public class EmployeeAddDTO extends AccountAddDTO {

    /**
     * 人员编码
     */
    @Length(max = 20,message = "人员编码不能超过20位字符")
    private String code;

    /**
     * 租户ID
     */
    @NotBlank(message = "租户ID不能为空")
    @Length(min = 32,max = 32,message = "租户ID为32位字符")
    private String tenantId;

    /**
     * 姓名
     */
    @Length(max = 32,message = "姓名不能超过32位字符")
    private String employeeName;

    /**
     * 头像
     */
    @Length(max = 500,message = "头像不能超过500位字符")
    private String avatar;

    /**
     * 身份证号
     */
    @Length(max = 18,message = "身份证号不能超过18位字符")
    private String idcardNo;

    /**
     * 性别  M：男   F：女  U：未知
     */
    private GenderEnum genderEnum;

    /**
     * Email
     */
    @Length(max = 50,message = "Email不能超过50位字符")
    private String email;

    /**
     * 办公电话
     */
    @Length(max = 20,message = "办公电话不能超过20位字符")
    private String officePhone;
}
