package com.happy.ipss.user.api.dto.employee;

import com.happy.ipss.user.api.constant.AccountStateParamEnum;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Max;


/**
 * 工作人员分页查询 入参
 *
 * @author lc
 */
@Getter
@Setter
public class EmployeeParamDTO{

    /**
     * 登录名--模糊匹配
     */
    private String accountName;
    /**
     * 手机号--模糊匹配
     */
    private String phone;
    /**
     * 状态
     */
    private AccountStateParamEnum stateEnum;

    /**
     * 人员编码--可选 模糊匹配
     */
    private String code;

    /**
     * 租户ID--可选
     */
    private String tenantId;

    /**
     * 姓名--可选 模糊匹配
     */
    private String employeeName;

    /**
     * 身份证号--可选 模糊匹配
     */
    private String idcardNo;

    /**
     * 性别  M：男   F：女  U：未知--可选
     */

    private String gender;
    /**
     * 当前页
     */
    private int pageNum;
    /**
     * 每页条数
     */
    @Max(
            value = 200L,
            message = "每页显示不能超过200条记录"
    )
    private int pageSize;

    public void setPageNum(int pageNum) {
        if (pageNum < 1) {
            pageNum = 1;
        }
        this.pageNum = pageNum;
    }
}
