package com.happy.ipss.user.api.dto;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

/**
 * 通过账户登录 2018-10-22 17:35
 *
 * @author yangk
 **/
@Getter
@Setter
public class LoginByAccountDTO {

    /**
     * 登录名（手机号、账户名称） ;必传
     */
    @NotBlank(message = "登录名不能为空")
    private String loginName;

    @NotBlank(message = "应用Code不能为空")
    private String applicationCode;
    /**
     * 密码 ;必传;
     * 由大小写字母、数字组成  长度为6-20位）
     */
    @NotBlank(message = "密码不能为空")
    private String password;
    /**
     * token失效时间 分种
     */
    private Long expiry;
    /**
     * 登录IP（可选）;最大不能超过100字符
     */
    @Length(max = 100, message = "登录IP最大不能超过100字符")
    private String loginIp;
    /**
     * 操作人 ;必传
     */
    @NotBlank(message = "操作人不能为空")
    @Length(max = 32, message = "操作人最大不能超过32字符")
    private String maintainer;

}
