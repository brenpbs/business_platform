package com.happy.ipss.user.api.dto.employee;

import com.happy.ipss.user.api.dto.account.AccountDTO;
import lombok.Getter;
import lombok.Setter;

/**
 * 工作人员信息  出参
 */
@Getter
@Setter
public class EmployeeInfoDTO extends AccountDTO {

    /**
     * 工作人员详情
     */
    private EmployeeDTO employeeDTO;

}
