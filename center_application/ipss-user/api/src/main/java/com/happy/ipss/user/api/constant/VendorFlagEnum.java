package com.happy.ipss.user.api.constant;

/**
 * 2018-12-05 18:53
 *
 * @author yangk
 **/
public enum VendorFlagEnum {
    /**
     * 是虚拟用户
     */
    YES("Y"),
    /**
     * 非虚拟用户
     */
    NO("N");

    VendorFlagEnum(String code) {
        this.code = code;
    }

    /**
     * 枚举代码
     */
    private String code;

    /**
     * 返回枚举代码
     *
     * @return 枚举代码
     */
    public String getCode() {
        return code;
    }
}
