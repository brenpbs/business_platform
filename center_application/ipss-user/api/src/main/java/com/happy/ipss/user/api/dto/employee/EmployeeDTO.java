package com.happy.ipss.user.api.dto.employee;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * 工作人员 出参
 *
 * @author lc
 */
@Getter
@Setter
public class EmployeeDTO {

    /**
     * 帐号ID
     */
    private String accountId;

    /**
     * 人员编码
     */
    private String code;

    /**
     * 租户ID
     */
    private String tenantId;

    /**
     * 姓名
     */
    private String employeeName;

    /**
     * 头像
     */
    private String avatar;

    /**
     * 身份证号
     */
    private String idcardNo;

    /**
     * 性别  M：男   F：女  U：未知
     */
    private String gender;

    /**
     * Email
     */
    private String email;

    /**
     * 办公电话
     */
    private String officePhone;

    /**
     * 备用1
     */
    private String reserve1;

    /**
     * 备用2
     */
    private String reserve2;

    /**
     * 备用3
     */
    private String reserve3;

    /**
     * 备注
     */
    private String remark;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 修改时间
     */
    private Date updateTime;

    /**
     * 维护人
     */
    private String maintainer;

}
