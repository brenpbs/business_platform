package com.happy.ipss.user.api.service;

import com.happy.ipss.dto.PageRspDTO;
import com.happy.ipss.user.api.dto.LoginByAccountDTO;
import com.happy.ipss.user.api.dto.employee.EmployeeInfoDTO;
import com.happy.ipss.user.api.dto.employee.EmployeePageResultDTO;
import com.happy.ipss.user.api.dto.employee.EmployeeParamDTO;

/**
 * YH12 用户管理-工作人员信息管理
 *
 * @author yww lc
 */
public interface EmployeeService {

    /**
     * YH2105：通过用户名密码登录
     *
     * @param loginByAccountDTO 登录传参
     * @return 登录成功token
     * @throws com.cwag.pss.ipss.common.exception.ParamException   参数异常
     * @throws com.cwag.pss.ipss.common.exception.HandlerException 业务异常
     */
    String loginByLoginName(LoginByAccountDTO loginByAccountDTO);

    /**
     * YH2107:带参数分页查询
     *
     * @param employeeParamDTO 查询参数
     * @return 查询结果List
     */
    PageRspDTO<EmployeePageResultDTO> findInfoByParam(EmployeeParamDTO employeeParamDTO);
    /**
     * YH1113:认证token服务
     *
     * @param token  token值 必填
     * @param expiry token认证成功后延长多少分种，不填取默认值  可选
     * @return 账户Id
     * @throws com.cwag.pss.ipss.common.exception.ParamException   业务异常
     * @throws com.cwag.pss.ipss.common.exception.HandlerException 业务异常
     */
    String authToken(String token, Long expiry);

    /**
     * YH2103:账户详情
     *
     * @param accountId 账户ID
     * @return 工作人员详情
     */
    EmployeeInfoDTO findInfo(String accountId);
}
