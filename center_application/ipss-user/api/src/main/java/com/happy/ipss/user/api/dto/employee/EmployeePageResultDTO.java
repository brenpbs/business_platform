package com.happy.ipss.user.api.dto.employee;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * 工作人员分页查询结果
 */
@Getter
@Setter
public class EmployeePageResultDTO {

    /**
     * 账号ID
     */
    private String accountId;
    /**
     * 账号名称
     */
    private String accountName;
    /**
     * 手机号
     */
    private String phone;
    /**
     * 状态 Y：正常  N:锁定  D:已删除
     */
    private String state;
    /**
     * 最后登录时间
     */
    private Date lastLoginTime;
    /**
     * 最后登录IP
     */
    private String lastLoginIp;

    /**
     * 人员编码
     */
    private String code;

    /**
     * 租户ID
     */
    private String tenantId;

    /**
     * 姓名
     */
    private String employeeName;

    /**
     * 头像
     */
    private String avatar;

    /**
     * 身份证号
     */
    private String idcardNo;

    /**
     * 性别  M：男   F：女  U：未知
     */
    private String gender;

    /**
     * Email
     */
    private String email;

    /**
     * 办公电话
     */
    private String officePhone;

    /**
     * 备用1
     */
    private String reserve1;

    /**
     * 备用2
     */
    private String reserve2;

    /**
     * 备用3
     */
    private String reserve3;

    /**
     * 备注
     */
    private String remark;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 修改时间
     */
    private Date updateTime;

    /**
     * 维护人
     */
    private String maintainer;
}
