package com.happy.ipss.user.api.constant;

/**
 * 账户状态 2018-10-21 10:42
 *
 * @author yangk
 */
public enum AccountStateParamEnum {
    /**
     * 正常
     */
    NORMAL("Y"),
    /**
     * 锁定
     */
    LOCK("N");

    AccountStateParamEnum(String code) {
        this.code = code;
    }

    /**
     * 枚举代码
     */
    private String code;

    /**
     * 返回枚举代码
     *
     * @return 枚举代码
     */
    public String getCode() {
        return code;
    }
}
