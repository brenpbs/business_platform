package com.happy.ipss.user.api.constant;

/**
 * 账户类型
 * 2018-10-21 10:24
 *
 * @author yangk
 */
public enum AccountTypeEnum {
    /**
     * 工作人员
     */
    EMPLOYEE("E"),
    /**
     * 服务人员
     */
    SERVICE_STAFF("S"),
    /**
     * 旅客
     */
    PASSENGER("P");

    AccountTypeEnum(String code) {
        this.code = code;
    }

    /**
     * 枚举代码
     */
    private String code;

    /**
     * 返回枚举代码
     *
     * @return 枚举代码
     */
    public String getCode() {
        return code;
    }

    /**
     * 根据value返回枚举类型
     *
     * @param value 代码
     * @return 枚举值
     */
    public static AccountTypeEnum getByValue(String value) {
        for (AccountTypeEnum typeEnum : values()) {
            if (value.equals(typeEnum.getCode())) {
                return typeEnum;
            }
        }
        return null;
    }


}
