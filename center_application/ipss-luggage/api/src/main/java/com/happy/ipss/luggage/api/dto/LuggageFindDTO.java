package com.happy.ipss.luggage.api.dto;
import com.happy.ipss.dto.BasePageDTO;
import lombok.Getter;
import lombok.Setter;
/**
 * 行李查询条件
 */
@Getter
@Setter
public class LuggageFindDTO extends BasePageDTO {
    /**
     * 行李号
     */
    private String luggage_number;
    /**
     * 旅客行程
     */
    private String trip_number;
    /**
     * 旅客OneID
     */
    private String one_id;
}
