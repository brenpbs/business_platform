package com.happy.ipss.luggage.api.service;
import com.happy.ipss.dto.PageDTO;
import com.happy.ipss.luggage.api.dto.LuggageFindDTO;
import com.happy.ipss.luggage.api.dto.LuggageFindResultDTO;
import com.happy.ipss.luggage.api.dto.LuggageOperationFindDTO;
import com.happy.ipss.luggage.api.dto.LuggageOperationFindResultDTO;
/**
 * 行李中心查询接口
 */
public interface LuggageService {
    /**
     * 301:按行李条件查询旅客行李基本信息
     * @param luggageFindDTO
     * @return
     */
    PageDTO<LuggageFindResultDTO> findLuggageByParam(LuggageFindDTO luggageFindDTO);

    /**
     * 302 按行李号查询旅客行李轨迹信息
     * @param luggageOperationFindDTO
     * @return
     */
    PageDTO<LuggageOperationFindResultDTO> findLuggageOperationByParam(LuggageOperationFindDTO luggageOperationFindDTO);
}
