package com.happy.ipss.luggage.api.dto;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
/**
 * 行李查询返回结果
 */
@Getter
@Setter
public class LuggageFindResultDTO {
  /**
   * 用户Id
   */
  private String one_id;
  /**
   * 行李编号
   */
  private String luggage_number;
  /**
   * 行李重量
   */
  private String luggage_weight;
  /**
   * 创建时间
   */
  private Date gmt_create;
}
