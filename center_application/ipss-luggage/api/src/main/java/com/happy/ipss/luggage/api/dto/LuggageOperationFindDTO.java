package com.happy.ipss.luggage.api.dto;
import com.happy.ipss.dto.BasePageDTO;
import lombok.Getter;
import lombok.Setter;
/**
 * 行李轨迹查询条件
 */
@Getter
@Setter
public class LuggageOperationFindDTO extends BasePageDTO {
    private String luggage_number;
}
