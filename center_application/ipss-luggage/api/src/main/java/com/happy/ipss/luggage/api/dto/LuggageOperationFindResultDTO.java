package com.happy.ipss.luggage.api.dto;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
/**
 * 行李轨迹查询返回结果
 */
@Getter
@Setter
public class LuggageOperationFindResultDTO {
    /**
     * 行李状态
     */
    private String luggage_status;
    /**
     * 描述
     */
    private String description;
    /**
     * 操作人
     */
    private String maniter;
    /**
     * 操作时间
     */
    private Date gmt_create;
}
