package com.happy.ipss.luggage.provider.service;


import com.happy.ipss.dto.PageDTO;
import com.happy.ipss.luggage.api.dto.LuggageFindDTO;
import com.happy.ipss.luggage.api.dto.LuggageFindResultDTO;
import com.happy.ipss.luggage.api.dto.LuggageOperationFindDTO;
import com.happy.ipss.luggage.api.dto.LuggageOperationFindResultDTO;
import com.happy.ipss.luggage.provider.utils.PageUtil;
import com.happy.ipss.luggage.api.service.LuggageService;
import com.alibaba.boot.hsf.annotation.HSFProvider;
import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
/**
 * 行李中心查询服务
 */
@Slf4j
@HSFProvider(serviceInterface = LuggageService.class)

public class luggageServiceImpl  implements LuggageService{
    @Autowired
    private SqlSessionTemplate sqlSessionTemplate;
    @Autowired
    private PageUtil pageUtil;
    /**
     * 301:按行李条件旅客行李基本信息
     * @param luggageFindDTO 行李查询条件
     * @return
     */
    @Override
    public PageDTO<LuggageFindResultDTO> findLuggageByParam(LuggageFindDTO luggageFindDTO) {
     //   log.info("行李查询 根据参数查询行李："+ JSONUtil.toJsonStr(luggageFindDTO));
        final String template = "LuggageMapper.selectLuggageByParam";
        return pageUtil.selectPage(template,luggageFindDTO);
   }
    /**
     * 302 按行李号查询旅客行李轨迹信息
     * @param luggageOperationFindDTO 行李轨迹查询条件
     * @return
     */
    @Override
    public PageDTO<LuggageOperationFindResultDTO>
    findLuggageOperationByParam(LuggageOperationFindDTO luggageOperationFindDTO) {
     //   log.info(" 行李查询 根据参数查询轨迹："+ JSONUtil.toJsonStr(luggageOperationFindDTO));
        final String template = "LuggageOperarionMapper.selectLuggageOperationByParam";
        return pageUtil.selectPage(template,luggageOperationFindDTO);
    }
}
