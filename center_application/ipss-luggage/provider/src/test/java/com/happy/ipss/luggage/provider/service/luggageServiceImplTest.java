//package com.happy.ipss.luggage.provider.service;
//import com.happy.ipss.dto.PageDTO;
//import com.happy.ipss.luggage.api.dto.LuggageFindDTO;
//import com.happy.ipss.luggage.api.dto.LuggageFindResultDTO;
//import com.happy.ipss.luggage.api.dto.LuggageOperationFindDTO;
//import com.happy.ipss.luggage.api.dto.LuggageOperationFindResultDTO;
//import com.happy.ipss.luggage.provider.IpssLuggageProviderApplication;
//import com.happy.ipss.luggage.api.service.LuggageService;
//import com.taobao.hsf.app.spring.util.annotation.HSFConsumer;
//import com.taobao.pandora.boot.test.junit4.DelegateTo;
//import com.taobao.pandora.boot.test.junit4.PandoraBootRunner;
//import junit.framework.TestCase;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.stereotype.Component;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//
//@RunWith(PandoraBootRunner.class)
//@DelegateTo(SpringJUnit4ClassRunner.class)
//// 加载测试需要的类，一定要加入 Spring Boot 的启动类，其次需要加入本类。
//@SpringBootTest(classes = {IpssLuggageProviderApplication.class, luggageServiceImplTest.class })
//@Component
//public class luggageServiceImplTest {
//    @HSFConsumer(generic = true)
//    LuggageService luggageService;
//    @Test
//    public void findLuggageByParam(){
//        LuggageFindDTO luggageFindDTO = new LuggageFindDTO();
//        luggageFindDTO.setLuggage_number("3876420645");
//        PageDTO<LuggageFindResultDTO> LuggageFindResultDTO =
//                luggageService.findLuggageByParam(luggageFindDTO);
//        TestCase.assertEquals(LuggageFindResultDTO.getList().size(),1);
//    }
//    @Test
//    public void findTripInfos(){
//        LuggageOperationFindDTO luggageOperationFindDTO = new LuggageOperationFindDTO();
//        luggageOperationFindDTO.setLuggage_number("3876420646");
//        PageDTO<LuggageOperationFindResultDTO> luggageOperationFindResultDTO =
//                luggageService.findLuggageOperationByParam(luggageOperationFindDTO);
//        TestCase.assertEquals(luggageOperationFindResultDTO.getList().size(),2);
//    }
//}
