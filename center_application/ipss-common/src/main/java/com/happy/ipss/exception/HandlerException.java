package com.happy.ipss.exception;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * 自定义异常
 */
public class HandlerException  extends  RuntimeException implements Serializable{

    private static final long serialVersionUID = -1037904232872559487L;
    public HandlerException(String message){
        super(message);
        this.message = message;
    }
    @Setter
    @Getter
    private String message;
}
