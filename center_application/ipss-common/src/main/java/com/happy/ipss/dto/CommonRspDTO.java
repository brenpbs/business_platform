package com.happy.ipss.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

public class CommonRspDTO {

    /**
     * 服务内部错误编码
     */
    public  static  final String ERROR_CODE = "8500";
    /**
     * 请求成功
     **/
    public  static  final String SUCCESS_CODE = "8200";
    /**
     * 参数错误编码
     */
    public  static  final String ERROR_CODE_PARAM = "8400";


    public CommonRspDTO(String code, String message) {
        this.code = code;
        this.message = message;
    }

    /**
     * 返回状态码
     */
    @Getter
    @Setter
    private String code;
    /**
     * 消息
     */
    @Getter
    @Setter
    private String message;

    public static CommonRspDTO success(){
        return  new CommonRspDTO(SUCCESS_CODE,"成功");
    }

    public static CommonRspDTO error(){
        return  new CommonRspDTO(ERROR_CODE,"内部异常");
    }


    /**
     * 通过HanderException返回单个CommonRspDTO
     * @param codeAndMessage code和message
     * @return 结果
     */
    public static CommonRspDTO getHanderExceptionDTO(String codeAndMessage){
        codeAndMessage = codeAndMessage.replace("：",":");
        String[] vals = codeAndMessage.split(":");
        String code = vals[0];
        String message=null;
        if(vals.length>1){
            message = vals[1];
        }
        return  new CommonRspDTO(code,message);
    }

    /**
     * 通过ParamException返回多个CommonRspDTO
     * @param codeAndMessages 错误列表
     * @return 结果
     */
    public static CommonRspDTO getParamExceptionDTO(List<String> codeAndMessages){
        return  new CommonRspDTO(ERROR_CODE_PARAM,codeAndMessages.toString());
    }
}
