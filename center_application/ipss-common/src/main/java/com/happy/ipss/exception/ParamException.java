package com.happy.ipss.exception;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
/**
 * 参数校验异常
 **/

public class ParamException extends  RuntimeException implements Serializable {
    private static final long serialVersionUID = 3874496319995892829L;

    public ParamException(String messages){
        super(messages);
        this.messages = messages;
    }
    @Setter
    @Getter
    private String messages;
}
