package com.happy.ipss.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Order {

    /**
     * 排序顺序
     */
    private DirectionEnum directionEnum = DirectionEnum.ASC;

    /**
     * 排序字段
     */
    private String property;
    public Order(){} //无参构造

    public Order(DirectionEnum directionEnum, String property) {
        this.directionEnum = directionEnum;
        this.property = property;
    }
}
