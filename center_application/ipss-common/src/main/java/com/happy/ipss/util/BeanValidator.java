package com.happy.ipss.util;


import com.happy.ipss.dto.CommonRspDTO;
import com.happy.ipss.exception.ParamException;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 对象验证器
 */
public class BeanValidator {

    /**
     * 验证某个bean的参数
     *
     * @param object 被校验的参数
     * @param <T>    泛型
     * @throws ParamException 如果参数校验不成功则抛出此异常
     */
    public static <T> void validate(T object) {

        if (object == null) {
            throw new ParamException(CommonRspDTO.ERROR_CODE_PARAM + ":接口入参不能为空");
        }
        //获得验证器
        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
        //执行验证
        Set<ConstraintViolation<T>> constraintViolations = validator.validate(object);
        //如果有验证信息，则取出来包装成异常返回
        if (constraintViolations == null || constraintViolations.size() == 0) {
            return;
        }
        throw new ParamException(convertErrorMsg(constraintViolations));
    }

    /**
     * 验证某个bean的参数
     *
     * @param list 泛型集合
     * @throws ParamException 如果参数校验不成功则抛出此异常
     */
    public static <T> void validate(List<T> list) {

        if (list == null || list.size() == 0) {
            throw new ParamException(CommonRspDTO.ERROR_CODE_PARAM + ":接口入参不能为空");
        }
        for (T t : list) {
            try {
                validate(t);
            } catch (ParamException e) {
                throw new ParamException(String.format("%s,异常对象:%s",e.getMessages(),t) );
            }
        }
    }

    //转换异常信息
    private static <T> String convertErrorMsg(Set<ConstraintViolation<T>> set) {
        Map<String, StringBuilder> errorMap = new HashMap<>();
        String property;
        for (ConstraintViolation<T> cv : set) {
            //这里循环获取错误信息，可以自定义格式
            property = cv.getPropertyPath().toString();
            if (errorMap.get(property) != null) {
                errorMap.get(property).append("," + cv.getMessage());
            } else {
                StringBuilder sb = new StringBuilder();
                sb.append(cv.getMessage());
                errorMap.put(property, sb);
            }
        }
        return CommonRspDTO.ERROR_CODE_PARAM + ":" + errorMap.toString();
    }
}
