package com.happy.ipss.dto;

public enum DirectionEnum {

    /**
     * 正序
     */
    ASC("ASC"),
    /**
     * 倒序
     */
    DESC("DESC");
    DirectionEnum(String code) {
        this.code = code;
    }
    /**
     * 枚举代码
     */
    private String code;

    /**
     * 返回枚举代码
     *
     * @return 枚举代码
     */
    public String getCode() {
        return code;
    }
}
