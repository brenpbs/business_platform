package com.happy.ipss.passenger.api.dto;
import com.happy.ipss.dto.BasePageDTO;
import lombok.Getter;
import lombok.Setter;
/**
 * 旅客信息查询条件
 */
@Getter
@Setter
public class PassengerParamDTO extends BasePageDTO {

    /**
     * 会员名称
     */
    private String memberName;
    /**
     * 证件号码
     */
    private String certificateNumber;
    /**
     * 手机号码
     */
    private String memberPhone;

}
