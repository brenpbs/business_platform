package com.happy.ipss.passenger.api.service;
import com.happy.ipss.dto.PageDTO;
import com.happy.ipss.passenger.api.dto.PassengerParamDTO;
import com.happy.ipss.passenger.api.dto.PassengerResultDTO;
public interface PassengerService {
    /**
     * 101:根据旅客OneID查询旅客基本信息
     * @param id    旅客OneID
     * @return
     */
    PassengerResultDTO findById(String id);

    /**
     * 102:根据查询条件查询旅客列表
     * @param passengerParamDTO
     * @return
     */
    PageDTO<PassengerResultDTO> findByParam(PassengerParamDTO passengerParamDTO);
}
