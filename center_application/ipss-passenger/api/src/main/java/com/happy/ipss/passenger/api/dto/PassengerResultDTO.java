package com.happy.ipss.passenger.api.dto;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
/**
 *旅客信息返回信息
 */
@Getter
@Setter
public class PassengerResultDTO {
    /**
     * 旅客oneId
     */
    private String oneId;
    /**
     * 旅客姓名
     */
    private String memberName;
    /**
     * 证件号码
     */
    private String certificateNumber;
    /**
     * 手机号码
     */
    private String memberPhone;
    /**
     * 会员等级
     */
    private Integer memberLevel;
    /**
     * 是否删除
     */
    private Byte isDeleted;
    /**
     * 创建时间
     */
    private Date gmtCreate;
    /**
     * 修改时间
     */
    private Date gmtModified;
    /**
     * 备注
     */
    private String remark;
    /**
     * 保留域1
     */
    private String spare1;
    /**
     * 保留域2
     */
    private String spare2;
    /**
     * 保留域3
     */
    private String spare3;
}
