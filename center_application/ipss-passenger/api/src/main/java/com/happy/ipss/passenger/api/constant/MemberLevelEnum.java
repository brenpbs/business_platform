package com.happy.ipss.passenger.api.constant;

public enum MemberLevelEnum {
    /**
     * 初级会员
     */
    JUNIOR("J"),
    /**
     * 中级会员
     */
    INTERMEDIATE("I"),
    /**
     * 高级会员
     */
    SENIOR("S");
    MemberLevelEnum(String code) {
        this.code = code;
    }
    /**
     * 枚举代码
     */
    private String code;
    /**
     * 返回枚举代码
     *
     * @return 枚举代码
     */
    public String getCode() {
        return code;
    }
}
