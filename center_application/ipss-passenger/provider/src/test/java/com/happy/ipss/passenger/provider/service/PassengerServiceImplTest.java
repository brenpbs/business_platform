package com.happy.ipss.passenger.provider.service;

import com.alibaba.boot.hsf.annotation.HSFConsumer;
import com.happy.ipss.dto.PageDTO;
import com.happy.ipss.passenger.provider.IpssPassengerProviderApplication;
import com.happy.ipss.passenger.provider.serivce.PassengerServiceImpl;
import com.happy.ipss.passenger.api.dto.PassengerParamDTO;
import com.happy.ipss.passenger.api.dto.PassengerResultDTO;
import com.happy.ipss.passenger.api.service.PassengerService;
import com.taobao.pandora.boot.test.junit4.DelegateTo;
import com.taobao.pandora.boot.test.junit4.PandoraBootRunner;
import junit.framework.TestCase;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.stereotype.Component;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(PandoraBootRunner.class)
@DelegateTo(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {IpssPassengerProviderApplication.class, PassengerServiceImpl.class})
@Component
public class PassengerServiceImplTest {
    @HSFConsumer(generic = true)
    private PassengerService passengerService;
    @Test
    public void findById() {
        TestCase.assertNotNull(passengerService.findById("5db4c960d5c445cc8f9e259029fb996b"));
    }
    @Test
    public void findByParam() {
        PassengerParamDTO passengerParamDTO = new PassengerParamDTO();
        passengerParamDTO.setPageSize(10);
        passengerParamDTO.setPageNum(1);
        passengerParamDTO.setCertificateNumber("612999198801011212");
        passengerParamDTO.setMemberName("张冰");
        PageDTO<PassengerResultDTO> pageDTO =  passengerService.findByParam(passengerParamDTO);
        TestCase.assertNotNull(pageDTO);
    }
}