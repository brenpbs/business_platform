package com.happy.ipss.passenger.provider.serivce;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import com.alibaba.boot.hsf.annotation.HSFProvider;
import com.happy.ipss.dto.PageDTO;
import com.happy.ipss.exception.HandlerException;
import com.happy.ipss.exception.ParamException;
import com.happy.ipss.passenger.provider.utils.PageUtil;
import com.happy.ipss.passenger.api.dto.PassengerParamDTO;
import com.happy.ipss.passenger.api.dto.PassengerResultDTO;
import com.happy.ipss.passenger.api.service.PassengerService;
import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
@Slf4j
@HSFProvider(serviceInterface = PassengerService.class)
public class PassengerServiceImpl implements PassengerService {
    @Autowired
    private PageUtil pageUtil;
    @Autowired
    private SqlSessionTemplate sqlSessionTemplate;
    /**
     * 101:根据旅客OneID查询旅客基本信息
     * @param id    旅客OneID
     * @return
     */
    @Override
    public PassengerResultDTO findById(String id){
        log.info(" 行李查询 根据ID查询旅客："+ id);
        if (StrUtil.isBlank(id)){
            throw new ParamException("id不能为空");
        }
        PassengerResultDTO passengerResultDTO = sqlSessionTemplate.selectOne(
                "PassengerMemberMapper.selectByPrimaryKey",id);
        if (ObjectUtil.isNull(passengerResultDTO)||1 == passengerResultDTO.getIsDeleted()){
            throw new HandlerException("该会员不存在");
        }
        return passengerResultDTO;
    }
    /**
     * 102:根据查询条件查询旅客列表
     * @param passengerParamDTO 查询条件
     * @return
     */
    @Override
    public PageDTO<PassengerResultDTO> findByParam(PassengerParamDTO passengerParamDTO) {
        log.info(" 行李查询 根据参数查询旅客："+ JSONUtil.toJsonStr(passengerParamDTO));
        final String template = "PassengerMemberMapper.selectByParam";
        return pageUtil.selectPage(template,passengerParamDTO);
    }
}
