package com.happy.ipss.passenger.provider.utils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.happy.ipss.dto.BasePageDTO;
import com.happy.ipss.dto.PageDTO;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
@Component
public class PageUtil<T> {

    @Autowired
    SqlSessionTemplate sqlSessionTemplate;

    /**
     * 分页查询封装
     * @param template
     * @param basePageDTO
     * @return
     */
    public PageDTO<T> selectPage(String template, BasePageDTO basePageDTO) {
        PageInfo<T> pageInfo = PageHelper.startPage(basePageDTO.getPageNum(), basePageDTO.getPageSize())
                .doSelectPageInfo(() -> {sqlSessionTemplate.selectList(template, basePageDTO);
        });
        return new PageDTO(pageInfo.getTotal(),pageInfo.getPages(), pageInfo.getList());
    }
}
