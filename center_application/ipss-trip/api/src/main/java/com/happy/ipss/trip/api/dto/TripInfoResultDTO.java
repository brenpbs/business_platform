package com.happy.ipss.trip.api.dto;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
/**
 * 旅客行程返回信息
 * @author kkh
 */
@Setter
@Getter
public class TripInfoResultDTO {
    /**
     * 行程编号
     */
    private String tripNumber;
    /**
     * 旅客编号
     */
    private String oneId;
    /**
     * 航班号
     */
    private String flightNumber;
    /**
     * 航班日期
     */
    private Date flightDate;
    /**
     * 出发地
     */
    private String flightDeparture;
    /**
     * 目的地
     */
    private String flightDestination;
    /**
     * 登机时间
     */
    private Date boardTime;
    /**
     * 登机牌号码
     */
    private String boardNumber;
    /**
     * 登机口
     */
    private String boardGate;
    /**
     * 创建时间
     */
    private Date gmtCreate;
    /**
     * 更新时间
     */
    private Date gmtModified;
    /**
     * 备注
     */
    private String remark;
    /**
     * 备注1
     */
    private String spare1;
    /**
     * 备注2
     */
    private String spare2;
    /**
     * 备注3
     */
    private String spare3;

}
