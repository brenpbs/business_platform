package com.happy.ipss.trip.api.dto;
import com.happy.ipss.dto.BasePageDTO;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
/**
 * 旅客行程查询条件
 * @author kkh
 */
@Getter
@Setter
public class TripInfoParamDTO extends BasePageDTO {
    /**
     * 旅客编号
     */
    private String oneId;
    /**
     * 航班日期
     */
    private Date flightDate;
    /**
     * 目的地
     */
    private String flightDestination;
}
