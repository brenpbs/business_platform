package com.happy.ipss.trip.api.service;

import com.happy.ipss.dto.PageDTO;
import com.happy.ipss.trip.api.dto.TripInfoResultDTO;
import com.happy.ipss.trip.api.dto.TripInfoParamDTO;

public interface TripInfoService {
    /**
     * 201:按旅客信息查询行程信息
     * @param tripInfoParamDTO
     */
    TripInfoResultDTO findTripInfo(TripInfoParamDTO tripInfoParamDTO);
    /**
     * 202:按条件查询旅客行程列表
     * @param tripInfoParamDTO
     */
    PageDTO<TripInfoResultDTO> findTripInfos(TripInfoParamDTO tripInfoParamDTO);
}
