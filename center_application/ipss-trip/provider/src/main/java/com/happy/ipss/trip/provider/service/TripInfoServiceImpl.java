package com.happy.ipss.trip.provider.service;
import com.happy.ipss.dto.PageDTO;
import com.happy.ipss.trip.provider.utils.PageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.SqlSessionTemplate;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import com.alibaba.boot.hsf.annotation.HSFProvider;
import com.happy.ipss.exception.HandlerException;
import com.happy.ipss.trip.api.dto.TripInfoParamDTO;
import com.happy.ipss.trip.api.dto.TripInfoResultDTO;
import com.happy.ipss.trip.api.service.TripInfoService;
import com.happy.ipss.exception.ParamException;
@Slf4j
@HSFProvider(serviceInterface = TripInfoService.class)
public class TripInfoServiceImpl implements TripInfoService {
    @Autowired
    private SqlSessionTemplate sqlSessionTemplate;
    @Autowired
    private PageUtil pageUtil;
    /**
     * 202:按旅客查询行程列表
     * @param tripInfoParamDTO
     */
    @Override
    public TripInfoResultDTO findTripInfo(TripInfoParamDTO tripInfoParamDTO) {
        log.info("根据旅客oneid和航班日期查询旅客的行程："+ JSONUtil.toJsonStr(tripInfoParamDTO));
        if (StrUtil.isBlank(tripInfoParamDTO.getOneId())|| ObjectUtil.isNull(tripInfoParamDTO.getFlightDate())){
            throw new ParamException("旅客oneid或航班日期不能为空");
        }
        TripInfoResultDTO tripInfoResultDTO = sqlSessionTemplate.
                selectOne("TripMapper.selectByParam",tripInfoParamDTO);
        if(ObjectUtil.isNull(tripInfoResultDTO)){
            throw new HandlerException("该旅客没有行程");
        }
        return tripInfoResultDTO;
    }
    /**
     * 202:按条件查询旅客行程列表
     * @param tripInfoParamDTO
     * @return
     */
    public PageDTO findTripInfos(TripInfoParamDTO tripInfoParamDTO) {
        log.info("根据旅客oneid分页查询旅客的所有行程："+ JSONUtil.toJsonStr(tripInfoParamDTO));
        return pageUtil.selectPage("TripMapper.selectByParam",tripInfoParamDTO);
    }
}
