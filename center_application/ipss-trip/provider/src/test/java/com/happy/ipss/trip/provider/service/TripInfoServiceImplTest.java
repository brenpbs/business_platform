package com.happy.ipss.trip.provider.service;
import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import com.alibaba.boot.hsf.annotation.HSFConsumer;
import com.happy.ipss.dto.PageDTO;
import com.happy.ipss.trip.api.dto.TripInfoParamDTO;
import com.happy.ipss.trip.api.dto.TripInfoResultDTO;
import com.happy.ipss.trip.api.service.TripInfoService;
import com.happy.ipss.trip.provider.IpssTripProviderApplication;
import com.taobao.pandora.boot.test.junit4.DelegateTo;
import com.taobao.pandora.boot.test.junit4.PandoraBootRunner;
import junit.framework.TestCase;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.stereotype.Component;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.text.SimpleDateFormat;
import java.util.Date;
@RunWith(PandoraBootRunner.class)
@DelegateTo(SpringJUnit4ClassRunner.class)
// 加载测试需要的类，一定要加入 Spring Boot 的启动类，其次需要加入本类。
@SpringBootTest(classes = {IpssTripProviderApplication.class, TripInfoServiceImplTest.class })
@Component
public class TripInfoServiceImplTest {
    @HSFConsumer(generic = true)
    TripInfoService tripInfoService;
    @Test
    public void findTripInfo(){
        TripInfoParamDTO tripInfoParamDTO = new TripInfoParamDTO();
        tripInfoParamDTO.setOneId("5db4c960d5c445cc8f9e259029fb996b");
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

        tripInfoParamDTO.setFlightDate(DateUtil.parse("2020-01-28", "yyyy-MM-dd"));
        TripInfoResultDTO tripInfoResultDTO = tripInfoService.findTripInfo(tripInfoParamDTO);
        TestCase.assertEquals(tripInfoResultDTO.getFlightNumber(),"MU2770");
    }
    @Test
    public void findTripInfos(){
        TripInfoParamDTO tripInfoParamDTO = new TripInfoParamDTO();
        tripInfoParamDTO.setOneId("5db4c960d5c445cc8f9e259029fb996b");
        PageDTO<TripInfoResultDTO> tripInfoResultDTOs = tripInfoService.findTripInfos(tripInfoParamDTO);
        TestCase.assertEquals(tripInfoResultDTOs.getList().size(),1);
    }
}
